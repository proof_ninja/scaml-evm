(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2020  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Ppxlib
open Ocaml_common

let log fmt = Format.eprintf fmt

(* XXX Maybe not precise, but I do not know the proper way to get the source
   code name *)
let get_source_of_str = function
  | [] -> None
  | s :: _ -> Some s.pstr_loc.Location.loc_start.Lexing.pos_fname

(* modified version of Compile_common.with_info *)
let with_info str k =
  Compmisc.init_path () ;
  let source_file =
    match get_source_of_str str with Some fn -> fn | None -> "noname"
  in
  let output_prefix = Compenv.output_prefix source_file in
  let module_name = Compenv.module_of_filename source_file output_prefix in
  Env.set_unit_name module_name ;
  let env = Compmisc.initial_env () in
  let dump_file = String.concat "." [output_prefix; ".cmotmp"] in
  Compmisc.with_ppf_dump ~file_prefix:dump_file @@ fun ppf_dump ->
  k
    {
      Compile_common.source_file;
      module_name;
      output_prefix;
      env;
      ppf_dump;
      tool_name = "scaml.compiler.ppx";
      native = false;
    }

(* returns [true] if [str] contains [scaml] attribute *)
let is_with_scaml str =
  let module M = struct
    class iter =
      object
        inherit Ast_traverse.iter
        method! attribute a = if a.attr_name.txt = "SCaml" then raise Exit
      end
  end in
  let i = new M.iter in
  try
    i#structure str ;
    false
  with Exit -> true

open SCaml_tools

(* PPX only inherits the following Clflag options.  See parsing/ast_mapper.ml,
   PpxContext.make:

   lid "tool_name",    make_string tool_name;
   lid "include_dirs", make_list make_string !Clflags.include_dirs;
   lid "load_path",    make_list make_string (Load_path.get_paths ());
   lid "open_modules", make_list make_string !Clflags.open_modules;
   lid "for_package",  make_option make_string !Clflags.for_package;
   lid "debug",        make_bool !Clflags.debug;
   lid "use_threads",  make_bool !Clflags.use_threads;
   lid "use_vmthreads", make_bool false;
   lid "recursive_types", make_bool !Clflags.recursive_types;
   lid "principal", make_bool !Clflags.principal;
   lid "transparent_modules", make_bool !Clflags.transparent_modules;
   lid "unboxed_types", make_bool !Clflags.unboxed_types;
   lid "unsafe_string", make_bool !Clflags.unsafe_string;
*)
let preprocess str info =
  let open Compile_common in
  (* By default, dune compiles a module with adding a prefix like:
       ocamlc ... x.ml -open Dune__exe -o .../dune__exe__Main.cmx
     Unfortunately, info does not contain this prefixed name.

     We have a hack to workaround it.
  *)
  Clflags.dont_write_files := true ;

  let info =
    if List.mem "Dune__exe" !Clflags.open_modules then begin
      { info with module_name = "Dune__exe__" ^ info.module_name;
                  output_prefix = Filename.(concat (dirname info.output_prefix) ("dune__exe__" ^ info.module_name))
      }
    end else
      info
  in

  (* We need OCaml's str, not one for Ppxlib *)
  let str' = Ppxlib.Selected_ast.to_ocaml Structure str in
  let typed = Compile_common.typecheck_impl info str' in
  let typed = { typed with structure = SCamlc.Attribute.filter_by_SCaml_attribute typed.structure } in
  let module_ =
    (* Exceptions must be raised as are, to be handled nicely by
       ocamlc and merlin *)
    SCamlc.SCamlComp.translate_and_optimize
      info.source_file
      info.output_prefix
      info.module_name
      typed
  in
  if module_.contracts = [] && module_.defs = None then begin
    log "Warning: %s has no SCaml code" info.module_name; (* XXX make it a proper warning *)
    str
  end else
    let open Ast_builder.Default in
    let loc = Location.in_file info.source_file in
    let register =
      (* Rather than writing pretty printer for module_, we use OCaml marshaling *)
      log "Embedding SCaml intermediate code into module %s@." info.Compile_common.module_name ;
      (* log "  @[%a@]@." SCamlc.SCamlComp.Module.pp module_; *)
      [
        [%stri
          let () =
            SCamlc.Ppx.register [%e estring ~loc (Marshal.to_string module_ [])]];
      ]
    in
    let emits =
      List.map
        (fun em ->
          let n = em.SCamlc.Contract.name in
          log "Adding SCaml linker for %s.tz into module %s@." n info.Compile_common.module_name;
          (* log "  @[%a@]@." SCamlc.Contract.pp em; *)
          [%stri let () = SCamlc.Ppx.emit [%e estring ~loc n]])
        module_.contracts
    in
    str @ register @ emits

let debug = SCaml_tools.Debug.register ["scaml"; "compiler"; "ppx"]

let impl str =
  (* The untyped PPX preprocessing should be done prior to this PPX compiler.
     The PPX compiler should not perform the preprocessing, since if it fails
     due to a type error, the preprocessing result is gone. *)
  let tool_name = Ast_mapper.tool_name () in
  match tool_name with
  | "ocamldep" -> str
  | _ when not @@ is_with_scaml str -> str
  | "merlin" | "ocamlc" | "ocamlopt" ->
      if !debug then
        log
          "scaml.compiler.ppx: %s %s@."
          tool_name
          (Stdlib.Option.value (get_source_of_str str) ~default:"???");
      with_info str @@ preprocess str
  | _ ->
      Format.eprintf
        "scaml.compiler.ppx: called from unknown tool %s.  Skip processing"
        tool_name ;
      str

let () = Driver.register_transformation ~impl "scaml.compiler.ppx"
