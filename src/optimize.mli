(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

val knormalize : IML.t -> IML.t
val beta : exclude:Ident.t list -> bool ref -> IML.t -> IML.t
val assoc : bool ref -> IML.t -> IML.t
val inline : exclude:Ident.t list -> bool ref -> IML.t -> IML.t
val elim : exclude:Ident.t list -> bool ref -> IML.t -> IML.t
val cfold : exclude:Ident.t list -> bool ref -> IML.t -> IML.t

val unknormalize : exclude:Ident.t list -> IML.t -> IML.t
val inline_pmatch : IML.t -> IML.t

val alpha_conv :
  exclude:Ident.t list -> (Ident.t * Ident.t) list -> IML.t -> IML.t

val tailrec2loop : IML.t -> IML.t
