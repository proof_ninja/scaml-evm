# SCaml language reference

## SCaml is OCaml

SCaml is a pure subset of OCaml.  Please refer [OCaml's reference manual](https://ocaml.org/manual/) for the basic language syntax and semantics.

## `open SCaml`

API functions to access Michelson primitives are declared in module [`SCaml`](https://gitlab.com/dailambda/scaml/-/blob/master/lib/SCaml.mli).  In SCaml programs, you should always `open SCaml` first.

In the normal installation, module `SCaml` should be found at directory `` `opam config var prefix`/lib/scaml/scamlib ``  Check `SCaml.mli` in this directory or the source code of SCaml to learn what functions are available.

## Entrypoint

An *entrypoint* of a Tezos smart contract is a program execution entry of the contract.

In SCaml, an entrypoint must be declared with `let [@entry] name = ..`, a toplevel `let` declaration with `[@entry]` attribute.

Each entrypoint is a function of 2 arguments, *parameter* and *storage* which returns a tuple of a list of operations and an updated storage.  The parameter is specified by the caller of the contract.  The storage is the value stored in the contract at the time of the execution.  An *operation* is an action performed after the contract execution such as a token transaction.

The simplest SCaml contract looks like:

```ocaml
open SCaml

let [@entry] main         (* put [@entry] attribute *)
               ()         (* parameter *)
               ()         (* storage *)
  =
    ( []                  (* operation list *)
    , () )                (* updated storage *)
```

### Multiple entrypoints

A contract may have multiple entrypoints.  They must be declared in the *same* module scope.  Here is a contract with 2 entrypoints `incr` and `reset`:

```ocaml
open SCaml

let [@entry] incr () n = [], n + Int 1

let [@entry] reset () _ = [], Int 0
```

### Entrypoint name

Each entrypoint is named based on the variable name of the definition.  It can be overridden by giving `name` field in the `[@entry]` attribute.  For example, the following definitions introduce 2 entry points, `reset` and `do`. 

```ocaml
let [@entry name="do"] do_ () x = x + Int 1

let [@entry] reset () _ = [], Int 0
```

### Entrypoint typing

The type of the entrypoints must have the form:

```ocaml
ty_parameter -> ty_storage -> operation list * ty_storage
```

where `ty_parameter` and `ty_storage` are the contract's parameter type and storage type respectively.  All the entrypoints of a contract must share the same storage type.

#### `type storage`

If type `storage` is defined, it is used as the type of the contract storage.  Therefore, the second arguments of the entrypoints must all have this type.  The following example is rejected:

```
open SCaml

type storage = int

let [@entry] main () () = [], ()
```

because the entrypoint `main`'s storage type is `unit` while the type `storage` is declared equal to `int`.

It is not mandatory to define the type `storage`, unless the contract module is not used for `Contract.create`.

#### Defaulting of entrypoint typing 

In Tezos, the entrypoints cannot have polymorphic types.  Simple contracts often have polymorphic types in OCaml and they cannot be compiled as they are.  SCaml performs *defaulting*, automatically instantiates the polymorphic types of entrypoints to default types:

* If an entrypoint has a polymorphic type in OCaml, SCaml forces its type variables unified with `unit`.
* If the first element of the returned tuple of an entrypoint is inferred polymorphic in OCaml, SCaml unifies it with `operation list`.

In the following example:

```ocaml=
open SCaml

let [@entry] main _ x = [], x
```

As an OCaml program, `main` has a polymorphic type`'a -> 'b -> 'c list * 'b`.  SCaml instantiates it to `unit -> unit -> operation list * unit`.

Without this defaulting, users would have to add type constraints or use `()`:

```ocaml
open SCaml

let [@entry] main (_ : unit) () = ([] : operation list), ()
```


To ease writing entrypoints, their typing has some *defaulting*: 
if OCaml type system infers a polymorphic type for an entrypoint, the type variables are automatically unified:

* The type is first unified with `'parameter -> 'storage -> operation list * 'storage`.
* If type `storage` is defined in the same module scope, the entrypoints' storage types are unified with it.
* Then, the storage type of the entrypoints are unified each other.
* If types of the entrypoints still have polymorphic type variables, they are unified with `unit`.

### Accessing entrypoints

Use `SCaml.contract'` to access the entrypoint of the given name.  For example, `init` entrypoint of the current contract is obtained by:

```ocaml
Option.get (SCaml.contract' Contract.self "init")
```

## Arithmetic types

In SCaml, there are 3 arithmetic types:

`int` 
:    Arbitrary sized integers.  `Int 3`, `Int (-23)`.      This is not the native `int` type of OCaml but defined in `SCaml`.

`nat`
:    Arbitrary sized natural numbers.  `Nat 0`, `Nat 12345`

`tz`
:    Tezzies.  It takes a float but internally it is handled as a natural number of micro tezzies.  `Tz 0.000001` is for 1 mutez.  Note that the size is fixed to 64bits (signed) and `Tz 9223372036854.775807` is the maximum value for `tz`.  Any overflow fails the execution of contracts.

There is no overloading of arithmetic constants.  Even simple integers must be explicitly wrapped with its constructor `Int` for the simplicitly of the language.

Arguments of aritmetic type constructors must be constants like `Int (-42)`, `Nat 3`, `Tz 1.2`.  Non constant arguments such as `Int (3+4)` and `Nat x` are illegal.

Operations over arithmetics are also monomorphic and not overloaded just as OCaml.

* Integers: `+`, `-`, `*`, etc
* Natural numbers: `+^`, `-^`, `*^`, etc.  (`^` for "positive".)
* Tezzies: `+$`, `-$`, `*$`, etc.  (`$` for "currency".)

## Container literals

SCaml has 4 built-in container types: lists, sets, maps, and big maps.
Lists, sets, and maps have literals:

Lists
: `[ Int 1; Int 2; Int 3 ]`

Sets
:  `Set [ Nat 1; Nat 2; Nat 3 ]`

Maps
:  `Map [ (Nat 1, "1"); (Nat 2, "2"); (Nat 3, "3") ]`

Big maps
:  `BigMap [ (Nat 1, "1"); (Nat 2, "2"); (Nat 3, "3") ]`.  Note that this is not available in contract code but only in storage definitions.

Currently, all the elements in `Set _`, `Map _`, and `BigMap _` must be constants.

## Other crypto related literals

Bytes
: `Bytes "0123456789abcdef"`,  Even number of `[0-9a-f]` characters.

Address
: `Address "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN"`

Keys
: `Key "edpkuSR6ywqsk17myFVRcw2eXhVib2MeLc9D1QkEQb98ctWUBwSJpF"`
Key hashes
: `Key_hash "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"`

Signatures
: `Signature "edsigu4chLHh7rDAUxHyifHYTJyuS8zybSSFQ5eSXydXD7PWtHXrpeS19ds3hA587p5JNjyJcZbLx8QtemuJBEFkLyzjAhTjjta"`

Timestamps
: `Timestamp "2019-09-11T08:30:23Z"`, RFC3339 string.

Chain IDs
: `Chain_id "NetXdQprcVkpaWU"`

BLS12-381 parameters
: `G1Bytes ".."`, `G1Point ".."`, `G2Bytes ".."`, `G2Point ".."`, `FrBytes ".."`, `Fr ".."`.  See the type declarations of `bls12_381_*` types in `SCaml.mli` for details.

These constructors must take string literals.  SCaml does not validate the form of strings for now.

## Self

`Contract.self` returns the contract of the code itself.  Its type must tagree with the type of the entire contract.
      
Unlike Michelson's `SELF` operator, `Contract.self` can appear inside a function.  Even if the function value is sent to another contract, it does not point to the other contract but to the original contract which uses `Contract.self`.

## Contract creation and call

### Contract creation

#### Using Michelson code

SCaml provides the lowest level of APIs to originate contracts within SCaml:

* `Contract.create_from_tz_code <Michelson code string>` takes a string literal of
   Michelson source code.
* `Contract.create_from_tz_file <Michelson code path name>` takes a string literal of Michelson source file path.  The Michelson code in the source file is included at the compilation time.  Be careful of setting proper build dependnecy if the Michelson source file is generated from another language. 

#### Using SCaml code

`Contract.create` provides a contract origination written not in Michelson but in SCaml:

```ocaml
open SCaml

module M = struct
  type storage = int
  let [@entry] main () x = [], x + Int 1
end

let [@entry] main () () =
  let op, adrs = Contract.create (module M) None (Tz 0.0) (Int 0) in
  [op], ()
```

`Contract.create` takes an expression `(module M)` where `M` defines a contract with type `storage`.

### Contract call

`Operation.transfer_tokens` is the API to call other contracts within SCaml contracts.

## Exception

Unlike OCaml, exceptions are fatal errors and cannot be caught in SCaml:`try-with` is not supported.

`raise e` can throw an exception `e`. `e` can be predefined or user-defined exceptions.  Exception values are encoded and used for Michelson opcode `FAILWITH` so that they can be investigated.

SCaml exception of path name `C` without arguments is encoded to Michelson's string value `"C"`.  Exception with arguments, `C args`, is encoded to `Pair "C" args'` where `args'` is the encoding of the arguments.

For example:

* `raise Exit` => `"Exit"`
* `raise (Failure "error")` => `Pair "Failure" "error"`
* `raise (MyModule.Error (Int 1, Nat 2))` => `Pair "MyModule.Error" (Pair 1 2)`

## Modules and contracts

Multiple contracts can be defined in 1 compilation unit by splitting
them into modules.  The following example defines contracts `M` and `N`:

```ocaml
open SCaml

module M = struct
  let [@entry] main () () = [], ()
end

module N = struct
  let [@entry] incr () n = [], n + Int 1

  let [@entry] reset () _ = [], Int 0
end
```

Not recommended but contract modules can be nested.  The following defines contracts `M` and `M.N`. Note that contract `M` only has 1 entry ponit `main`:

```ocaml
open SCaml

module M = struct
  let [@entry] main () () = [], ()

  module N = struct
    let [@entry] incr () n = [], n + Int 1
  
    let [@entry] reset () _ = [], Int 0
  end
end
```

## Monomorphism

Michelson is a monomorphic language.  So is SCaml.

If OCaml type-checker, which is used by SCaml, infers polymorphic types for a value, SCaml rejects it.  To avoid it, you have to add type constraints to values whose inferred types are too general.

## No recursion

Michelson does not have an opcode for recursion.  Therefore SCaml does not support recursion either:  `let rec` bindings are rejected.

Still there are still some recursions are available:

* `SCaml` provides mappings and foldings of set, map, and big maps.
* `SCaml` also provides a simple loop: `Loop.left`.

It might be possible to encode recursion in SCaml using Michelson's closure creation and serializers (`Obj.pack` and `Obj.unpack`), but it should be very gas inefficient.  At your own risk.

## Unsupported features of OCaml

The following OCaml features are not supported.

* Polymorphism
* Recursion (Use `Loop.left` for simple loops)
* Reference and mutable record field
* Class and object
* Polymorphic variant
* GADT
* Functor
* First class module

If contract code uses these unsupported features, SCaml compiler rejects it.
