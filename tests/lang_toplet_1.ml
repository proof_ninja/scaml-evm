(*
   STORAGE= Int 2
*)

open SCaml

(* let x = Int 1 *)
(* let (x,y) = (Int 1, Int 3) *)
let [x] = [Int 1]

let[@entry] main () _ = [], x
