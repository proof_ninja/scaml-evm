(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Asttypes
open Untyped
open Typedtree
open Tools
open Error

let debug = SCaml_tools.Debug.register ["scaml"; "translate"]

module M = Michelson
open M.Type
module C = M.Constant

open IML

open Transpattern

let noloc = Location.none

let create_ident s = Ident.create_local s
let contract_self_id = create_ident IML.self_name

(* M.N.x  =>  gM__N__x *)
let flatten_global_path p =
  let rec f = function
    | Path.Pdot (p, n) -> f p ^ "__" ^ n
    | Pident id -> Ident.name id
    | _ -> assert false
  in
  (* We need it persistent with stamp 0,
     since the same id must created at multiple places *)
  Ident.create_persistent & ("g" ^ f p)

(* push_mid M N.x => M.N.x *)
let push_mid mid p =
  let rec f = function
    | Path.Pident id -> Path.Pdot (Pident mid, Ident.name id)
    | Pdot (p', n) -> Pdot (f p', n)
    | Papply _ -> assert false
  in
  f p

module Lenv = struct
  (* In Michelson, a free varible inside a lambda must be packable.
     We extend this restriction to OCaml:

       let x = ... in fun v -> ... x ...  (* x must be packable *)

       fun v1 v2 -> ... v1 ... (* v1 must be packable *)

     This is an over restriction but is not affected by compilation.
     Therefore easier to understand.
  *)
  type t = {
    variables : (Path.t * Ident.t) list;
    (* OCaml name and IML name *)
    fun_level : int;
    (* Level of the current innermost funciton scope.
       Less than 0 means it is at one of entry point/view function abstractions.*)
    modules : (Ident.t * Path.t) list; (* module names and their global path *)
  }

  let empty fun_level =
    {variables = []; fun_level; modules = []}

  let add_locals vs lenv =
    {
      lenv with
      variables = List.map (fun i -> Path.Pident i, i) vs @ lenv.variables;
    }

  let add_values vbs lenv =
    {
      lenv with
      variables = List.map (fun (path, id) -> path, id) vbs @ lenv.variables;
    }

  let add_modules ms lenv = {lenv with modules = ms @ lenv.modules}

  let into_fun ~loc:_ lenv =
    {lenv with fun_level = lenv.fun_level + 1}

  let pp ppf lenv =
    Format.fprintf
      ppf
      "vars= %s@ "
      (String.concat
         ", "
         (List.map
            (fun (p, i) -> Path.xname p ^ " -> " ^ Ident.unique_name i)
            lenv.variables))
end

let attr_has_entry_point =
  let open Parsetree in
  List.find_map_opt (function
      | {attr_name = {txt = "entry"; loc}; attr_payload = payload; _} -> begin
          match Attribute.parse_options_in_payload "entry" ~loc payload with
          | _ :: _ :: _ ->
              Error.raisef
                Entry
                ~loc
                "@@entry cannot specify more than one options"
          | [] -> Some (loc, None)
          | [ Attribute.
                {
                  ident = Longident.Lident "name";
                  value = `Constant (Pconst_string (s, _, _));
                };
            ] ->
                Some (loc, Some s)
          | [{ident = Longident.Lident "name"}] ->
              Error.raisef Entry ~loc "@@entry can take only a string literal"
          | [_] ->
              Error.raisef
                Entry
                ~loc
                "@@entry can take at most one name=<name> binding"
        end
      | _ -> None)

let attr_has_view_point =
  let open Parsetree in
  List.find_map_opt (function
      | {attr_name = {txt = "view"; loc}; attr_payload = payload; _} -> begin
          match Attribute.parse_options_in_payload "view" ~loc payload with
          | _ :: _ :: _ ->
              Error.raisef
                Entry
                ~loc
                "@@view cannot specify more than one options"
          | [] -> Some (loc, None)
          | [ Attribute.
                {
                  ident = Longident.Lident "name";
                  value = `Constant (Pconst_string (s, _, _));
                };
            ] ->
              Some (loc, Some s)
          | [{ident = Longident.Lident "name"}] ->
              Error.raisef Entry ~loc "@@view can take only a string literal"
          | [_] ->
              Error.raisef
                Entry
                ~loc
                "@@view can take at most one name=<name> binding"
        end
      | _ -> None)

let rec list_elems e =
  match e.desc with
  | Cons (e, es) -> e :: list_elems es
  | Nil -> []
  | _ -> Error.raisef Constant ~loc:e.loc "List is expected"

module TopLet = struct
  let top_let_placeholder_attr = IML.Attr.Annot "scaml_placeholder"

  let top_let_placeholder =
    {
      (IML.mk
         ~loc:Location.none
         Michelson.Type.tyUnit
         (IML.Const Michelson.Constant.Unit))
      with
      attrs = [top_let_placeholder_attr];
    }

  let is_top_let_placeholder t = List.mem top_let_placeholder_attr t.attrs

  let top_let ~loc mpath p body ~is_recursive =
    (* Compile a toplet as `let p = body in unit [@scaml_placeholder]` *)
    let patvars = Idents.to_list & Transpattern.Pattern.vars p in
    let gpatvars =
      List.map
        (fun pv ->
          {
            pv with
            desc = flatten_global_path (Path.Pdot (mpath, Ident.name pv.desc));
          })
        patvars
    in
    let iml =
      if is_recursive then
        match p, gpatvars with
        | {desc = Var pv; typ}, [gpv] ->
            mkletrec ~loc (mk ~loc typ pv) body
            & mkletrec
                ~loc
                (mk ~loc gpv.typ gpv.desc)
                (mkvar ~loc (pv, typ))
                top_let_placeholder
        | _ ->
            unsupported
              ~loc
              "recursion with non-variable pattern. (Typechecker should \
               exclude this.)"
      else
        let rename =
          List.fold_right2
            (fun pv gpv t -> mklet ~loc gpv (mkvar ~loc (pv.desc, pv.typ)) t)
            patvars
            gpatvars
            top_let_placeholder
        in
        Transpmatch.compile ~loc body [p, None, rename]
    in
    gpatvars, patvars, iml

  (* like substitution but no alpha conversion *)
  let replace_top_let_placeholder t ~by =
    let rec f t0 =
      if is_top_let_placeholder t0 then by
      else
        let mk desc = {t0 with desc} in
        match t0.desc with
        | Dealloc _ -> assert false
        | Dealloc_then _ -> assert false
        | Let (pv, t1, t2) -> mk & Let (pv, f t1, f t2)
        | LetRec (pv, t1, t2) -> mk & LetRec (pv, f t1, f t2)
        | App (t, []) -> f t
        | App (t, ts) -> mk & App (f t, List.map f ts)
        | Prim (n, ty, ts) -> mk & Prim (n, ty, List.map f ts)
        | Unpair (pv1, pv2, t1, t2) -> mk & Unpair (pv1, pv2, f t1, f t2)
        | Switch_or (t, pl, tl, pr, tr) ->
            mk & Switch_or (f t, pl, f tl, pr, f tr)
        | Switch_cons (t1, p1, p2, t2, t3) ->
            mk & Switch_cons (f t1, p1, p2, f t2, f t3)
        | Switch_none (t1, t2, p, t3) -> mk & Switch_none (f t1, f t2, p, f t3)
        | Var _ | Const _ | Nil | IML_None | AssertFalse -> t0
        | Contract_create (s, l, t1, t2, t3) ->
            mk & Contract_create (s, l, f t1, f t2, f t3)
        | IML_Some t -> mk & IML_Some (f t)
        | Left t -> mk & Left (f t)
        | Right t -> mk & Right (f t)
        | Assert t -> mk & Assert (f t)
        | Fun (c, t) -> mk & Fun (c, f t)
        | Cons (t1, t2) -> mk & Cons (f t1, f t2)
        | Pair (t1, t2) -> mk & Pair (f t1, f t2)
        | IfThenElse (t1, t2, Some t3) ->
            mk & IfThenElse (f t1, f t2, Some (f t3))
        | IfThenElse (t1, t2, None) -> mk & IfThenElse (f t1, f t2, None)
        | Seq (t1, t2) -> mk & Seq (f t1, f t2)
        | Set ts -> mk & Set (List.map f ts)
        | Map kvs -> mk & Map (List.map (fun (k, v) -> f k, f v) kvs)
        | BigMap kvs -> mk & BigMap (List.map (fun (k, v) -> f k, f v) kvs)
    in
    f t
end

let constructions_by_string =
  [
    ( "signature",
      ("signature", "Signature", tySignature, fun ~loc:_ x -> Ok (C.String x)) );
    ( "key_hash",
      ("key_hash", "Key_hash", tyKeyHash, fun ~loc:_ x -> Ok (C.String x)) );
    "key", ("key", "Key", tyKey, fun ~loc:_ x -> Ok (C.String x));
    "address", ("address", "Address", tyAddress, fun ~loc:_ x -> Ok (C.String x));
    ( "timestamp",
      ("timestamp", "Timestamp", tyTimestamp, Transliteral.parse_timestamp) );
    "bytes", ("bytes", "Bytes", tyBytes, Transliteral.parse_bytes);
    ( "chain_id",
      ("chain_id", "Chain_id", tyChainID, fun ~loc:_ x -> Ok (C.String x)) );
    ( "bls12_381_g1",
      ("bls12_381_g1", "G1Bytes", tyBLS12_381_G1, Transliteral.parse_bytes) );
    ( "bls12_381_g2",
      ("bls12_381_g2", "G2Bytes", tyBLS12_381_G2, Transliteral.parse_bytes) );
    ( "bls12_381_fr",
      ("bls12_381_fr", "FrBytes", tyBLS12_381_Fr, Transliteral.parse_bytes) );
  ]

type res = {
  globals : IML.PatVar.t list;
  locals : (Path.t * Ident.t) list;
  code : IML.t list;
}

let pp_res ppf { globals; locals; code } =
  let f fmt = Format.fprintf ppf fmt in
  f "@[@[<2>globals: @[%a@]@]@ @[<2>locals: @[%a@]@]@ @[<2>code: @[%a@]@]"
    (Format.list ";@ " IML.PatVar.pp) globals
    (Format.list ";@ " (fun ppf (path, id) ->
         Format.fprintf ppf "%a, %a" Path.xpp path Ident.pp id)) locals
    (Format.list "@ and " IML.pp) code

let res_empty = {globals = []; locals = []; code = []}

(* XXX This handling is extremely confusing and error prone *)
let res_concat res1 res2 =
  {
    globals = res1.globals @ res2.globals;
    locals = res2.locals @ res1.locals; (* locals for env therefore must be reversed *)
    code = res1.code @ res2.code;
  }

let rec construct lenv ~loc tyenv exp_type ({Types.cstr_name} as cdesc) args =
  let gloc = Location.ghost loc in
  let typ =
    Result.at_Error (fun e ->
        Error.raisef
          Type_expr
          ~loc
          "@[This has type %a.@ %a@]"
          Printtyp.type_expr
          exp_type
          Transtype.pp_error
          e)
    & Transtype.type_expr tyenv exp_type
  in
  let make typ desc = {loc; typ; desc; attrs = []} in
  match Types.get_desc (Ctype.expand_head tyenv exp_type), typ.desc with
  (* bool *)
  | Tconstr (p, [], _), _ when p = Predef.path_bool ->
      make
        tyBool
        (match cstr_name with
        | "true" -> Const (C.Bool true)
        | "false" -> Const (C.Bool false)
        | s -> Error.raisef_internal ~loc "strange bool constructor %s" s)
  (* list *)
  | Tconstr (p, [_], _), TyList ty when p = Predef.path_list -> begin
      match cstr_name with
      | "[]" -> make (tyList ty) Nil
      | "::" -> begin
          match args with
          | [e1; e2] ->
              let e1 = expression lenv e1 in
              let e2 = expression lenv e2 in
              mkcons ~loc e1 e2
          | _ -> Error.raisef_internal ~loc "strange cons"
        end
      | s -> Error.raisef_internal ~loc "strange list constructor %s" s
    end
  (* option *)
  | Tconstr (p, [_], _), TyOption (_, ty) when p = Predef.path_option -> begin
      match cstr_name with
      | "None" -> make (tyOption (None, ty)) IML_None
      | "Some" -> begin
          match args with
          | [e1] ->
              let e1 = expression lenv e1 in
              mksome ~loc e1
          | _ -> Error.raisef_internal ~loc "strange cons"
        end
      | s -> Error.raisef_internal ~loc "strange list constructor %s" s
    end
  (* sum *)
  | Tconstr (p, [_; _], _), TyOr (_, tyl, _, tyr)
    when match Path.is_scaml p with Some "sum" -> true | _ -> false -> (
      let arg =
        match args with
        | [arg] -> arg
        | _ -> Error.raisef_internal ~loc "strange sum arguments"
      in
      match cstr_name with
      | "Left" ->
          let e = expression lenv arg in
          (* e.typ = ty1 *)
          mkleft ~loc tyr e
      | "Right" ->
          let e = expression lenv arg in
          (* e.typ = ty2 *)
          mkright ~loc tyl e
      | s -> Error.raisef_internal ~loc "strange sum constructor %s" s)
  (* unit *)
  | Tconstr (p, _, _), TyUnit when p = Predef.path_unit ->
      make tyUnit (Const Unit)
(*
  (* set *)
  | Tconstr (p, [_], _), TySet _
    when match Path.is_scaml p with Some "set" -> true | _ -> false -> (
      (* XXX comparable type check *)
      if cstr_name <> "Set" then
        Error.raisef_internal ~loc "strange set constructor" ;
      match args with
      | [arg] ->
          let es = list_elems & expression lenv arg in
          mke ~loc typ (Set es)
      | _ -> Error.raisef_internal ~loc "strange set arguments")
  (* map *)
  | Tconstr (p, [_; _], _), TyMap _
    when match Path.is_scaml p with Some "map" -> true | _ -> false -> (
      (* XXX comparable type check *)
      if cstr_name <> "Map" then
        Error.raisef_internal ~loc "strange map constructor" ;
      match args with
      | [arg] ->
          let es = list_elems & expression lenv arg in
          (*
                let rec check_uniq = function
                  | [] | [_] -> ()
                  | (c1,_)::(c2,_)::_ when c1 = c2 -> (* XXX OCaml's compare *)
                      errorf ~loc "Map literal contains duplicated key %a" C.pp c1
                  | _::xs -> check_uniq xs
                in
                check_uniq xs;
*)
          let kvs =
            List.map
              (fun e ->
                match e.desc with
                | Pair (k, v) -> k, v
                | _ ->
                    Error.raisef
                      Constant
                      ~loc:e.loc
                      "Map binding must be a pair expression")
              es
          in
          {loc; typ; desc = Map kvs; attrs = []}
      | _ -> Error.raisef_internal ~loc "strange map arguments")
  (* big_map
     We cannot write big_map constants in Michelson but we can write them
     at storage initializations: tezos-client run script .. on storage '{ Elt 1 1 }' ..
  *)
  | Tconstr (p, [_; _], _), TyBigMap _
    when match Path.is_scaml p with Some "big_map" -> true | _ -> false -> (
      (* XXX comparable type check *)
      if cstr_name <> "BigMap" then
        Error.raisef_internal ~loc "strange big_map constructor" ;
      match args with
      | [arg] ->
          let es = list_elems & expression lenv arg in
          (*
                let rec check_uniq = function
                  | [] | [_] -> ()
                  | (c1,_)::(c2,_)::_ when c1 = c2 -> (* XXX OCaml's compare *)
                      errorf ~loc "Map literal contains duplicated key %a" C.pp c1
                  | _::xs -> check_uniq xs
                in
                check_uniq xs;
*)
          let kvs =
            List.map
              (fun e ->
                match e.desc with
                | Pair (k, v) -> k, v
                | _ ->
                    Error.raisef
                      Constant
                      ~loc:e.loc
                      "Map binding must be a pair expression")
              es
          in
          {loc; typ; desc = BigMap kvs; attrs = []}
      | _ -> Error.raisef_internal ~loc "strange map arguments")
*)
  | Tconstr (p, [], _), _ when Path.is_scaml p <> None -> begin
      match Path.is_scaml p with
      | None -> assert false
      | Some "bls12_381_g1" when cstr_name = "G1Point" -> begin
          match args with
          | [] | [_] | _ :: _ :: _ :: _ ->
              Error.raisef_internal ~loc "strange arguments for G1Point"
          | [arg1; arg2] -> (
              let e1 = expression lenv arg1 in
              let e2 = expression lenv arg2 in
              match e1.desc, e2.desc with
              | Const (String arg1), Const (String arg2) -> begin
                  match Transliteral.parse_g1 ~loc arg1 arg2 with
                  | Ok v -> {loc; typ; desc = Const v; attrs = []}
                  | Error e ->
                      Error.raisef
                        Constant
                        ~loc
                        "strange arguments for G1Point: %s"
                        e
                end
              | _ ->
                  Error.raisef
                    Constant
                    ~loc
                    "G1Point must take 2 string constants")
        end
      | Some "bls12_381_g2" when cstr_name = "G2Point" -> begin
          match args with
          | [] | [_] | _ :: _ :: _ :: _ ->
              Error.raisef_internal ~loc "strange arguments for G1Point"
          | [arg1; arg2] -> (
              let e1 = expression lenv arg1 in
              let e2 = expression lenv arg2 in
              match e1.desc, e2.desc with
              | ( Pair ({desc = Const (String x1)}, {desc = Const (String x2)}),
                  Pair ({desc = Const (String y1)}, {desc = Const (String y2)})
                ) -> begin
                  match Transliteral.parse_g2 ~loc (x1, x2) (y1, y2) with
                  | Ok v -> {loc; typ; desc = Const v; attrs = []}
                  | Error e ->
                      Error.raisef
                        Constant
                        ~loc
                        "strange arguments for G2Point: %s"
                        e
                end
              | _ ->
                  Error.raisef
                    Constant
                    ~loc
                    "G2Point must take a constant of (string * string) * \
                     (string * string)")
        end
      | Some "bls12_381_fr" when cstr_name = "Fr" -> begin
          match args with
          | [] | _ :: _ :: _ ->
              Error.raisef_internal ~loc "strange arguments for Fr"
          | [arg] -> (
              let e = expression lenv arg in
              match e.desc with
              | Const (String arg) -> begin
                  match Transliteral.parse_fr ~loc arg with
                  | Ok v -> {loc; typ; desc = Const v; attrs = []}
                  | Error e ->
                      Error.raisef
                        Constant
                        ~loc
                        "strange arguments for Fr: %s"
                        e
                end
              | _ -> Error.raisef Constant ~loc "Fr must take a string constant"
              )
        end
      | Some n -> (
          (* C "string" style constants *)
          match List.assoc_opt n constructions_by_string with
          | None -> unsupported ~loc "constants of data type %s" (Path.name p)
          | Some (tyname, cname, typ, parse) -> (
              if cstr_name <> cname then
                Error.raisef_internal
                  ~loc
                  "strange constructor for %s: %s"
                  tyname
                  cstr_name ;
              match args with
              | [] | _ :: _ :: _ ->
                  Error.raisef_internal ~loc "strange arguments for %s" cname
              | [arg] -> (
                  let e = expression lenv arg in
                  match e.desc with
                  | Const (String s) -> begin
                      match parse ~loc:e.loc s with
                      | Ok v -> {e with typ; desc = Const v}
                      | Error s ->
                          Error.raisef
                            Constant
                            ~loc:e.loc
                            "Parse error of %s string: %s"
                            tyname
                            s
                    end
                  | _ ->
                      Error.raisef
                        Constant
                        ~loc
                        "%s only takes a string literal"
                        cname)))
    end
  (* others *)
  | Tconstr (p, [], _), _ when Path.is_scaml p = None -> (
      let rec embed ty sides arg =
        match ty.M.Type.desc, sides with
        | _, [] -> arg
        | TyOr (_, ty1, _, ty2), Binplace.Left :: sides ->
            mkleft ~loc:gloc ty2 (embed ty1 sides arg)
        | TyOr (_, ty1, _, ty2), Right :: sides ->
            mkright ~loc:gloc ty1 (embed ty2 sides arg)
        | _ -> assert false
      in
      match Env.find_type_descrs p tyenv with
      | Type_abstract when p = Predef.path_exn ->
          Error.raisef
            Type_expr
            ~loc
            "%a"
            Transtype.pp_error
            Exception_out_of_raise
      | Type_abstract ->
          Error.raisef
            Type_expr
            ~loc
            "Abstract data type %s is not supported in SCaml"
            (Path.name p)
      | Type_record _ -> assert false (* record cannot come here *)
      | Type_variant (constrs, _) -> (
          (* XXX mty is already calculated at the beginning *)
          let consts_ty, non_consts_ty, entire_ty =
            from_Ok & Transtype.variant_type tyenv exp_type p constrs
          in
          let rec find_constr i = function
            | [] -> assert false
            | n :: _ when n = cstr_name -> i
            | _ :: names -> find_constr (i + 1) names
          in
          match cdesc.cstr_arity, consts_ty, non_consts_ty with
          | _, None, [] -> assert false
          | 0, None, _ -> assert false
          | 0, Some names, [] -> mkint ~loc:gloc & find_constr 0 names
          | 0, Some names, xs ->
              let sides = Binplace.path 0 (List.length xs + 1) in
              embed entire_ty sides & mkint ~loc:gloc & find_constr 0 names
          | _, _, [] -> assert false
          | _, _, cs ->
              let names = List.map fst cs in
              let i = find_constr 0 names in
              let sides =
                let shift = if consts_ty = None then 0 else 1 in
                Binplace.path (i + shift) (List.length names + shift)
              in
              embed entire_ty sides
              & Transtype.encode_by (mkpair ~loc:gloc)
              & List.map (expression lenv) args)
      | Type_open -> assert false)
  | Tconstr (p, _, _), _ ->
      Error.raisef
        Type_expr
        ~loc
        "This polymorphic data type %s is not supported in SCaml"
        (Path.name p)
  | _ ->
      Error.raisef
        Type_expr
        ~loc
        "This data type constructor %s is not supported in SCaml"
        cstr_name

and expression (lenv : Lenv.t)
    { exp_desc;
      exp_loc = loc;
      exp_type = mltyp;
      exp_env = tyenv;
      exp_extra = _;
      exp_attributes;
    } =
  let gloc = Location.ghost loc in
  (* wildly ignores extra *)
  (* if exp_extra <> [] then unsupported ~loc "expression extra"; *)
  let exp_attributes = Untyped.Migrate.copy_attributes exp_attributes in
  begin
    match attr_has_entry_point exp_attributes with
    | None -> ()
    | Some (loc, _) ->
        Error.raisef
          Entry
          ~loc
          "entry declaration is only allowed for the toplevel definitions"
  end ;
  begin
    match attr_has_view_point exp_attributes with
    | None -> ()
    | Some (loc, _) ->
        Error.raisef
          Entry
          ~loc
          "view declaration is only allowed for the toplevel definitions"
  end ;
  let typ =
    Result.at_Error (fun e ->
        Error.raisef
          Type_expr
          ~loc
          "This expression has type@ %a.@ %a"
          Printtyp.type_expr
          mltyp
          Transtype.pp_error
          e)
    & Transtype.type_expr tyenv mltyp
  in
  let mk desc = {loc; typ; desc; attrs = []} in
  let e =
    match exp_desc with
    | Texp_ident (p, {loc}, _vd) -> begin
        match Path.is_scaml p with
        | Some "Contract.self" ->
            (* self is defined outside of everything so that SELF
               is never called inside closures *)
            (* We cannot check the paramter type of self here yet *)
            (* Attach the type used when self type error happens *)
            let ty = Printtyp.tree_of_typexp Type mltyp in
            {(mk & Var contract_self_id) with attrs = [Attr.Type ty]}
        | Some "Contract.create_raw" ->
            (* SCaml.Contract.create_raw must be always applied with a string literal.
               If we see it here, it is not.
            *)
            Error.raisef
              Contract
              ~loc
              "Contract.create_raw must be immediately applied to a string \
               literal"
        | Some "Contract.create_from_tz_file" ->
            Error.raisef
              Contract
              ~loc
              "Contract.create_from_tz_file must be immediately applied to a \
               string literal"
        | Some "Contract.create" ->
            Error.raisef
              Contract
              ~loc
              "Contract.create must be immediately applied to a contract \
               module value"
        | Some "Bytes.of_string" ->
            Error.raisef
              Contract
              ~loc
              "Bytes.of_string must be immediately applied to a string constant"
        | Some n -> mk & primitive ~loc typ n []
        | None -> (
            match Path.is_stdlib p with
            | Some _ ->
                Error.raisef
                  Stdlib
                  ~loc
                  "This value is defined in Stdlib module which is not \
                   supported by SCaml."
            | None -> (
                let global =
                  let rec f = function
                    | Path.Pident id -> Ident.persistent id
                    | Pdot (p, _) -> f p
                    | Papply _ -> assert false
                  in
                  f p
                in
                if global then
                  (* XXX Var should take Path.t... here we use a workaround *)
                  mk & Var (flatten_global_path p)
                else
                  match List.assoc_opt p lenv.variables with
                  | None ->
                      Error.raisef Link
                        ~loc
                        "This is not tracked by SCaml. You may want to annotate the module defining it with [@@@SCaml]"
                  | Some id ->
                      (* https://tezos.gitlab.io/michelson-reference/#instr-APPLY

                         Values that are not both pushable and storable
                         (values of type operation, contract _ and big_map _ _)
                         cannot be captured by APPLY (cannot appear in ty1).
                      *)
                      (* The warnings are too many. *)
                      (* if not (List.mem id lenv.fun_variables)
                          && not (Michelson.Type.is_storable typ
                                  && Michelson.Type.is_pushable typ)
                          && lenv.fun_level > 0 then
                         warnf 400 ~loc "Compilation may fail if this variable of non pushable or non storable type is captured in a closure.";
                      *)
                      mk & Var id))
      end
    | Texp_constant (Const_string (s, _loc, _)) -> mk & Const (String s)
    | Texp_constant _ -> unsupported ~loc "constant"
    | Texp_tuple [e1; e2] ->
        let e1 = expression lenv e1 in
        let e2 = expression lenv e2 in
        (* tyPair (e1.typ, e2.typ) = typ *)
        mk & Pair (e1, e2)
    | Texp_tuple es ->
        Binplace.fold
          ~leaf:(fun c -> c)
          ~branch:(fun c1 c2 -> mkpair ~loc:gloc c1 c2)
        & Binplace.place
        & List.map (expression lenv) es
    | Texp_construct ({loc}, c, args) -> construct lenv ~loc tyenv mltyp c args
    | Texp_assert e -> begin
        match e.exp_desc with
        | Texp_construct (_, {cstr_name = "false"}, []) ->
            (* assert false has type 'a *)
            mk AssertFalse
        | _ -> mkassert ~loc & expression lenv e
      end
    | Texp_let
        ( Recursive,
          [({vb_pat = {pat_desc = Tpat_var (funcname, _)}; vb_expr} as vb)],
          e ) ->
        let lenv' = Lenv.add_locals (Typedtree.let_bound_idents [vb]) lenv in

        let e = expression lenv' e in
        let vb_expr = expression lenv' vb_expr in
        let typ = vb_expr.typ in
        let x = {desc = funcname; typ; loc = noloc; attrs = []} in
        mkletrec ~loc x vb_expr e
    | Texp_let (Recursive, [_], _) ->
        unsupported
          ~loc
          "recursion with non-variable pattern. (Typechecker should exclude \
           this.)"
    | Texp_let (Recursive, _, _) -> unsupported ~loc "mutual recursion"
    | Texp_let (Nonrecursive, vbs, e) ->
        let lenv' = Lenv.add_locals (Typedtree.let_bound_idents vbs) lenv in
        (* let p = e and p' = e' in e''
           =>
           let xnew = e in match xnew with p ->
           let xnew' = e' in match xnew' with p' -> e''
        *)
        List.fold_right
          (fun vb e' ->
            let {vb_pat; vb_expr} = vb in
            let vb_expr = expression lenv vb_expr in
            let typ = vb_expr.typ in
            let i = Ident.create_local & Varname.create "x" typ in
            let x = {desc = i; typ; loc = noloc; attrs = []} in
            let ex = {desc = Var i; typ; loc = noloc; attrs = []} in
            {
              desc =
                Let
                  ( x,
                    vb_expr,
                    Transpmatch.compile ~loc ex [pattern vb_pat, None, e'] );
              typ = e'.typ;
              loc;
              attrs = [];
            })
          vbs
        & expression lenv' e
    | Texp_apply (_, []) -> assert false

    | Texp_apply
        ( {
            exp_desc =
              Texp_ident
                (_, _, {val_kind = Val_prim {prim_name = "%sequand"; _}; _});
          },
          [(Nolabel, Some e1); (Nolabel, Some e2)] ) ->
        let e1 = expression lenv e1 in
        let e2 = expression lenv e2 in
        mk
        & IfThenElse
            ( e1,
              e2,
              Some {loc; typ = tyBool; desc = Const (Bool false); attrs = []} )

    | Texp_apply
        ( {
            exp_desc =
              Texp_ident
                (_, _, {val_kind = Val_prim {prim_name = "%sequor"; _}; _});
          },
          [(Nolabel, Some e1); (Nolabel, Some e2)] ) ->
        let e1 = expression lenv e1 in
        let e2 = expression lenv e2 in
        mk
        & IfThenElse
            ( e1,
              {loc; typ = tyBool; desc = Const (Bool true); attrs = []},
              Some e2 )
    | Texp_apply (f, args) -> (
        let get_args =
          List.map (function
              | Nolabel, Some (e : Typedtree.expression) -> expression lenv e
              | _ -> unsupported ~loc "labeled arguments")
        in
        let name =
          match f with
          | {exp_desc = Texp_ident (p, _, _)} -> Path.is_scaml p
          | _ -> None
        in
        match name with
        | None -> (
            let name =
              match f with
              | {exp_desc = Texp_ident (p, _, _)} -> Path.is_stdlib p
              | _ -> None
            in
            match name with
            | Some "@@" (* XXX not "(@@)" *) -> begin
                (* e1 @@ e2 => e1 e2 *)
                match get_args args with
                | [e] -> e
                | e1 :: es -> mk & App (e1, es)
                | [] -> assert false
              end
            | _ -> mk & App (expression lenv f, get_args args))
        | Some "Obj.TypeSafe.pack" ->
            let fty =
              Result.at_Error (fun e ->
                  Error.raisef
                    Type_expr
                    ~loc:f.exp_loc
                    "This primitive has type %a.  %a"
                    Printtyp.type_expr
                    f.exp_type
                    Transtype.pp_error
                    e)
              & Transtype.type_expr f.exp_env
              &
              match Types.get_desc f.exp_type with
              | Tarrow (_, _, ty, _) -> ty
              | _ -> assert false
            in
            (* fty = fty' *)
            mk
            & primitive ~loc:f.exp_loc fty "Obj.pack"
            & get_args (List.tl args)
        | Some "Obj.TypeSafe.unpack" ->
            let fty =
              Result.at_Error (fun e ->
                  Error.raisef
                    Type_expr
                    ~loc:f.exp_loc
                    "This primitive has type %a.  %a"
                    Printtyp.type_expr
                    f.exp_type
                    Transtype.pp_error
                    e)
              & Transtype.type_expr f.exp_env
              &
              match Types.get_desc f.exp_type with
              | Tarrow (_, _, ty, _) -> ty
              | _ -> assert false
            in
            (* fty = fty' *)
            mk
            & primitive ~loc:f.exp_loc fty "Obj.unpack"
            & get_args (List.tl args)
        | Some "Sapling.empty_state" -> begin
            match args with
            | [] ->
                Error.raisef
                  Primitive
                  ~loc
                  "Sapling.empty_state needs an argument"
            | [(Nolabel, Some a)] -> begin
                match Transtype.memo_size a.exp_type with
                | Error e ->
                    Error.raisef
                      Type_expr
                      ~loc
                      "This expression has type %a.  %a"
                      Printtyp.type_expr
                      a.exp_type
                      Transtype.pp_error
                      e
                | Ok ms ->
                    let mty = tySapling_state ms in
                    mk & primitive ~loc:f.exp_loc mty "Sapling.empty_state" []
              end
            | _ -> assert false
          end
        | Some "raise" -> translate_raise ~loc lenv typ args
        | Some "Contract.create" -> mk & contract_create_module ~loc lenv args
        | Some n when String.is_prefix "Contract.create" n ->
            mk & contract_create_other ~loc n & get_args args
        | Some "Bytes.of_string" -> mk & bytes_of_string ~loc lenv args

        | Some "_Int" ->
            begin match args with
            | [(Nolabel, Some a)] -> begin
                match a.exp_desc with
                | Texp_constant (Const_string (s, _, _)) ->
                    mk & Const (Int (Z.of_string s))
                | _ ->
                    Error.raisef Constant ~loc "__int must take a string constant"
              end
            | _ ->
                Error.raisef Constant ~loc "__int needs 1 argument"
            end
        | Some "_Nat" ->
            begin match args with
            | [(Nolabel, Some a)] -> begin
                match a.exp_desc with
                | Texp_constant (Const_string (s, _, _)) ->
                    mk & Const (Int (Z.of_string s))
                | _ ->
                    Error.raisef Constant ~loc "__nat must take a string constant"
              end
            | _ ->
                Error.raisef Constant ~loc "__nat needs 1 argument"
            end
        | Some "_Mutez" ->
            begin match args with
            | [(Nolabel, Some a)] -> begin
                match a.exp_desc with
                | Texp_constant (Const_string (s, _, _)) ->
                    mk & Const (Int (Z.of_string s))
                | _ ->
                    Error.raisef Constant ~loc "_Mutez must take a string constant"
              end
            | _ ->
                Error.raisef Constant ~loc "_Mutez needs 1 argument"
            end

        | Some "_Set" ->
            begin match args with
            | [(Nolabel, Some arg)] ->
                let es = list_elems & expression lenv arg in
                mke ~loc typ (Set es)
            | _ ->
                Error.raisef Constant ~loc "_Set needs 1 argument"
            end

        | Some "_Map" ->
            begin match args with
            | [(Nolabel, Some arg)] ->
                let es = list_elems & expression lenv arg in
          (*
                let rec check_uniq = function
                  | [] | [_] -> ()
                  | (c1,_)::(c2,_)::_ when c1 = c2 -> (* XXX OCaml's compare *)
                      errorf ~loc "Map literal contains duplicated key %a" C.pp c1
                  | _::xs -> check_uniq xs
                in
                check_uniq xs;
*)
                let kvs =
                  List.map
                    (fun e ->
                       match e.desc with
                       | Pair (k, v) -> k, v
                       | _ ->
                           Error.raisef
                             Constant
                             ~loc:e.loc
                             "Map binding must be a pair expression")
                    es
                in
                {loc; typ; desc = Map kvs; attrs = []}
            | _ -> Error.raisef_internal ~loc "strange map arguments"
            end

        | Some "_BigMap" ->
            begin match args with
            | [(Nolabel, Some arg)] ->
                let es = list_elems & expression lenv arg in
          (*
                let rec check_uniq = function
                  | [] | [_] -> ()
                  | (c1,_)::(c2,_)::_ when c1 = c2 -> (* XXX OCaml's compare *)
                      errorf ~loc "Map literal contains duplicated key %a" C.pp c1
                  | _::xs -> check_uniq xs
                in
                check_uniq xs;
*)
                let kvs =
                  List.map
                    (fun e ->
                       match e.desc with
                       | Pair (k, v) -> k, v
                       | _ ->
                           Error.raisef
                             Constant
                             ~loc:e.loc
                             "BigMap binding must be a pair expression")
                    es
                in
                {loc; typ; desc = Map kvs; attrs = []}
            | _ -> Error.raisef_internal ~loc "strange map arguments"
            end

        | Some n ->
            let fty =
              Result.at_Error (fun e ->
                  Error.raisef
                    Type_expr
                    ~loc:f.exp_loc
                    "This primitive has type %a.  %a"
                    Printtyp.type_expr
                    f.exp_type
                    Transtype.pp_error
                    e)
              & Transtype.type_expr f.exp_env f.exp_type
            in
            mk & primitive ~loc:f.exp_loc fty n & get_args args)
    | Texp_function {arg_label = Labelled _ | Optional _} ->
        unsupported ~loc "labeled arguments"
    | Texp_function {arg_label = Nolabel; param = _; cases; partial} ->
        if partial = Partial then
          Error.raisef Pattern_match ~loc "Pattern match is not exhaustive" ;
        let targ, _tret =
          match typ.desc with
          | TyLambda (targ, tret) -> targ, tret
          | _ -> assert false
        in
        (*
                (* name the same name of the original if possible *)
                let i = create_ident & match cases with
                  | [ { c_lhs = { pat_desc= (Tpat_var (id, _) |
                                             Tpat_alias ({ pat_desc= Tpat_any }, id, _)) } } ] ->
                      Ident.name id
                  | _ -> "arg"
                in
        *)
        let i = Ident.create_local & Varname.create "arg" targ in
        let var = {desc = i; typ = targ; loc = noloc; attrs = []} in
        let lenv = Lenv.add_locals [i] & Lenv.into_fun ~loc lenv in
        let evar = {desc = Var i; typ = targ; loc = noloc; attrs = []} in
        (* function case1 | .. | casen
           =>
           fun xnew -> match xnew with case1 | .. | casen
        *)
        let compile_case case =
          let lenv =
            Lenv.add_locals (Typedtree.pat_bound_idents case.c_lhs) lenv
          in
          let guard = Option.fmap (expression lenv) case.c_guard in
          pattern case.c_lhs, guard, expression lenv case.c_rhs
        in
        let t = Transpmatch.compile ~loc evar & List.map compile_case cases in
        mkfun ~loc var t
    | Texp_ifthenelse (cond, then_, Some else_) ->
        let econd = expression lenv cond in
        let ethen = expression lenv then_ in
        let eelse = expression lenv else_ in
        (* ignore (unify ethen.typ eelse.typ);
           ignore (unify typ ethen.typ); *)
        mk & IfThenElse (econd, ethen, Some eelse)
    | Texp_ifthenelse (cond, then_, None) ->
        let econd = expression lenv cond in
        let ethen = expression lenv then_ in
        if ethen.typ.desc <> TyUnit then
          Error.raisef_internal ~loc:ethen.loc "else None has non unit type" ;
        mk & IfThenElse (econd, ethen, None)
    | Texp_match (_, _, Partial) ->
        unsupported ~loc "non exhaustive pattern match"
    | Texp_match (e, cases, Total) ->
        let e = expression lenv e in
        let compile_case case =
          let lenv =
            Lenv.add_locals (Typedtree.pat_bound_idents case.c_lhs) lenv
          in
          let guard = Option.fmap (expression lenv) case.c_guard in
          match Typedtree.split_pattern case.c_lhs with
          | None, None -> assert false (* I guess *)
          | _, Some p -> unsupported ~loc:p.pat_loc "exception pattern"
          | Some p, None -> pattern p, guard, expression lenv case.c_rhs
        in
        Transpmatch.compile ~loc e (List.map compile_case cases)
    | Texp_try _ -> unsupported ~loc "try-with"
    | Texp_variant _ -> unsupported ~loc "polymorphic variant"
    | Texp_record {fields; extended_expression = None; _} ->
        (* I believe fields are already sorted *)
        let es =
          List.map (fun (_ldesc, ldef) ->
              match ldef with
              | Overridden (_, e) -> expression lenv e
              | Kept _ -> assert false)
          & Array.to_list fields
        in
        Binplace.fold
          ~leaf:(fun c -> c)
          ~branch:(fun c1 c2 -> mkpair ~loc:gloc c1 c2)
        & Binplace.place es
    | Texp_record {fields; extended_expression = Some e} ->
        (* optimal code, I hope *)
        (* I believe fields are already sorted *)
        let es =
          List.map (fun (_ldesc, ldef) ->
              match ldef with
              | Overridden (_, e) -> Some (expression lenv e)
              | Kept _ -> None)
          & Array.to_list fields
        in
        let tree = Binplace.place es in
        let rec simplify = function
          | Binplace.Leaf x as t -> t, x = None
          | Branch (t1, t2) ->
              let t1, b1 = simplify t1 in
              let t2, b2 = simplify t2 in
              if b1 && b2 then Leaf None, true else Branch (t1, t2), false
        in
        let rec f e = function
          | Binplace.Leaf None -> e (* no override *)
          | Leaf (Some e) -> e
          | Branch (t1, t2) ->
              let e1 = f (mkfst ~loc:gloc e) t1 in
              let e2 = f (mksnd ~loc:gloc e) t2 in
              mkpair ~loc:gloc e1 e2
        in
        f (expression lenv e) & fst & simplify tree
    | Texp_field (e, _, label) ->
        let pos = label.lbl_pos in
        let nfields = Array.length label.lbl_all in
        if !debug then
            Format.eprintf "field %d %s of %d @." pos label.lbl_name nfields ;
        let e = expression lenv e in
        let rec f e = function
          | [] -> e
          | Binplace.Left :: dirs -> f (mkfst ~loc:gloc e) dirs
          | Binplace.Right :: dirs -> f (mksnd ~loc:gloc e) dirs
        in
        f e & Binplace.path pos nfields
    | Texp_open (_, e) -> expression lenv e
    | Texp_sequence (e1, e2) -> mk & Seq (expression lenv e1, expression lenv e2)
    | Texp_setfield _ -> unsupported ~loc "record field set"
    | Texp_array _ -> unsupported ~loc "array"
    | Texp_while _ -> unsupported ~loc "while-do-done"
    | Texp_for _ -> unsupported ~loc "for-do-done"
    | Texp_send _ -> unsupported ~loc "method call"
    | Texp_new _ -> unsupported ~loc "new"
    | Texp_instvar _ -> unsupported ~loc "class instance variable"
    | Texp_setinstvar _ -> unsupported ~loc "class instance variable set"
    | Texp_override _ -> unsupported ~loc "override"
    | Texp_letmodule _ -> unsupported ~loc "let-module"
    | Texp_letexception _ -> unsupported ~loc "let-exception"
    | Texp_lazy _ -> unsupported ~loc "lazy"
    | Texp_object _ -> unsupported ~loc "object"
    | Texp_pack _ -> unsupported ~loc "first class module"
    | Texp_extension_constructor _ -> unsupported ~loc "open variant"
    | Texp_unreachable -> unsupported ~loc "this type of expression"
    | Texp_letop _ -> unsupported ~loc "let op"
  in
  {e with typ}

(* XXX loc is likely incorrect *)
and primitive ~loc fty n args =
  let apply_left x left =
    match left with
    | [] -> x
    | _ ->
        let typ =
          let rec f ty = function
            | [] -> ty
            | _arg :: args -> (
                match ty.M.Type.desc with
                | TyLambda (_, ty2) -> f ty2 args
                | _ -> assert false)
          in
          f fty args
        in
        App ({loc; (* XXX inaccurate *)
                   typ; desc = x; attrs = []}, left)
  in
  match n with
  | "Contract.self" -> assert false (* must be handled already *)
  | "Contract.contract'" -> begin
      match args with
      | [] | [_] ->
          Error.raisef Contract ~loc "Contract.contract' must be fully applied"
      | address :: ({desc = Const (M.Constant.String _)} as entry) :: left ->
          apply_left (Prim (n, fty, [address; entry])) left
      | _ :: {loc} :: _ ->
          Error.raisef Contract ~loc "contract entry name must be a constant"
    end
  | "view" -> begin
      match args with
      | [] | [_] | [_; _] ->
          Error.raisef Contract ~loc "view must be fully applied"
      | address
        :: ({desc = Const (M.Constant.String _)} as name) :: param :: left ->
          apply_left (Prim (n, fty, [address; name; param])) left
      | _ :: {loc} :: _ ->
          Error.raisef Contract ~loc "view name must be a constant"
    end
  | _ -> (
      match List.assoc_opt n Primitives.primitives with
      | None -> Error.raisef Primitive ~loc "Unknown primitive SCaml.%s" n
      | Some (_pure, arity, _conv) ->
          if arity > List.length args then
            let tys, ret = M.Type.function_args fty in
            let tys =
              List.take (arity - List.length args)
              & List.drop (List.length args) tys
            in
            let xtys =
              List.map
                (fun ty -> Ident.create_local & Varname.create "x" ty, ty)
                tys
            in
            let e =
              List.fold_right
                (fun (x, ty) e -> mkfun ~loc (mk ~loc ty x) e)
                xtys
              & mkprim
                  ~loc
                  ret
                  n
                  fty
                  (args @ List.map (fun (x, ty) -> mkvar ~loc (x, ty)) xtys)
            in
            e.desc
          else
            let args, left = List.split_at arity args in
            apply_left (Prim (n, fty, args)) left)

and value_binding mpath (* XXX *) lenv
    {vb_pat; vb_expr; vb_attributes; vb_loc = loc} ~is_recursive =
  let is_entry =
    match attr_has_entry_point & Migrate.copy_attributes vb_attributes with
    | None -> false
    | Some _ -> true
  in
  let is_view =
    match attr_has_view_point & Migrate.copy_attributes vb_attributes with
    | None -> false
    | Some _ -> true
  in
  begin match is_entry, is_view with
    | true, true ->
        Error.raisef
          Entry
          ~loc
          "A bind can have at most one attribute of entry, view and value attributes" ;
    | _ -> ()
  end;
  let lenv =
    (* For the entry points and views,
       the first 2 arguments are NOT LAMBDA abstractions *)
    if lenv.Lenv.fun_level = 0 && (is_entry || is_view) then
      {lenv with fun_level = -2}
    else lenv
  in
  let p = pattern vb_pat in
  let e = expression lenv vb_expr in
  let globals, locals, iml = TopLet.top_let ~loc mpath p e ~is_recursive in
  {
    globals;
    locals = List.map (fun patvar -> let id = patvar.IML.desc in Path.Pident id, id) locals;
    code = [iml];
  }

and structure_item lenv mpath {str_desc; str_loc = loc} =
  match str_desc with
  | Tstr_eval _ -> unsupported ~loc "toplevel evaluation"
  | Tstr_primitive _ -> unsupported ~loc "primitive declaration"
  | Tstr_typext _ -> unsupported ~loc "type extension"
  | Tstr_recmodule _ -> unsupported ~loc "recursive module declaration"
  | Tstr_class _ -> unsupported ~loc "class declaration"
  | Tstr_class_type _ -> unsupported ~loc "class type declaration"
  | Tstr_modtype _ -> unsupported ~loc "module type declaration"
  | Tstr_include _ -> unsupported ~loc "include"
  | Tstr_value (Recursive, [vb]) ->
      let lenv = Lenv.add_locals (Typedtree.let_bound_idents [vb]) lenv in
      let vbss = value_binding mpath lenv vb ~is_recursive:true in
      lenv, vbss
  | Tstr_value (Recursive, _vbs) ->
      unsupported ~loc "mutually recursive definitions"
  | Tstr_value (Nonrecursive, vbs) ->
      let lenv = Lenv.add_locals (Typedtree.let_bound_idents vbs) lenv in
      let vbss =
        List.fold_right
          (fun vb vbss ->
            let vbs = value_binding mpath lenv vb ~is_recursive:false in
            res_concat vbs vbss)
          vbs
          res_empty
      in
      lenv, vbss
  | Tstr_open _open_description -> lenv, res_empty
  | Tstr_exception _ -> lenv, res_empty
  | Tstr_type _ -> lenv, res_empty
  | Tstr_module {mb_id = None} -> unsupported ~loc "unnamed module"
  | Tstr_module ({mb_id = Some mb_id} as mb) ->
      let mname = Ident.name mb_id in
      let mpath = Path.Pdot (mpath, mname) in
      let rec module_expr me =
        match me.mod_desc with
        | Tmod_structure str -> structure lenv mpath str
        | Tmod_constraint (me, _, _, _) -> module_expr me
        | _ -> unsupported ~loc "module declaration other than simple structure"
      in
      let lenv, vbs = module_expr mb.mb_expr in
      (* Make local defs accessible from the outside *)
      (* module M = struct
           module N = struct
             let x = 1
           end
           let x1 = N.x
         end
         let x2 = M.N.x

         =>

         let x = 1 in
         let N_x = x in      (* add *)
         let x1 = N_x in
         let M_N_x = N_x in  (* add *)
         let M_x1 = x1 in    (* add *)
         let x2 = M_N_x
      *)
      let locals =
        List.map (fun (path, id) -> push_mid mb_id path, id) vbs.locals
      in
      (* add module itself *)
      let locals = (Path.Pident mb_id, mb_id) :: locals in
      let vbs = {vbs with locals} in
      let lenv = Lenv.add_values locals lenv in
      let lenv = Lenv.add_modules [mb_id, mpath] lenv in
      lenv, vbs
  | Tstr_attribute _ ->
      (* simply ignores it for now *)
      lenv, res_empty

and structure lenv mpath {str_items = sitems} =
  List.fold_left
    (fun (lenv, vbss) sitem ->
      let lenv, vbs = structure_item lenv mpath sitem in
      lenv, res_concat vbss vbs)
    (lenv, res_empty)
    sitems

and contract_create_module ~loc lenv args =
  match args with
  | [] | [_] | [_; _] | [_; _; _] ->
      Error.raisef Contract ~loc "Contract.create must be fully applied"
  | [(Nolabel, Some e0); e1; e2; e3] -> begin
      (* e0 must be (module M) *)
      match e0.exp_desc with
      | Texp_pack me ->
          let rec f me =
            match me.mod_desc with
            | Tmod_ident (path, _) -> path
            | Tmod_constraint (me, _, _, _) -> f me
            | _ ->
                Error.raisef
                  Contract
                  ~loc:e0.exp_loc
                  "The first argument of Contract.create must have the form \
                   (module M)"
          in
          let path = f me in
          (* how this path is referred globally? *)
          let path =
            let rec is_global = function
              | Path.Pident id -> Ident.persistent id
              | Pdot (p, _) -> is_global p
              | _ -> assert false
            in
            if is_global path then path
            else
              match List.assoc_opt path lenv.variables with
              | None -> assert false
              | Some id -> (
                  match List.assoc_opt id lenv.modules with
                  | Some p -> p
                  | None -> assert false)
          in
          let e1, e2, e3 =
            let get_args (* XXX dupe *) =
              List.map (function
                  | Nolabel, Some (e : Typedtree.expression) ->
                      expression lenv e
                  | _ -> unsupported ~loc "labeled arguments")
            in
            match get_args [e1; e2; e3] with
            | [e1; e2; e3] -> e1, e2, e3
            | _ -> assert false
          in
          Contract_create (Module path, e0.exp_loc, e1, e2, e3)
      | _ ->
          Error.raisef
            Contract
            ~loc:e0.exp_loc
            "The first argument of Contract.create must have the form (module \
             M)"
    end
  | _ -> assert false
(* too many args must be rejeced by OCaml type system *)

and contract_create_other ~loc n args =
  match n with
  | "Contract.create_raw" | "Contract.create_from_tz_code" -> begin
      match args with
      | [] | [_] | [_; _] | [_; _; _] ->
          Error.raisef Contract ~loc "%s must be fully applied" n
      | [e0; e1; e2; e3] ->
          let s =
            match e0.desc with
            | Const (C.String s) -> s
            | _ ->
                Error.raisef
                  Contract
                  ~loc:e0.loc
                  "The first argument of %s must be a string literal of \
                   Michelson code"
                  n
          in
          Contract_create (Tz_code s, e0.loc, e1, e2, e3)
      | _ ->
          assert false (* too many args must be rejeced by OCaml type system *)
    end
  | "Contract.create_from_tz_file" -> begin
      match args with
      | [] | [_] | [_; _] | [_; _; _] ->
          Error.raisef Contract ~loc "%s must be fully applied" n
      | [e0; e1; e2; e3] ->
          let s =
            match e0.desc with
            | Const (C.String s) -> s
            | _ ->
                Error.raisef
                  Contract
                  ~loc:e0.loc
                  "The first argument of %s must be a string literal of \
                   Michelson file path"
                  n
          in
          Contract_create (Tz_file s, e0.loc, e1, e2, e3)
      | _ ->
          assert false (* too many args must be rejeced by OCaml type system *)
    end
  | _ -> Error.raisef_internal ~loc "Unknown Contract.create* function: %s" n

and translate_raise lenv ~loc typ args =
  match args with
  | [(Nolabel, Some arg)] -> begin
      match arg.exp_desc with
      | Texp_construct (_, cdesc, args) -> begin
          (* raise C(a1, .., an)  = >  failwith ("C", a1, .., an) *)
          match cdesc.cstr_tag with
          | Cstr_extension (p, _) ->
              let name =
                (* It seems TZIPs assume errors without any module
                   name quantification. *)
                let (* rec *) make_name = function
                  | Path.Pident id ->
                      if Ident.persistent id || Ident.is_predef id then
                        Ident.name id
                      else Ident.name id
                  (* | Pdot (p, s) -> make_name p ^ "." ^ s *)
                  | Pdot (_p, s) -> s
                  | Papply _ -> assert false
                in
                make_name p
              in
              let args = List.map (expression lenv) args in
              let arg =
                Binplace.fold
                  ~leaf:(fun c -> c)
                  ~branch:(fun c1 c2 -> mkpair ~loc:arg.exp_loc c1 c2)
                & Binplace.place
                & mke ~loc:cdesc.cstr_loc tyString (Const (C.String name))
                  :: args
              in
              mke ~loc typ & Prim ("raise", tyLambda (arg.typ, typ), [arg])
          | _ -> assert false
        end
      | _ -> Error.raisef Constant ~loc "raise takes only an exception constant"
    end
  | _ -> assert false

and bytes_of_string ~loc lenv args =
  match args with
  | [(Nolabel, Some e)] ->
      let e = expression lenv e in
      begin match e.desc with
      | Const (String s) ->
          let `Hex hex = Hex.of_string s in
          Const (Bytes hex)
      | _ ->
          Error.raisef
            Primitive
            ~loc:e.loc
            "Bytes.of_string must take a string constant"
      end
  | [] -> Error.raisef Primitive ~loc "Bytes.of_string must be fully applied"
  | _ ->
      assert false (* too many args must be rejeced by OCaml type system *)
