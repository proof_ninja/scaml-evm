open SCaml_tools
open SCaml_michelson
open New_size

let f fn_contract fn_storage =
  Format.printf "%s@." fn_storage;

  let m =
    let s = from_Ok @@ Spotlib.File.to_string fn_contract in
    let parsed =
      match Micheline.Parse.parse_expression_string s with
      | Ok parsed -> parsed
      | Error etrace ->
          Format.eprintf "%a@." Tzerror.pp etrace;
          assert false
    in
    match Michelson.Module.of_micheline parsed with
    | Ok m -> m
    | Error (_loc, s) -> prerr_endline s; assert false
  in

  let ty = m.storage in

  let m =
    let s = from_Ok @@ Spotlib.File.to_string fn_storage in
    let parsed =
      match Micheline.Parse.parse_expression_string s with
      | Ok parsed -> parsed
      | Error etrace ->
          Format.eprintf "%a@." Tzerror.pp etrace;
          assert false
    in
      match Michelson.Constant.of_micheline ty parsed with
      | Ok m -> m
      | Error (_loc, s) -> prerr_endline s; assert false
  in
  Format.printf "%s size: %d size2: %d size3: %d size4: %d@."
    fn_storage
    (Size.constant m)
    (New_size2.constant m)
    (New_size3.constant m)
    (New_size4.constant m)

let () =
  List.iter (fun p ->
      let base = p#base in
      let fn_contract = "./_code/" ^ base in
      let fn_storage = "./_storage/" ^ base in
      if base <> "_storage" then f fn_contract fn_storage
    ) @@ Unix.Find.files ["_storage"]
