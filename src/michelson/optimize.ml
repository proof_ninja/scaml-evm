(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2022 DaiLambda, Inc.                     *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open SCaml_tools
open Michelson.Opcode

let debug = Debug.register ["michelson"; "optimize"]

module IntSet = Set.Make(Int)

module Make(Conf : sig
    val logger : string -> unit
    val search_space_multiplier : float (* default: 1.0 *)
    (* val do_dijkstra : bool (* default: false *) *)
    (* val use_lcs_for_astar_score : bool (* default: false *) *)
  end) = struct

  let log = Conf.logger
  let logf fmt = Format.kasprintf log fmt

  (* pair contraction *)
  let rec type_ t =
    let open Michelson.Type in
    let desc =
      (* scrape the right combs *)
      let rec scrape t =
        match t.tyannot with
        | Some _ -> []
        | None ->
            match t.desc with
            | TyPair (f1, t1, (Some _ as f2), t2) ->
                [(f1,t1); (f2,t2)]
            | TyPair (f1, t1, (None as f2), t2) ->
                begin match scrape t2 with
                | [] -> [(f1,t1); (f2,t2)]
                | fts -> (f1,t1)::fts
                end
            | TyPairN fts ->
                let rec f = function
                  | [] -> assert false
                  | [((Some _ as f),t)] -> [(f,t)]
                  | [(None, t)] ->
                      begin match scrape t with
                      | [] -> [(None,t)]
                      | fts -> fts
                      end
                  | ft::fts -> ft::f fts
                in
                f fts
            | _ -> []
      in
      let rec unscrape = function
        | [] | [_] -> assert false
        | [(f1,t1); (f2,t2)] -> tyPair (f1, t1, f2, t2)
        | (f,t)::fts -> tyPair (f,t, None,unscrape fts)
      in
      let mayPairN t =
        match scrape t with
        | [] -> assert false
        | fts ->
            (* Currently, TyPairN is less efficient when #fts < 5 *)
            if List.length fts < 5 then
              (unscrape fts).desc
            else
              TyPairN fts
      in
      match t.desc with
      | TyPair (f1, t1, f2, t2) ->
          let t1 = type_ t1 in
          let t2 = type_ t2 in
          mayPairN { t with desc= TyPair (f1, t1, f2, t2) }
      | TyPairN fts ->
          let fts = List.map (fun (f,t) -> (f, type_ t)) fts in
          mayPairN { t with desc = TyPairN fts }
      | TyString
      | TyNat
      | TyInt
      | TyBytes
      | TyBool
      | TyUnit
      | TyMutez
      | TyKeyHash
      | TyTimestamp
      | TyAddress
      | TyChainID
      | TyKey
      | TySignature
      | TyOperation
      | TyNever
      | TyBLS12_381_Fr
      | TyBLS12_381_G1
      | TyBLS12_381_G2
      | TySapling_state _
      | TySapling_transaction _
        -> t.desc
      | TyList t -> TyList (type_ t)
      | TyOption (f, t) -> TyOption (f, type_ t)
      | TyOr (f1, t1, f2, t2) -> TyOr (f1, type_ t1, f2, type_ t2)
      | TySet t -> TySet (type_ t)
      | TyMap (t1, t2) -> TyMap (type_ t1, type_ t2)
      | TyBigMap (t1, t2) -> TyBigMap (type_ t1, type_ t2)
      | TyContract t -> TyContract (type_ t)
      | TyLambda (t1, t2) -> TyLambda (type_ t1, type_ t2)
      | TyTicket t -> TyTicket (type_ t)
    in
    { t with desc }

  let _test () =
    let open Michelson.Type in
    let report t = Format.eprintf "%a@." pp t in
    report & type_ (tyList (tyPair (None, tyString, None, tyPair (None, tyString, None, tyString))));
    report & type_ (tyList (tyPair (Some "a", tyString, None, tyPair (Some "b", tyString, Some "c", tyString))));
    report & type_ (tyList (tyPair (Some "a", tyString, Some "aa", tyPair (Some "b", tyString, Some "c", tyString))));
    report & type_ (tyList (tyPair (Some "a", tyString,
                                    None, tyPairN [(Some "b", tyString);
                                                   (Some "c", tyString);
                                                   (None, tyPairN [(Some "d", tyString);
                                                                   (None, tyPair (Some "e", tyString,
                                                                                  Some "f", tyString))])])))

  let rec constant c =
    let open Michelson.Constant in
    (* scrape the right combs *)
    let rec scrape t =
      match t with
      | Pair (t1, t2) ->
          begin match scrape t2 with
          | [] -> [t1; t2]
          | ts -> t1::ts
          end
      | PairN ts ->
          let rec f = function
            | [] -> assert false
            | [t] ->
                begin match scrape t with
                | [] -> [t]
                | ts -> ts
                end
            | t::ts -> t::f ts
          in
          f ts
      | _ -> []
    in
    let rec unscrape = function
      | [] | [_] -> assert false
      | [t1; t2] -> Pair (t1, t2)
      | t::ts -> Pair (t, unscrape ts)
    in
    let mayPairN t =
      match scrape t with
      | [] -> assert false
      | ts ->
          (* Currently, PairN is less efficient when #ts < 5 *)
          if List.length ts < 5 then
            unscrape ts
          else
            PairN ts
    in
    match c with
    | Pair (t1, t2) ->
        let t1 = constant t1 in
        let t2 = constant t2 in
        mayPairN (Pair (t1, t2))
    | PairN ts ->
        let ts = List.map constant ts in
        mayPairN (PairN ts)
    | List ts -> List (List.map constant ts)
    | Set ts -> Set (List.map constant ts)
    | Map tts -> Map (List.map (fun (t1,t2) -> constant t1, constant t2) tts)
    | Left t -> Left (constant t)
    | Right t -> Right (constant t)
    | Option (Some t) -> Option (Some (constant t))
    | Code ops -> Code ops (* not optimize here *)
    | Option None
    | Unit
    | Bool _
    | Int _
    | String _
    | Bytes _
    | Timestamp _ -> c

  module StackOpsOptimizer = struct
    (* returns (a,b) where a @ b == l /\ length a == n *)
    let rec _split_n n l =
      if n = 0 then [], l
      else
        match l with
        | x :: xs ->
            let rem, xs = _split_n (n - 1) xs in
            x :: rem, xs
        | [] -> assert false

    module Stack = struct
      module Expansion = struct
        (* Stack for expansion.  Good to see the area of search
           but probably slower to execute *)

        type stack = stack_desc ref
        and stack_desc =
          | Tail
          | Cons of value * stack
        and value = value_desc ref
        and value_desc =
          | Untouched
          | Touched of int
          | Pair of [`Normal | `New] * value * value

        let rec pp_value ppf v =
          let f fmt = Format.fprintf ppf fmt in
          match !v with
          | Untouched -> f "?"
          | Touched n -> f "%d" n
          | Pair (flag, v1, v2) ->
              f "%s(%a,%a)" (if flag = `New then "!" else "") pp_value v1 pp_value v2

        let rec _pp_stack ppf v =
          let f fmt = Format.fprintf ppf fmt in
          match !v with
          | Tail -> f ".."
          | Cons (v, stack) ->
              f "%a,%a" pp_value v _pp_stack stack

        let rec _length stack =
          match !stack with
          | Tail -> 0
          | Cons (_, stack) -> _length stack + 1

        let cntr_touched = ref (-1)
        let reset_cntr_touched () = cntr_touched := (-1)

        let make_touched () =
          incr cntr_touched;
          Touched !cntr_touched

        let make_value () = ref (make_touched ())

        let init () = ref Tail

        let touch v =
          match !v with
          | Untouched -> v := make_touched ()
          | _ -> ()

        let cons (x, xs) = ref (Cons (x, xs))

        let snoc stack =
          match !stack with
          | Tail ->
              let hd = ref Untouched in
              touch hd; (* XXX *)
              let tl = init () in
              stack := Cons (hd, tl);
              Some (hd, tl)
          | Cons (hd, tl) -> Some (hd, tl)

        (*
          let pair v1 v2 =
            ref (Pair (`Normal, v1, v2))
        *)

        let new_pair v1 v2 =
          ref (Pair (`New, v1, v2))

        let unpair v =
          touch v;
          match !v with
          | Untouched -> assert false
          | Pair (_, v1, v2) -> Some (v1, v2)
          | Touched _n ->
              let v1 = ref @@ make_touched () in
              let v2 = ref @@ make_touched () in
              v := Pair (`Normal, v1, v2);
              Some (v1, v2)

        (* let value v = ref (Touched v) *)

      end

      module Static = struct
        (* Stack not for expansion.  Stacks and values are not expandable,
           but probably faster to execute *)

        type stack = value list
        and value =
          | Value of int
          | Pair of [`Normal | `New] * value * value

        module ValueSet = Set.Make(struct
            type t = value
              let compare = compare
            end)

        let rec constants_in_value = function
          | Value i -> IntSet.singleton i
          | Pair (_, v1, v2) -> IntSet.union (constants_in_value v1) (constants_in_value v2)

        let constants_in_stack =
          List.fold_left (fun s v -> IntSet.union s (constants_in_value v)) IntSet.empty

        (* `New flag is reset to `Normal *)
        let rec pairs_in_value = function
          | Value _ -> ValueSet.empty
          | Pair (_, v1, v2) ->
              ValueSet.add (Pair (`Normal, v1, v2)) (ValueSet.union (pairs_in_value v1) (pairs_in_value v2))

        let pairs_in_stack =
          List.fold_left (fun set x -> ValueSet.union set (pairs_in_value x)) ValueSet.empty

        let rec of_expansion s =
          match !s with
          | Expansion.Tail -> []
          | Cons (v, s) -> of_evalue v :: of_expansion s

        and of_evalue v =
          match !v with
          | Untouched -> assert false
          | Touched i -> Value i
          | Pair (flag, v1, v2) -> Pair (flag, of_evalue v1, of_evalue v2)

        let rec pp_value ppf v =
          let f fmt = Format.fprintf ppf fmt in
          match v with
          | Value n -> f "%d" n
          | Pair (flag, v1, v2) ->
              f "%s(%a,%a)" (if flag = `New then "!" else "") pp_value v1 pp_value v2

        let rec pp_stack ppf v =
          let f fmt = Format.fprintf ppf fmt in
          match v with
          | [] -> f ".."
          | v :: stack ->
              f "%a,%a" pp_value v pp_stack stack

        let rec positions_of_value = function
          | Value _ -> 1
          | Pair (_, v1, v2) -> positions_of_value v1 + positions_of_value v2

        let positions_of_stack = List.fold_left (fun acc v -> acc + positions_of_value v) 0

        (* let length = List.length *)

        let touch _v = ()

        let cons (x, xs) = x :: xs

        let snoc stack =
          match stack with
          | [] -> None
          | hd::tl -> Some (hd, tl)

        (* let pair v1 v2 = Pair (`Normal, v1, v2) *)

        let new_pair v1 v2 = Pair (`New, v1, v2)

        let unpair v =
          match v with
          | Pair (_, v1, v2) -> Some (v1, v2)
          | Value _ -> None

        (* let value v = Value v *)
      end
    end

    module StackOp = struct
      type 'v t =
        | SO_PUSH of 'v
        | SO_DIG of int
        | SO_DUG of int
        | SO_DUP of int
        | SO_DROP of int
        | SO_DIP of int
        | SO_PID
        | SO_CAR
        | SO_CDR
        | SO_PAIR
        | SO_UNPAIR

      let rec pp pp_v ppf =
        let f fmt = Format.fprintf ppf fmt in
        function
        | SO_DUP n -> f "DUP %d;" n
        | SO_DUG n -> f "DUG %d;" n
        | SO_DIG n -> f "DIG %d;" n
        | SO_DROP n -> f "DROP %d;" n
        | SO_PUSH v -> f "PUSH %a;" pp_v v
        | SO_DIP n -> f "DIP %d { " n
        | SO_PID -> f " }"
        | SO_CAR -> f "CAR;"
        | SO_CDR -> f "CDR;"
        | SO_PAIR -> f "PAIR;"
        | SO_UNPAIR -> f "UNPAIR;"

      and pp_list pp_v ppf ops = Format.list "@ " (pp pp_v) ppf ops
    end

    open StackOp

    let convert_ops ops =
      let comments = ref [] in
      let env = ref [] in
      let rec f ({desc} as op) =
        if not & is_pure_push desc then comments := op.comments :: !comments ;
        match desc with
        | DUP -> [SO_DUP 1]
        | DUPn n -> [SO_DUP n]
        | DIP ops -> SO_DIP 1 :: g ops @ [SO_PID]
        | DIPn (n, ops) -> SO_DIP n :: g ops @ [SO_PID]
        | DIG n -> [SO_DIG n]
        | DUG n -> [SO_DUG n]
        | DROP -> [SO_DROP 1]
        | DROPn n -> [SO_DROP n]
        | SWAP -> [SO_DIG 1]
        | CAR -> [SO_CAR]
        | CDR -> [SO_CDR]
        | PAIR _ss -> [SO_PAIR]
        | UNPAIR -> [SO_UNPAIR]
        | desc when is_pure_push desc ->
            let v = Stack.Expansion.make_value () in
            env := (v,op) :: !env ;
            [SO_PUSH v]
        | _ -> assert false
      and g ops =
        (* f has side effects and must be applied from head to tail *)
        List.concat @@ List.map f ops
      in
      let ops =
        (* SCaml uses {} to carry comments *)
        match ops with
        | ({desc = BLOCK []} as op) :: ops ->
            comments := op.comments :: !comments ;
            ops
        | _ -> ops
      in
      let ops = g ops in
      List.rev !env, List.concat @@ List.rev !comments, ops

    let is_stack_op op =
      try
        let _ = convert_ops [op] in
        true
      with Assert_failure _ -> false

    module Emulation = struct

      module Make(A : sig
          type value
          type stack

          val cons : value * stack -> stack
          (* val value : int -> value *)
          val snoc : stack -> (value * stack) option
          val touch : value -> unit
          (* val pair : value -> value -> value *)
          val new_pair : value -> value -> value
          val unpair : value -> (value * value) option
        end) = struct

        open A

        let snoc' s = match snoc s with None -> assert false | Some (hd,tl) -> hd, tl
        let unpair' v = match unpair v with None -> assert false | Some (x,y) -> x, y

        let rec interps (stack, dips) ops = List.fold_left interp (stack, dips) ops
        and interp (stack, dips) op =
          match op with
          | SO_PUSH v -> cons (v, stack), dips
          | SO_DROP 0 -> stack, dips
          | SO_DROP n ->
              assert (n > 0);
              let hd, tl = snoc' stack in
              touch hd;
              interp (tl, dips) (SO_DROP (n-1))
          | SO_DUP n ->
              assert (n > 0);
              let rec get stack n =
                let hd, tl = snoc' stack in
                match n with
                | 1 ->
                    touch hd;
                    hd
                | n ->
                    get tl (n-1)
              in
              let v = get stack n in
              cons (v, stack), dips
          | SO_DIG n ->
              assert (n >= 0);
              let rec dig rev_prevs stack = function
                | 0 ->
                    let hd, tl = snoc' stack in
                    touch hd;
                    cons (hd, List.fold_left (fun stack prev -> cons (prev, stack)) tl rev_prevs)
                | n ->
                    let hd, tl = snoc' stack in
                    dig (hd::rev_prevs) tl (n-1)
              in
              dig [] stack n, dips
          | SO_DUG n ->
              assert (n >= 0);
              let v, tl = snoc' stack in
              touch v;
              let rec dug rev_prevs stack = function
                | 0 ->
                    List.fold_left (fun stack prev -> cons (prev, stack))
                      (cons (v, stack)) rev_prevs
                | n ->
                    let hd, tl = snoc' stack in
                    dug (hd :: rev_prevs) tl (n-1)
              in
              dug [] tl n, dips
          | SO_DIP n ->
              assert (n >= 0);
              let rec getn stack = function
                | 0 -> [], stack
                | n ->
                    let hd, tl = snoc' stack in
                    let hds, stack = getn tl (n-1) in
                    hd::hds, stack
              in
              let dip, stack = getn stack n in
              (stack, dip::dips)
          | SO_PID ->
              begin match dips with
              | [] -> assert false
              | dip::dips ->
                  let rec list stack = function
                    | [] -> stack
                    | hd::hds -> cons (hd, list stack hds)
                  in
                  list stack dip, dips
              end
          | SO_PAIR ->
              let hd1, tl = snoc' stack in
              let hd2, tl = snoc' tl in
              cons (new_pair hd1 hd2, tl), dips
          | SO_UNPAIR ->
              let hd, tl = snoc' stack in
              let v1, v2 = unpair' hd in
              cons (v1, cons (v2, tl)), dips
          | SO_CAR ->
              let hd, tl = snoc' stack in
              let v1, _v2 = unpair' hd in
              cons (v1, tl), dips
          | SO_CDR ->
              let hd, tl = snoc' stack in
              let _v1, v2 = unpair' hd in
              cons (v2, tl), dips

        let drop_only ~before ~after =
          let get_tops s =
            let rec f s =
              match snoc s with
              | None -> []
              | Some (v, s) -> v :: f s
            in
            f s
          in
          let before = get_tops before in
          (* The elems of before must be distinct *)
          assert (List.length before = List.length (List.sort_uniq compare before));
          let after = get_tops after in
          let rec check before after =
            match before, after with
            | _, [] -> true
            | [], _ -> false
            | x::xs, y::ys when x = y -> check xs ys
            | _::xs, ys -> check xs ys
          in
          if check before after then Some (before, after) else None
        end

    end

    module Expansion = struct
      include Stack.Expansion
      include Emulation.Make(Stack.Expansion)
    end

    module Static = struct
      include Stack.Static
      include Emulation.Make(Stack.Static)
    end

    let convert_then_interp_ops ops =
      (* Interp with stack+value expansion *)
      Expansion.reset_cntr_touched ();
      let env, comments, ops = convert_ops ops in
      let start = Expansion.init () in
      let goal, dips = Expansion.interps (start,[]) ops in
      assert (dips = []);
      let start = Static.of_expansion start in
      let goal = Static.of_expansion goal in
      let ops = List.map (function
          | SO_PUSH v -> SO_PUSH (Static.of_evalue v)
          | SO_DUP n -> SO_DUP n
          | SO_DUG n -> SO_DUG n
          | SO_DIG n -> SO_DIG n
          | SO_DROP n -> SO_DROP n
          | SO_DIP n -> SO_DIP n
          | SO_PID -> SO_PID
          | SO_CAR -> SO_CAR
          | SO_CDR -> SO_CDR
          | SO_PAIR -> SO_PAIR
          | SO_UNPAIR -> SO_UNPAIR) ops
      in
      (* What Opcode to be used to push a value? *)
      let env =
        let rec f = function
          | [] -> []
          | (v,op)::env ->
              let vops =
                match !v, op.desc with
                | Stack.Expansion.Touched i, _ ->
                    [Stack.Static.Value i, op]
                | _, PUSH (ty,c)  ->
                    let rec g v ty c =
                      match !v, ty.Michelson.Type.desc, c with
                      | Stack.Expansion.Touched i, _, c ->
                          let v = Stack.Static.Value i in
                          v, [Stack.Static.Value i, _PUSH (ty, c)]
                      | Pair (_, v1, v2),
                        TyPair (_, ty1, _, ty2),
                        Michelson.Constant.Pair (c1, c2) ->
                          let v1, vops1 = g v1 ty1 c1 in
                          let v2, vops2 = g v2 ty2 c2 in
                          let v = Stack.Static.Pair (`New, v1, v2) in
                          v, (v, _PUSH (ty, c)) :: vops1 @ vops2
                      | _ -> assert false (*oups *)
                    in
                    snd (g v ty c)
                | _ -> assert false (* non PUSH pushes a tuple? *)
              in
              vops @ f env
        in
        f env
      in
      env, comments, start, goal, ops

    let restore_ops env comments ops =
      let rec f acc acc' ops =
        match ops with
        | [] -> assert (acc' = []); List.rev_map (fun desc -> { desc; comments= [] }) acc
        | SO_DUP 1 :: ops -> f (DUP :: acc) acc' ops
        | SO_DUP n :: ops -> f (DUPn n :: acc) acc' ops
        | (SO_DIG 1 | SO_DUG 1) :: ops -> f (SWAP :: acc) acc' ops
        | SO_DIG n :: ops -> f (DIG n :: acc) acc' ops
        | SO_DUG n :: ops -> f (DUG n :: acc) acc' ops
        | SO_DROP 1 :: ops -> f (DROP :: acc) acc' ops
        | SO_DROP n :: ops -> f (DROPn n :: acc) acc' ops
        | SO_PUSH v :: ops -> f ((List.assoc v env).desc :: acc) acc' ops
        | SO_PID :: ops ->
            begin match acc' with
              | [] -> assert false
              | (1,rops)::acc' ->
                  f (DIP (List.rev_map (fun desc -> { desc; comments= [] }) acc)::rops) acc' ops
              | (n,rops)::acc' ->
                  f (DIPn (n, List.rev_map (fun desc -> { desc; comments= [] }) acc)::rops) acc' ops
            end
        | SO_DIP n :: ops -> f [] ((n,acc)::acc') ops
        | SO_CAR :: ops -> f (CAR :: acc) acc' ops
        | SO_CDR :: ops -> f (CDR :: acc) acc' ops
        | SO_PAIR :: ops -> f (PAIR [] :: acc) acc' ops
        | SO_UNPAIR :: ops -> f (UNPAIR :: acc) acc' ops
      in
      match f [] [] ops with
      | o::os -> prepend_comments comments o :: os
      | [] -> [] (* XXX comment lost! *)

    let rec _pp_stack ppf = function
      | [] -> Format.fprintf ppf ".."
      | x::xs ->
          Format.fprintf ppf "%d,%a" x _pp_stack xs

    (* size vs gas can be tweaked here *)
    let op_cost op =
      let op_size = function
        | SO_DIP 1 ->
            (* tag : 1
               dip : 1
               tag : 1 for seq
               length : 4
            *)
            7
        | SO_DIP _ ->
            (* tag : 1
               dip : 1
               int : 2
               tag : 1 for seq
               length : 4
            *)
            9
        | SO_PID -> 0
        | SO_DUP 1 -> 2
        | SO_DUP _ -> 4
        | SO_DIG 1 | SO_DUG 1 -> 2 (* It is SWAP *)
        | SO_DIG _ -> 4
        | SO_DUG _ -> 4

        (* Tweak:

           The real size of DROP 1 is 2 bytes.  Therefore,

             size (DROP 1; DROP 1) = size (DROP 2) = 4bytes
             gas (DROP 1; DROP 1) = 10 = 0.010 gas
             gas (DROP 2) = 65 = 0.065 gas

           If we only consider the size and the gas cost to run the opcodes,
           DROP 1; DROP 1 is better than DROP 2 !!

           However, the gas of typechecking of DROP 1; DROP 1 is much larger
           than DROP 2:

             typecheck_gas(DROP 1; DROP 1) - typecheck_gas(DROP 2) = 0.340 gas

           We intentionally set the size of DROP 1 to 3 to prevent the rewriting
             DROP 2 => DROP 1; DROP 1
        *)
        | SO_DROP 1 -> 3 (* Actually it is 2.  See the above comment *)
        | SO_DROP _ -> 4

        | SO_PUSH _ ->
            (* it can be anything since PUSH cannot be removed unless DROPed immediately *)
            2
        | SO_PAIR | SO_UNPAIR | SO_CAR | SO_CDR -> 2
      in

      (* defined in michelson_v1_gas.ml commit bccabda848d68db3c28ac312bc5347fc9dccc2d8 *)
      let op_gas = function
        | SO_DIP 1 -> 15
        | SO_DIP n -> 45 + (n + (n lsl 1) + (n lsr 3))
        | SO_PID -> 0
        | SO_DROP 1 -> 10
        | SO_DROP n -> 60 + 2 * n + n lsr 1 + n lsr 3
        | SO_DUG n -> 60 + 6 * n + n lsr 1 + n lsr 2
        | SO_DIG n -> 60 + 6 * n + n lsr 1 + n lsr 2
        | SO_DUP 1 -> 10
        | SO_DUP n -> 20 + n + n lsr 3
        | SO_PUSH _ (* IConst *) -> 10
        | SO_PAIR -> 15
        | SO_UNPAIR -> 10
        | SO_CAR -> 10
        | SO_CDR -> 10
      in
      op_size op * 1000 + op_gas op

    let ops_cost ops = List.fold_left ( + ) 0 (List.map op_cost ops)

    let search_space_limit_log =
      let search_space_limit = Conf.search_space_multiplier *. 10000.0 in
      Stdlib.log search_space_limit

    let enumerate_next_operations push_ops stack dips pairs_in_goal ~last_op =
      let l = List.length stack in
      (* DIG/DUG: [1..l-1] *)
      (if l > 1 then
         List.concat (List.init (l-1) (fun n ->
             let n = n + 1 in
             (* DIG 1 = DUG 1 = SWAP *)
             if n = 1 then [SO_DIG 1] else [SO_DIG n; SO_DUG n]
           ))
       else [])
      (* DUP/DROP: [1..l] *)
      @ List.concat (List.init l (fun n -> let n = n + 1 in [SO_DUP n; SO_DROP n]))
      (* PID, if dips <> [] and last_op is not DIP *)
      @ (if dips <> [] && (match last_op with Some (SO_DIP _) -> false | _ -> true) then [SO_PID] else [])
      (* DIP [1..l] *)
      @ List.init l (fun n -> let n = n + 1 in SO_DIP n)
      (* PUSH *)
      @ push_ops
      (* CAR/CDR/UNPAIR *)
      @ (match stack with
         | Static.Pair (_, _, _) :: _ -> [SO_CAR; SO_CDR; SO_UNPAIR]
         | _ -> [])
      (* PAIR *)
      @ (match stack with
          | v1 :: v2 :: _ when
              (* pairs are all reset to `Normal in pairs_in_goal *)
              Static.ValueSet.mem (Static.Pair (`Normal, v1, v2)) pairs_in_goal ->
              [SO_PAIR]
          | _ -> [])

    type node = {
      (* cost of start -> stack -> goal.  The gost from stack to goal is estimated one. *)
      cost_to_goal_estimated : int;

      (* cost of start -> stack *)
      cost_to_stack : int;

      current_rev_ops : Static.value StackOp.t list;

      (* PUSHes must be used *)
      push_ops : Static.value StackOp.t list;

      stack : Static.stack;

      dips : Static.stack list;
    }

    module PriorityQueue = Binary_heap.Make (struct
        type t = node
        let compare t1 t2 = compare t1.cost_to_goal_estimated t2.cost_to_goal_estimated
      end)

    module StateMap = Map.Make (struct
        type t = Static.stack * Static.stack list
        let compare = compare
      end)

    (* XXX We don't use it since it is specialized for int *)
    let _longest_common_subsequence =
      let dp = Array.make (30*30) 0 in
      let get w y x =
        Array.unsafe_get dp (w*y + x)
      in
      let set w y x v =
        Array.unsafe_set dp (w*y + x) v
      in
      fun s t ->
        (* Format.eprintf "s : %a :: t : %a@." _pp_stack s _pp_stack t; *)
        let ls = List.length s in
        let lt = List.length t in
        assert ((ls+1) * (lt+1) < 30 * 30);
        let get = get (lt+1) in
        let set = set (lt+1) in
        for is = 0 to ls do
          for it = 0 to lt do
            set is it (-1)
          done;
        done;
        let rec aux (s,ls) (t,lt) =
          let v = get ls lt in
          if v >= 0 then v else begin
            let l =
              match s,t with
              | x :: xs, y :: ys ->
                  max (if x = y then aux (xs,ls-1) (ys,lt-1) + 1 else 0)
                    @@ max (aux (s,ls) (ys,lt-1)) (aux (xs,ls-1) (t,lt))
              | _, [] | [],_ -> 0
            in
              (* Format.eprintf "lcs : %d : %d :: %d@." ls lt l; *)
              set ls lt l;
              l
            end
        in
          aux (s, ls) (t, lt)

    let cost_push = op_cost (SO_PUSH 0)
    let _cost_dup = op_cost (SO_DUP 1)
    let _cost_move = min (op_cost (SO_DUG 1)) (op_cost (SO_DIG 1))
    let _cost_drop n = if n <= 0 then 0 else op_cost (SO_DROP n)

    let test_astar_score () = ()
(*
    let test_astar_score () =
      (* assumptions for the cost function *)
      assert (cost_push > cost_dup || true); (* XXX currently broken *)
      assert (cost_dup + cost_drop 1 > cost_move);
      assert (cost_dup + cost_drop 2 < cost_move + cost_drop 1 || true); (* XXX currently broken *)
      assert (cost_move > cost_dup);
      for i = 0 to 5 do
        for j = 0 to 5 do
          let mc =
            if i = 0 && j = 1 then
                cost_drop i + cost_move * j
            else
              cost_drop (i + j) + cost_dup * j in
          (* let c1 = cost_drop i + cost_move * j in
          let c2 = cost_drop (i + j) + cost_dup * j in
          Format.eprintf "%d %d : c1 %s c2@." i j (if c1 < c2 then "<" else if c1 > c2 then ">" else "=");
          let mc' = min c1 c2 in
          assert (mc = mc'); *)
          for k = 0 to j do
            let c = cost_drop (i + k) + cost_move * (j - k) + cost_dup * k in
            assert (mc <= c || true) (* XXX currently broken *)
          done;
        done;
      done

    (* this score function should satisfy
       0 <= score(s,t) <= best opcost from stack s to stack t *)
    let astar_score ~max_var_idx =
      let new_vars_slower t =
        let unique_t = List.unique t in
        fun s ->
        let rec aux acc t =
          match t with
          | [] -> acc
          | x :: xs ->
            aux (if List.mem x s then 0 else 1 + acc) xs
        in
          aux 0 unique_t
      in
      let new_vars_faster =
        let popcount =
          let v1 = 0x55555555 in
          let v2 = 0x33333333 in
          let v3 = 0x0f0f0f0f in
          let v4 = 0x00ff00ff in
          let v5 = 0x0000ffff in
          fun x ->
            let x = x land v1 + ((x lsr 1) land v1) in
            let x = x land v2 + ((x lsr 2) land v2) in
            let x = x land v3 + ((x lsr 4) land v3) in
            let x = x land v4 + ((x lsr 8) land v4) in
            let x = x land v5 + ((x lsr 16) land v5) in
            x
        in
        let to_bits l =
          List.fold_left (fun acc i -> acc lor (1 lsl i)) 0 l
        in
        fun t ->
          let t = to_bits t in
          let nt = popcount t in
          fun s ->
            let s = to_bits s in
            nt - popcount (s land t)
      in
      let new_vars = if max_var_idx >= 30 then new_vars_slower else new_vars_faster in
      (* For each A* search, same `t` (goal) is used for calculating score.
         Thus we can precalculate according to `t` to reduce calculation time. *)
      fun ~to_stack:t ->
        (* Format.eprintf "new_to_stack@."; *)
        let lt = List.length t in
        let new_vars = new_vars t in
        let tbl = Hashtbl.create 100 in
        fun ~from_stack:s ->
          (* Format.eprintf "%a -> %a@." _pp_stack s _pp_stack t; *)
          match Hashtbl.find_opt tbl s with
          | Some v -> v
          | None ->
            let ls = List.length s in
            let new_vars = new_vars s in
            let lt = lt - new_vars in
            let l =
              if Conf.use_lcs_for_astar_score && false then
                longest_common_subsequence s t
              else min ls lt
            in
            (* Format.eprintf "ls %d : lt %d : %d : %d@." ls lt new_vars l; *)
            let move_vars = min lt ls - l in
            let drop_vars = ls - l - move_vars in
            let dup_vars = lt - l - move_vars in
            assert (move_vars >= 0);
            let res =
              cost_push * new_vars + (
                if drop_vars = 0 && move_vars = 1 then
                  cost_move + cost_dup * dup_vars
                else
                  cost_drop (drop_vars + move_vars) + cost_dup * (dup_vars + move_vars)
              )
              (* min
                (cost_move * move_vars + cost_drop drop_vars + cost_dup * dup_vars)
                (cost_drop (drop_vars + move_vars) + cost_dup * (dup_vars + move_vars))  *)
            in
            assert (if s = t then res = 0 else true);
            Hashtbl.add tbl s res;
            (* Format.eprintf "new %d :: move %d :: drop %d@." new_vars move_vars drop_vars; *)
            res
*)

    (* For each A* search, same `t` (goal) is used for calculating score.
       Thus we can precalculate according to `t` to reduce calculation time. *)
    let astar_score ~to_stack:t =
      (* Format.eprintf "new_to_stack@."; *)
      let constants_t = Static.constants_in_stack t in
      let tbl = Hashtbl.create 100 in
      fun ~from_stack:s ->
        (* Format.eprintf "%a -> %a@." _pp_stack s _pp_stack t; *)
        match Hashtbl.find_opt tbl s with
        | Some v -> v
        | None ->
            let constants_s = Static.constants_in_stack s in
            let constants_to_add = IntSet.diff constants_t constants_s in
            let res = IntSet.cardinal constants_to_add * cost_push in
            Hashtbl.add tbl s res;
            (* Format.eprintf "new %d :: move %d :: drop %d@." new_vars move_vars drop_vars; *)
            res

    (* 2. Brute-force search.

       Dijkstra's algorithm with the following constraints:

       * Stack length must not exceed the max length of the start and the goal.
       * Cost does not exceed the cost of the original ops
    *)
    let brute_force_search
        ~start:(start : Static.stack)
        ~goal:(goal : Static.stack)
        ~input_ops
        ~input_ops_cost
        ~astar_score =
      let loop_count = ref 0 in
      let constants_num =
        let stack_constants =
          IntSet.union
            (Static.constants_in_stack start)
            (Static.constants_in_stack goal)
        in
        IntSet.(cardinal stack_constants)
      in
      let max_stack_len =
        let start_len = List.length start in
        let goal_len = List.length goal in
        max start_len goal_len
      in
      let max_stack_positions =
        let start_positions = Static.positions_of_stack start in
        let goal_positions = Static.positions_of_stack goal in
        max start_positions goal_positions
      in
      let pairs_in_goal = Static.pairs_in_stack goal in
      (* Check the search space size.  The size may be under estimated *)
      if Float.(of_int max_stack_positions *. log (of_int constants_num) > search_space_limit_log)
      then
        (* stack is too deep to complete in practical time *)
        `Too_Deep
      else begin
        if !debug then logf "@[<2>Search @[%a@] => @[%a@]@ of @[%a@]@]"
            Static.pp_stack start Static.pp_stack goal
            (StackOp.pp_list Static.pp_value) input_ops;
        let push_ops =
            (* Since the search cost of PUSH is high,
               we can assume that only the necessary variables are PUSHed. *)
            List.map (fun i -> SO_PUSH (Static.Value i))
              IntSet.(to_list @@ diff (Static.constants_in_stack goal) (Static.constants_in_stack start))
        in
        (* search best cost operations by A* algorithm *)
        let astar_score = astar_score ~to_stack:goal in

        let priority_queue =
          PriorityQueue.create
            ~dummy: { cost_to_goal_estimated= 0; cost_to_stack= 0; current_rev_ops= []; push_ops= []; stack= []; dips= [] }
            0
        in
        PriorityQueue.add priority_queue
          { cost_to_goal_estimated= astar_score ~from_stack:start;
            cost_to_stack= 0;
            current_rev_ops= [];
            push_ops;
            stack= start;
            dips= [] } ;

        let cost_map = ref (StateMap.singleton (start, []) 0) in

        let rec check_equal ~goal stack =
          (* goal = stack, but Pair(`Normal,_,_) in goal can be Pair(`New,_,_) in stack
                               Pair(`New,_,_) in goal must be Pair(`New,_,_) in stack
          *)
          match goal, stack with
          | [], [] -> true
          | [], _ | _, [] -> false
          | g::gs, s::ss ->
              let rec eq_value g s =
                match g, s with
                | Static.Value i, Static.Value j -> i = j
                | Pair (`Normal, g1, g2), Pair (_, s1, s2) ->
                    eq_value g1 s1 && eq_value g2 s2
                | Pair (`New, g1, g2), Pair (`New, s1, s2) ->
                    eq_value g1 s1 && eq_value g2 s2
                | _ -> false
              in
              eq_value g s && check_equal ~goal:gs ss
        in

        let rec loop () =
          incr loop_count;
          if PriorityQueue.is_empty priority_queue then
            (* input_ops is the best *)
            None
          else
            let { cost_to_goal_estimated= _;
                  cost_to_stack;
                  current_rev_ops;
                  push_ops;
                  stack;
                  dips } =
              PriorityQueue.pop_minimum priority_queue
            in
            if cost_to_stack > StateMap.find (stack, dips) !cost_map then
              (* Better path to stack known *)
              loop ()
            else if
              check_equal ~goal stack && dips = [] (* reached to the goal *)
            then
              (* Solution found *)
              Some (List.rev current_rev_ops)
            else begin
              let push_queue op =
                let push_ops =
                  match op with
                  | SO_PUSH _ ->
                      (* slightly faster than List.filter *)
                      let rec remove = function
                        | [] -> []
                        | x::xs when x = op -> xs
                        | x::xs -> x :: remove xs
                      in
                      remove push_ops
                  | _ -> push_ops
                in
                let stack', dips' = Static.interp (stack, dips) op in
                (* XXX UNPAIR or DUP at the head increases the stack size.  We should relax max_stack_len *)
                if List.length stack' > max_stack_len then
                  (* stack over *)
                  ()
                else
                  let cost_to_stack' = cost_to_stack + op_cost op in
                  if input_ops_cost <= cost_to_stack' then
                    (* worse than or equal to the input *)
                    ()
                  else if
                    match StateMap.find_opt (stack', dips') !cost_map with
                    | None -> false
                    | Some c -> c <= cost_to_stack'
                  then
                    (* better path known to stack' *)
                    ()
                  else begin
                    cost_map := StateMap.add (stack', dips') cost_to_stack' !cost_map ;
                    let stack'' =
                      let rec f stack'' = function
                        | [] -> stack''
                        | dip::dips -> f (dip@stack'') dips
                      in
                      f stack' dips'
                    in
                    let s = astar_score ~from_stack:stack'' in
                    let cost_to_goal_estimated = cost_to_stack' + s in
                    PriorityQueue.add priority_queue
                      { cost_to_goal_estimated;
                        cost_to_stack= cost_to_stack';
                        current_rev_ops= op :: current_rev_ops;
                        push_ops;
                        stack= stack';
                        dips= dips'
                      }
                  end
              in
              let next_ops =
                enumerate_next_operations push_ops stack dips pairs_in_goal
                  ~last_op:(match current_rev_ops with x::_ -> Some x | [] -> None)
              in
              List.iter push_queue next_ops ;
              loop ()
            end
        in

        match loop () with
        | None ->
            `Already_Optimal
        | Some ops ->
            let ops_cost = ops_cost ops in
            if ops_cost = input_ops_cost then
              (* Returns the input if the cost is the same *)
              `Already_Optimal
            else begin
              (* Format.eprintf "stack operation optimization (is_astar: %b)succeeded@." is_astar;
              Format.eprintf "from_l %d : to_l %d@." from_l to_l;
              Format.eprintf "input_ops (cost %d):@.%a@." input_ops_cost StackOp.pp_list input_ops;
              Format.eprintf "resulted_ops (cost %d):@.%a@." converted_ops_cost StackOp.pp_list ops; *)
              assert (
                let res, dips = Static.interps (start, []) ops in
                check_equal ~goal res && dips = []
              ) ;
              assert (ops_cost < input_ops_cost) ;
              let comments =
                (*
                [
                  [
                    "optimized stack ops";
                    Format.sprintf "from: @[%a@]" StackOp.pp_list input_ops_converted;
                    Format.sprintf "to:   @[%a@]" StackOp.pp_list ops;
                  ];
                ]
                *)
                []
              in
              `Optimized (ops, comments, !loop_count)
            end
      end

    let brute_force_search ~start ~goal ~input_ops ~input_ops_cost =
(*
      if Conf.do_dijkstra then
        let res_astar =
          brute_force_search ~start ~goal ~input_ops ~used_items
            ~astar_score: astar_score
        in
        let res_dijkstra =
          brute_force_search ~start ~goal ~input_ops ~used_items
            ~astar_score:(fun ~max_var_idx:_ ~to_stack:_ ~from_stack:_ -> 0)
        in
        match res_dijkstra, res_astar with
        | `Optimized (ops_dijkstra, _, loops_dijkstra), `Optimized (ops_astar, comments, loops_astar) ->
            logf "dijkstra_loops %d : astar_loops %d" loops_dijkstra loops_astar;
            let cost_dijkstra = ops_cost ops_dijkstra in
            let cost_astar = ops_cost ops_astar in
            if cost_dijkstra <> cost_astar then begin
              Format.eprintf "input: @[%a@]@." StackOp.pp_list input_ops;
              Format.eprintf "start: %a@." pp_stack start;
              Format.eprintf "goal: %a@." pp_stack goal;
              Format.eprintf "dijkstra: %d @[%a@]@." cost_dijkstra StackOp.pp_list ops_dijkstra;
              Format.eprintf "astar: %d @[%a@]@." cost_astar StackOp.pp_list ops_astar;
              assert false
            end;
            `Optimized (ops_astar, comments)
        | `Optimized _, _ | _, `Optimized _ -> assert false
        (* | _, `Optimized (ops, comments, _) -> `Optimized (ops, comments)  *)
        | _, `Already_Optimal -> `Already_Optimal
        | _, `Too_Deep -> `Too_Deep
      else
        let res_astar =
          brute_force_search ~start ~goal ~input_ops ~used_items
            ~astar_score: astar_score
        in
        match res_astar with
        | `Optimized (ops_astar, comments, loops_astar) ->
            if !debug then
              logf "A* %d: @[@[%a@]@ @[=> @[%a@]@]@]"
                loops_astar StackOp.pp_list input_ops
                StackOp.pp_list ops_astar;
            `Optimized (ops_astar, comments)
        | `Already_Optimal -> `Already_Optimal
        | `Too_Deep -> `Too_Deep
*)
      let res =
        brute_force_search ~start ~goal ~input_ops ~input_ops_cost ~astar_score
      in
      match res with
      | `Optimized (ops, comments, nloops) ->
          if !debug then
            logf "A* %d  %d => %d : @[@[%a@]@ @[=> @[%a@]@]@]"
              nloops
              (ops_cost input_ops)
              (ops_cost ops)
              (StackOp.pp_list Static.pp_value) input_ops
              (StackOp.pp_list Static.pp_value) ops;
          `Optimized (ops, comments)
      | `Already_Optimal -> `Already_Optimal
      | `Too_Deep -> `Too_Deep

    (*
      DIP m { DROP 1 } ==> DIG m; DROP 1
      DIP 1 { DROP n } ==> DUG n; DROP n
      DIP m { DROP 2 } ==> DIG m; DIG (m+1); DROP 2 (when m >= 2)
      DIP 2 { DROP n } ==> DUG (n+1); DUG (n+1); DROP n;
      DIP m { DROP n } ==> DIP m { DROP n } (when m >= 3 and n >= 3)
    *)
    let optimal_ops_for_dip_drop m n =
      if n = 1 then [SO_DIG m; SO_DROP 1]
      else if m = 1 then [SO_DUG n; SO_DROP n]
      else if n = 2 then [SO_DIG m; SO_DIG (m+1); SO_DROP 2]
      else if m = 2 then [SO_DUG (n+1); SO_DUG (n+1); SO_DROP n]
      else [SO_DIP m; SO_DROP n; SO_PID]

    let test_optimal_ops_for_dip_drop () =
      let f = optimal_ops_for_dip_drop in
      for i = 1 to 10 do
        assert (ops_cost [SO_DROP 1; SO_DROP i] > ops_cost [SO_DROP (i+1)])
      done;
      for i = 1 to 10 do
        for j = 1 to 10 do
          let ops = [SO_DIP i; SO_DROP j; SO_PID] in
          let start = List.init (i+j) (fun i -> Static.Value i) in
          let goal, dips = Static.interps (start, []) ops in
          assert (dips = []);
          let input_ops_cost = ops_cost ops in
          let res = brute_force_search ~start ~goal ~input_ops:ops ~input_ops_cost in
          let best_ops =
            match res with
            | `Already_Optimal -> ops
            | `Too_Deep -> f i j
            | `Optimized (ops,_) -> ops
          in
          if ops_cost best_ops <> ops_cost (f i j) then begin
            logf
              ("optimal_ops_for_dip_drop changed@."
               ^^ "optimization for:@.%a@."
               ^^ "hardcoded(cost %d):@.%a@."
               ^^ "actual(cost %d):@.%a@.")
              (StackOp.pp_list Static.pp_value) ops
              (ops_cost @@ f i j) (StackOp.pp_list Static.pp_value) (f i j)
              (ops_cost best_ops) (StackOp.pp_list Static.pp_value) best_ops;
              assert false
            end else ()
          (* let s =
            match res with
            | `Already_Optimal -> "Opt"
            | `Too_Deep -> "Deep"
            | `Optimized (ops,_) -> Format.sprintf "%a" StackOp.pp_list ops
          in
            Format.eprintf "%a : %s@." StackOp.pp_list ops s *)
        done
      done

    (* 1. optimization for special case *)
    let dip_drop_optimization ~before ~after ~input_ops_cost =
      (* It assumes:
         * The elements of [before] are distinct.
         * [after] is obtained from [before] by dropping some elements *)
      (* Normalization.  Make [before] [0..n] then change [after]
         to match with the updated [before] *)
      let before = List.mapi (fun pos n -> (n, pos)) before in
      let after = List.map (fun n -> List.assoc n before) after in
      (* get_consecutive [d; d+1; ... d+n-1; e; ... ] ==> (n,(d+n-1,[e; ...])) *)
      let rec get_consecutive = function
        | x :: ((y :: _) as xs)  when x + 1 = y ->
            let (n,rem) = get_consecutive xs in
            (n+1,rem)
        | x :: xs -> (1,(x,xs))
        | [] -> assert false
      in
      (* build ops which drop elements: [hd..last] => stk *)
      let rec build_drops (hd,last) stk =
        match stk with
        | [] ->
            if hd > last then []
            else [`DROP (last-hd+1)]
        | (x :: _) as stk ->
            if x <> hd then
              `DROP (x-hd) :: build_drops (x,last) stk
            else begin
              let (d,(x,xs)) = get_consecutive stk in
              assert (x - d + 1 = hd);
              let drop = build_drops (x+1,last) xs in
              if drop = [] then []
              else [`DIP (d, drop)]
            end
      in
      let ops' = build_drops (0, List.length before - 1) after in
      let optimize_ops' =
        (* XXX : More optimization might be possible *)
        let rec aux acc_m ops' =
          match ops' with
          | [] -> []
          | `DROP n :: xs -> SO_DROP n :: aux acc_m xs
          | [`DIP (m,(`DROP n :: xs))] ->
              (* DIP m { DROP n; ... }  may not be optimal
                 because of the high cost of { }. *)
              let m = m + acc_m in
              let best_xs = aux m xs in
              let ops = optimal_ops_for_dip_drop m n in
              ops @ best_xs
          | _ -> assert false
        in
        aux 0
      in
      let ops' = optimize_ops' ops' in
      (* XXX warn if > *)
      if ops_cost ops' >= input_ops_cost then
        `Already_Optimal
      else
        let comments = [] in (* XXX *)
        `Optimized (ops', comments)

    let optimize_converted_ops start goal input_ops =
      let input_ops_cost = ops_cost input_ops in
      match Static.drop_only ~before:start ~after:goal with
      | None ->
          brute_force_search ~start ~goal ~input_ops ~input_ops_cost
      | Some (before, after) ->
          match dip_drop_optimization ~before ~after ~input_ops_cost with
          | `Already_Optimal -> `Already_Optimal
          | `Optimized (ops, _comments) as res  ->
              if !debug then
                logf "Drops: @[@[%a@]@ @[=> @[%a@]@]@]"
                  (StackOp.pp_list Static.pp_value) input_ops
                  (StackOp.pp_list Static.pp_value) ops;
              res

    let optimize_stack_operation_list =
      (* Memoization *)
      let cache = Hashtbl.create 100 in
      let hit = ref 0 in
      let called = ref 0 in
      fun ops ->
        let env, comments, start, goal, ops = convert_then_interp_ops ops in
        let res =
          incr called;
          match Hashtbl.find cache (start, goal) with
          | x ->
              incr hit;
              x
          | exception Not_found ->
              let res = optimize_converted_ops start goal ops in
              Hashtbl.add cache (start, goal) res;
              res
        in
        match res with
        | `Already_Optimal -> `Already_Optimal
        | `Too_Deep -> `Too_Deep
        | `Optimized (ops', comments') ->
            `Optimized (restore_ops env (comments @ comments') ops')

    (* try every part of stack operations *)
    let optimize_stack_operation_list ops =
      match ops with
      | [] | [_] -> ops
      | _ ->
          (* reversed-ops -> non-reversed-ops *)
          let rec aux rev_ops =
            (* Format.eprintf "Optimizing: @[%a@] =>@." (Format.list ";@ " pp)  (List.rev rev_ops); *)
            let ops = List.rev rev_ops in
            match optimize_stack_operation_list ops with
            | `Optimized ops' ->
                `Optimized ops'
            | `Already_Optimal ->
                `Already_Optimal ops
            | `Too_Deep ->
                (* retry prefix *)
                match rev_ops with
                | [] -> `Already_Optimal [] (* weird *)
                | op :: rev_ops ->
                    let (`Optimized ops | `Already_Optimal ops | `Partial ops) =
                      aux rev_ops
                    in
                    `Partial (ops @ [op])
          in
          let rec aux' ops =
            match aux (List.rev ops) with
            | `Partial (op :: ops) ->
                (* it is worth to optimize postfixes *)
                op :: aux' ops
            | `Optimized ops ->
                (* optimization succeeded *)
                ops
            | `Already_Optimal _ | `Partial [] ->
                (* no point to optimize postfixes *)
                ops
          in
          aux' ops

    let optimize_stack_operation_list_in_ops ops =
      let rev_ops = List.rev ops in
      (* get the longest sequences of stack ops and apply optimization to them *)
      let rec loop rev_ops stack_ops rems =
        match rev_ops with
        | [] -> optimize_stack_operation_list stack_ops @ rems
        | x :: xs when is_stack_op x ->
            loop xs (x :: stack_ops) rems
        | x :: xs ->
            loop xs [] (x :: optimize_stack_operation_list stack_ops @ rems)
      in
      loop rev_ops [] []
  end

  let length_of_get d =
    match d with CAR -> 1 | CDR -> 2 | GETn n -> n | _ -> assert false

  (* Comments must be preserved somehow *)
  let optimize os =
    let nop comments = set_comments (_BLOCK []) comments in
    let modified = ref false in
    let rec f rev_acc os =
      match os with
      | [] ->
          (* XXX modified flag? *)
          StackOpsOptimizer.optimize_stack_operation_list_in_ops (List.rev rev_acc)

      (* FAILWITH; ... => FAILWITH
         This is not really optimization but required to be valid *)
      | ({desc= FAILWITH} as o) :: _ -> recurse rev_acc o []

      (* comments *)

      (* { } no comment => empty *)
      | [{desc = BLOCK []; comments = []}] ->
          reapply rev_acc []
      (* { } /* comment */; OP  =>  OP /* comment */  *)
      | {desc = BLOCK []; comments} :: o :: os ->
          reapply rev_acc (prepend_comments comments o :: os)
      (* OP ; { } /* comment */ AT THE END => OP /* comment */ *)
      | [o; {desc = BLOCK []; comments}] ->
          reapply rev_acc [append_comments o comments]
      (* { os' } /* comment */ ; os  =>  os' /* comment */ ; os *)
      | {desc = BLOCK (_ :: _ as os'); comments} :: os ->
          reapply rev_acc & append_comments_at_last os' comments @ os

      (* PUSH *)

      (* pure opcode to push 1 item ; DROP n  =>  DROP (n-1) *)
      | ({desc} as o) :: ({desc = DROP} as drop) :: os when is_pure_push desc
        ->
          reapply rev_acc (nop (o.comments @ drop.comments) :: os)
      | ({desc} as o) :: ({desc = DROPn n} as drop) :: os when is_pure_push desc
        ->
          if n = 1 then reapply rev_acc (nop (o.comments @ drop.comments) :: os)
          else reapply rev_acc (prepend_comments o.comments {drop with desc = DROPn (n - 1)} :: os)

      (* IF* *)

      (* IF { } { } => DROP 1 *)
      | { desc = IF ([],[]) } :: os -> reapply rev_acc (_DROP :: os)

      (* IF { a; b } { a; b' } => DIP 1 { a }; IF { b } { b' }  <- this is correct

         We need types for the following:
         IF { a; b } { a'; b } => IF* { b } { b' }; b'  <- same code but may be typed differently
      *)
      | ({ desc = IF (os', os'') } as op)
        :: os ->
          let mk os' os'' = [{ op with desc= IF (os', os'') }] in
        (* Even if the postfix are shared, they might work on differently typed stacks.
                  let shared_postfix, os', os'' =
                    let rec f rev_os' rev_os'' = match rev_os', rev_os'' with
                      | o':: rev_os', o''::rev_os'' when o' = o'' (* XXX attributes and comments? *)  ->
                          let shared_rev_postfix, rev_os', rev_os'' = f rev_os' rev_os'' in
                          o'::shared_rev_postfix, rev_os', rev_os''
                      | _ -> [], rev_os', rev_os''
                    in
                    let shared_rev_postfix, rev_os', rev_os'' = f (List.rev os') (List.rev os'') in
                    List.rev shared_rev_postfix, List.rev rev_os', List.rev rev_os''
                  in
        *)
          let shared_prefix, os', os''  =
            let rec f os' os'' = match os', os'' with
              | o':: os', o''::os'' when o' = o'' (* XXX attributes and comments? *)  ->
                  let shared_prefix, os', os'' = f os' os'' in
                  o'::shared_prefix, os', os''
              | _ -> [], os', os''
            in
            f os' os''
          in
          (* DIP (1, ..) costs 7 bytes.
             Apply only when shared_prefix > 7 bytes *)
          let shared_prefix, os', os'' =
            if
              List.fold_left (+) 0
                (List.map (fun op -> Bytes.length @@ Micheline.Encoding.to_bytes (Michelson.Opcode.to_micheline op)) shared_prefix) > 7
            then shared_prefix, os', os''
            else [], shared_prefix @ os', shared_prefix @ os''
          in
          let shared_postfix = [] in
          if shared_prefix <> [] || shared_postfix <> [] then begin
            let shared_prefix = [ _DIP shared_prefix ] in
            reapply rev_acc (shared_prefix @ mk os' os'' @ shared_postfix @ os)
          end else
            (* IF { ... ; FAILWITH } { OPS }
               => IF { ... ; FAILWITH } {}; OPS  *)
            if end_with_failwith os' && os'' <> [] then begin
              reapply rev_acc (mk os' [] @ os'' @ os)
            end else if end_with_failwith os'' && os' <> [] then begin
              reapply rev_acc (mk [] os'' @ os' @ os)
            end else
              recurse rev_acc op os

      (*    They are not simply applicable...
            (* IF_* { ... ; FAILWITH } { OPS }
               => IF_* { ... ; FAILWITH } {}; OPS  *)
                (* IF_* { ... ; FAILWITH } { OPS }
                   => IF_* { ... ; FAILWITH } {}; OPS  *)
      *)
      (*
      | ({ desc = ( IF_NONE (os', os'')
                  | IF_LEFT (os', os'')
                  | IF_CONS (os', os'') as desc ) } as op)
        :: os ->
          (* IF_* { } { } => DROP 1 *)
          let mk os' os'' =
            if os' = [] && os'' = [] then begin
              modified := true;
              [_DROP 1]
            end else
              let desc = match desc with
                | IF_NONE _ -> IF_NONE (os', os'')
                | IF_LEFT _ -> IF_LEFT (os', os'')
                | IF_CONS _ -> IF_CONS (os', os'')
                | _ -> assert false
              in
              [{ op with desc }]
          in
          (* IF_* { ... ; FAILWITH } { OPS }
             => IF_* { ... ; FAILWITH } {}; OPS  *)
          if end_with_failwith os' && os'' <> [] then begin
            modified := true;
            f rev_acc (mk os' [] @ os'' @ os)
          end else if end_with_failwith os'' && os' <> [] then begin
            modified := true;
            f rev_acc (mk [] os'' @ os' @ os)
          end else
            recurse rev_acc op os
      *)

      (* PAIR, PAIRn, UNPAIR, UNPAIRn *)

      (* We cannot UNPAIR ; PAIR => NOP, since it may break type safety.
         Pair int %x int %y  => NOP          => Pair int %x int %y
         Pair int %x int %y  => UNPAIR; PAIR => Pair int int

         Pair int int can be unified with another pair type with fields
         like Pair int %p int %q.
      *)

      (* PAIR 2 => PAIR *)
      | ({desc = PAIRn 2} as o) :: os ->
          reapply rev_acc & { o with desc= PAIR [] (* XXX *)} :: os

      (* UNPAIR 2 => UNPAIR *)
      | ({desc = UNPAIRn 2} as o) :: os ->
          reapply rev_acc & { o with desc= UNPAIR } :: os

      (* PAIR; UNPAIR => NOP *)
      | {desc = PAIR _; comments = cs1} :: {desc = UNPAIR; comments = cs2} :: os
        ->
          reapply rev_acc & nop (cs1 @ cs2) :: os

      (* XXX PAIRn PAIRn contraction?
         - DIP 1 { PAIR }; PAIR => PAIR 3
         - DIP 2 { PAIR }; DIP 1 { PAIR }; PAIR => PAIR 4
         - DIP 1 { DIP 1 { PAIR }; PAIR }; PAIR => PAIR 4
      *)
      (* XXX UNPAIRn UNPAIRn contraction? *)
      (* XXX UPDATE 0 *)

      (* PUSH _ (Pair a b); UNPAIR => PUSH b; PUSH a *)
      | {
          desc = PUSH ({desc = TyPair (_, t1, _, t2)}, Pair (a, b));
          comments = cs1;
        }
        :: {desc = UNPAIR; comments = cs2} :: os ->
          reapply rev_acc
            ({desc = PUSH (t2, b); comments = []}
            :: {desc = PUSH (t1, a); comments = cs1 @ cs2}
            :: os)

      (* DIP *)

      (* DIP n { } => nop *)
      | {desc = (DIPn (_, []) | DIP []); comments = cs1} :: os ->
          reapply rev_acc & nop cs1 :: os
      (* DIP 0 { ops } => ops *)
      | {desc = DIPn (0, os1) ; comments = cs1} :: os ->
          reapply rev_acc & append_comments_at_last os1 cs1 @ os
      (* DIP 1 { ops } => DIP { ops } *)
      | ({desc = DIPn (1, os1)} as o) :: os ->
          reapply rev_acc & { o with desc= DIP os1 } :: os
      (* DIP n xs; DIP n ys => DIP n (xs @ ys) *)
      | {desc = DIPn (n1, os1) ; comments = cs1}
        :: {desc = DIPn (n2, os2); comments = cs2} :: os when n1 = n2 ->
          reapply rev_acc & set_comments (_DIPn (n1, os1 @ os2)) (cs1 @ cs2) :: os
      | {desc = DIP os1 ; comments = cs1}
        :: {desc = DIP os2; comments = cs2} :: os ->
          reapply rev_acc & set_comments (_DIP (os1 @ os2)) (cs1 @ cs2) :: os

      (* Copy stack item before we have DUP n
         DIP n { DUP 1 }; DIG (n+1)  =>  DUP (n+1)
         DIP { DUP 1 }; DIG 2        =>  DUP 2
      *)
      | {desc= DIPn (n, [ { desc= (DUP | DUPn 1); comments= c2  } ]); comments= c1} :: {desc= DIG m; comments= c3} :: os when n+1 = m ->
          reapply rev_acc & set_comments (_DUPn m) (c1 @ c2 @ c3) :: os
      | {desc= DIP [ { desc= (DUP | DUPn 1); comments= c2  } ]; comments= c1} :: {desc= DIG 2; comments= c3} :: os ->
          reapply rev_acc & set_comments (_DUPn 2) (c1 @ c2 @ c3) :: os

      (* DIG *)

      (* DIG 0 => NOP *)
      | {desc = DIG 0; comments = cs} :: os ->
          reapply rev_acc & nop cs :: os
      (* DIG 1 => SWAP.  SWAP has less gas and storage costs than DIG 1 !!! *)
      | {desc = DIG 1; comments = cs} :: os ->
          reapply rev_acc & (set_comments _SWAP cs :: os)

      (* Copy stack item before we have DUP n
         DIG n; DUP; DUG (n+1)  =>  DUP (n+1)
      *)
      | {desc= DIG n; comments= c1} :: {desc= (DUP | DUPn 1); comments= c2} :: {desc= DUG m; comments= c3} :: os when n+1 = m ->
          reapply rev_acc & set_comments (_DUPn m) (c1 @ c2 @ c3) :: os

      (* SWAP *)

      (* SWAP; SWAP => empty *)
      | {desc = SWAP; comments = cs1} :: {desc = SWAP; comments = cs2} :: os ->
          reapply rev_acc & nop (cs1 @ cs2) :: os

      (* DUG *)

      (* DUG 0 => NOP *)
      | {desc = DUG 0; comments = cs} :: os ->
          reapply rev_acc & nop cs :: os
      (* DUG 1 => SWAP.  SWAP has less gas and storage costs than DUG 1 !!! *)
      | {desc = DUG 1; comments = cs} :: os ->
          reapply rev_acc & (set_comments _SWAP cs :: os)

      (* DUP *)

      (* DUP 1 => DUP *)
      | ({desc = DUPn 1} as o) :: os ->
          reapply rev_acc & { o with desc = DUP } :: os

      (* DROP *)

      (* DROP 0 => NOP *)
      | {desc = DROPn 0; comments = cs1} :: os ->
          reapply rev_acc & nop cs1 :: os

      (* DROP 1 => DROP *)
      | ({desc = DROPn 1 } as o) :: os ->
          reapply rev_acc ({o with desc= DROP} :: os)

      (* DROP x; DROP y => DROP (x+y) *)
      | {desc = DROPn x1; comments = cs1}
        :: {desc = DROPn x2; comments = cs2} :: os ->
          reapply rev_acc & set_comments (_DROPn (x1 + x2)) (cs1 @ cs2) :: os
      (* annoying.. *)
      | {desc = DROP; comments = cs1}
        :: {desc = DROP; comments = cs2} :: os ->
          reapply rev_acc & set_comments (_DROPn 2) (cs1 @ cs2) :: os
      | {desc = DROPn x1; comments = cs1}
        :: {desc = DROP; comments = cs2} :: os ->
          reapply rev_acc & set_comments (_DROPn (x1+1)) (cs1 @ cs2) :: os
      | {desc = DROP; comments = cs1}
        :: {desc = DROPn x2; comments = cs2} :: os ->
          reapply rev_acc & set_comments (_DROPn (1+x2)) (cs1 @ cs2) :: os

      (* GETn *)

      (* GET 0 => NOP *)
      | {desc = GETn 0; comments = cs1} :: os ->
          reapply rev_acc & nop cs1 :: os

      (* GET 1 => CAR *)
      | ({desc = GETn 1} as o) :: os ->
          reapply rev_acc & { o with desc = CAR } :: os

      (* GET 2 => CDR *)
      | ({desc = GETn 2} as o) :: os ->
          reapply rev_acc & { o with desc = CDR } :: os

      (* GETn (n >= 3) is better than CDR & CAR *)

      (* GET2n ; GETm => GET(2n+m) *)
      | {desc = (CDR | GETn _ as d1) ; comments = cs1}
        :: {desc = (CAR | CDR | GETn _ as d2); comments = cs2} :: os
        when length_of_get d1 mod 2 = 0 ->
          (* XXX CDR ; CDR => GETn 4, which may have the same size but be inefficient *)
          let l1 = length_of_get d1 in
          let l2 = length_of_get d2 in
          reapply rev_acc & set_comments (_GETn (l1 + l2)) (cs1 @ cs2) :: os

      (* PUSH constants *)

      (* PUSH ty {} => EMPTY_SET/MAP/BIGMAP *)
      | {desc = PUSH ({desc = TySet ty}, Set []); comments = cs} :: os ->
          reapply rev_acc & (set_comments (_EMPTY_SET ty) cs :: os)
      | {desc = PUSH ({desc = TyMap (ty1, ty2)}, Map []); comments = cs} :: os ->
          reapply rev_acc & (set_comments (_EMPTY_MAP (ty1, ty2)) cs :: os)
      | {desc = PUSH ({desc = TyBigMap (ty1, ty2)}, Map []); comments = cs} :: os ->
          reapply rev_acc & (set_comments (_EMPTY_BIG_MAP (ty1, ty2)) cs :: os)

      (* boolean and integer arithmetics *)

      | {desc = NOT; comments = cs1}
        :: {desc = IF (then_, else_); comments = cs2} :: os ->
          reapply rev_acc
          & {
              desc = IF (else_, then_);
              comments = cs1 @ ("(* flipped by optimization *)" :: cs2);
            }
            :: os
      | {desc = SWAP; comments = cs1}
        :: {desc = (ADD | AND | OR | XOR) as op; comments = cs2} :: os ->
          reapply rev_acc & ({desc = op; comments = cs1 @ cs2} :: os)
      | {desc = SWAP; comments = cs1}
        :: {desc = COMPARE; comments = cs2}
           :: {desc = (EQ | LT | LE | GT | GE | NEQ) as op; comments = cs3}
              :: os ->
          let op =
            match op with
            | EQ -> EQ
            | LT -> GT
            | LE -> GE
            | GT -> LT
            | GE -> LE
            | NEQ -> NEQ
            | _ -> assert false
          in
          reapply rev_acc
          & {desc = COMPARE; comments = cs1 @ cs2}
            :: {desc = op; comments = "(* operator flipped *)" :: cs3}
            :: os

      | o::os -> recurse rev_acc o os

    and reapply rev_acc ops =
      modified := true;
      f rev_acc ops

    and g os = f [] os

    and recurse rev_acc o os =
      (* recursion *)
      (* XXX calls of `g` here are not at tail position.
         Stack will overflow when there is a too deep nest of { }
      *)
      let mk desc = {o with desc} in
      match o.desc with
      | DIPn (i, os') ->
          let os' = g os' in
          (* required when optimization creates DIP n { ...; FAILWITH } ... *)
          if end_with_failwith os' then
            f rev_acc (_DROPn i :: os')
          else
            f ((mk & DIPn (i, g os')) :: rev_acc) os
      | DIP os' ->
          let os' = g os' in
          (* required when optimization creates DIP n { ...; FAILWITH } ... *)
          if end_with_failwith os' then
            f rev_acc (_DROP :: os')
          else
            f ((mk & DIP (g os')) :: rev_acc) os
      | LAMBDA (t1, t2, os') ->
          f (mk (LAMBDA (t1, t2, g os')) :: rev_acc) os
      | IF (os', os'') -> f (mk (IF (g os', g os'')) :: rev_acc) os
      | IF_NONE (os', os'') -> f (mk (IF_NONE (g os', g os'')) :: rev_acc) os
      | IF_LEFT (os', os'') -> f (mk (IF_LEFT (g os', g os'')) :: rev_acc) os
      | IF_CONS (os', os'') -> f (mk (IF_CONS (g os', g os'')) :: rev_acc) os
      | ITER os' -> f (mk (ITER (g os')) :: rev_acc) os
      | MAP os' -> f (mk (MAP (g os')) :: rev_acc) os
      | LOOP os' -> f (mk (LOOP (g os')) :: rev_acc) os
      | LOOP_LEFT os' -> f (mk (LOOP_LEFT (g os')) :: rev_acc) os
      | CREATE_CONTRACT m -> f (mk (CREATE_CONTRACT { m with code= g m.code }) :: rev_acc) os
      | _ -> f (o :: rev_acc) os
    in

    let rev_history = ref [] in
    let rec loop os =
      modified := false ;

      (* Check if the loop is infinite *)
      let rec check_loop acc = function
        | [] -> None
        | os'::_oss when os = os' -> Some (os' :: acc @ [os])
        | os'::oss -> check_loop (os'::acc) oss
      in
      begin match check_loop [] !rev_history with
        | Some loop ->
            logf "WARNING!!!! INFINITE LOOP IN OPTIMIZATION!!!";
            List.iter (fun os ->
                logf "@[%a@]@." Michelson.Opcode.pp (_BLOCK os)
              ) loop;
            (* os *)
            assert false
        | None ->
            rev_history := os :: !rev_history;
            let os = f [] os in
            if !modified then loop os else os
      end
    in
    loop os

  let view (v : Michelson.View.t) =
    { v with ty_param = type_ v.ty_param;
             ty_return = type_ v.ty_return;
             code = optimize v.code }

  let rec type_ ty = ty
  and module_ m =
    let open Michelson.Module in
    { parameter = type_ m.parameter;
      storage = type_ m.storage;
      code = List.map type_and_constants & optimize m.code;
      views = List.map view m.views }

  and type_and_constants t =
    let rec fs xs = List.map f xs
    and f t =
      let desc =
        match t.desc with
        | BLOCK ts -> BLOCK (fs ts)
        | DIP ts -> DIP (fs ts)
        | DIPn (n, ts) -> DIPn (n, fs ts)

        | DUP
        | DIG _
        | DUG _
        | DROP
        | DROPn _
        | SWAP
        | PAIR _
        | CAR
        | CDR
        | APPLY
        | CONS
        | SOME
        | COMPARE
        | EQ
        | LT
        | LE
        | GT
        | GE
        | NEQ
        | ADD
        | SUB
        | SUB_MUTEZ
        | MUL
        | EDIV
        | ABS
        | ISNAT
        | NEG
        | LSL
        | LSR
        | AND
        | OR
        | XOR
        | NOT
        | EXEC
        | FAILWITH
        | UNIT
        | SIZE
        | MEM
        | UPDATE
        | CONCAT
        | GET
        | PACK
        | SLICE
        | TRANSFER_TOKENS
        | SET_DELEGATE
        | CREATE_ACCOUNT
        | IMPLICIT_ACCOUNT
        | NOW
        | AMOUNT
        | BALANCE
        | CHECK_SIGNATURE
        | BLAKE2B
        | SHA256
        | SHA512
        | HASH_KEY
        | STEPS_TO_QUOTA
        | SOURCE
        | SENDER
        | ADDRESS
        | CHAIN_ID
        | INT
        | LEVEL
        | SELF_ADDRESS
        | UNPAIR
        | PAIRING_CHECK
        | NEVER
        | KECCAK
        | SHA3
        | TICKET
        | READ_TICKET
        | SPLIT_TICKET
        | JOIN_TICKETS
        | SAPLING_EMPTY_STATE _
        | SAPLING_VERIFY_UPDATE
        | VOTING_POWER
        | TOTAL_VOTING_POWER
        | GET_AND_UPDATE
        | GETn _
        | UPDATEn _
        | PAIRn _
        | UNPAIRn _
        | DUPn _
        | SELF _
        | RENAME _
            -> t.desc

        | LEFT ty -> LEFT (type_ ty)
        | RIGHT ty -> RIGHT (type_ ty)
        | LAMBDA (ty1, ty2, ts) -> LAMBDA (type_ ty1, type_ ty2, fs ts)
        | PUSH (ty, c) -> PUSH (type_ ty, constant c)
        | NIL ty -> NIL (type_ ty)
        | NONE ty -> NONE (type_ ty)
        | IF (ts1, ts2) -> IF (fs ts1, fs ts2)
        | IF_NONE (ts1, ts2) -> IF_NONE (fs ts1, fs ts2)
        | IF_LEFT (ts1, ts2) -> IF_LEFT (fs ts1, fs ts2)
        | IF_CONS (ts1, ts2) -> IF_CONS (fs ts1, fs ts2)
        | EMPTY_SET ty -> EMPTY_SET (type_ ty)
        | EMPTY_MAP (ty1, ty2) -> EMPTY_MAP (type_ ty1, type_ ty2)
        | EMPTY_BIG_MAP (ty1, ty2) -> EMPTY_BIG_MAP (type_ ty1, type_ ty2)
        | ITER ts -> ITER (fs ts)
        | MAP ts -> MAP (fs ts)
        | LOOP ts -> LOOP (fs ts)
        | LOOP_LEFT ts -> LOOP_LEFT (fs ts)
        | UNPACK ty -> UNPACK (type_ ty)
        | CAST ty -> CAST (type_ ty)
        | CONTRACT ty -> CONTRACT (type_ ty)
        | CONTRACT' (ty, n) -> CONTRACT' (type_ ty, n)
        | CREATE_CONTRACT m -> CREATE_CONTRACT (module_ m)
        | VIEW (s, ty) -> VIEW (s, type_ ty)
      in
      { t with desc }
    in
    f t

  let test () =
    let open StackOpsOptimizer in
    test_astar_score ();
    test_optimal_ops_for_dip_drop ()
end
