open Js_of_ocaml

module Html = Dom_html

let from_Some = function
  | Some x -> x
  | _ -> assert false

let (!$) = Js.string

module Console = struct
  let log a = (Js.Unsafe.js_expr "console.log" : 'a -> unit) a
  let logf fmt = Format.kasprintf (fun s -> log !$s) fmt
end

let getById coerce s =
  match Html.getElementById_coerce s coerce with
  | Some x -> x
  | None ->
      Console.logf "Element %s is not found" s;
      exit 1

let alert s = Html.window##alert (Js.string s)

let is_worker () =
  try ignore (Js.Unsafe.eval_string "window"); false with _ -> true
