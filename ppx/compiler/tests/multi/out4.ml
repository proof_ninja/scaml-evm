open SCaml
module X = struct
  [@@@SCaml]
  let[@entry] main () _ = [], Lib.one + Lib.Y.two
end
