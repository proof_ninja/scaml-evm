(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2022 DaiLambda, Inc.                     *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open SCaml_tools

let parse loc = function
  | "" -> Error (loc, "empty annotation")
  | s ->
      match String.split_at 1 s with
      | "@", s -> Ok (`Var s)
      | ":", s -> Ok (`Type s)
      | "%", s -> Ok (`Field s)
      | _ -> Error (loc, "invalid annotation: " ^ s)

let get f xs = List.partition_map f xs

let get_fields xs = get (function `Field s -> `Left s | a -> `Right a) xs
let get_vars xs = get (function `Var s -> `Left s | a -> `Right a) xs
let get_types xs = get (function `Type s -> `Left s | a -> `Right a) xs
