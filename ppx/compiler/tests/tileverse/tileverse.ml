[@@@SCaml contract = "tileverse"]
open SCaml

module Cell = struct
  type t =
    | Floor
    | Stone
    | Item of nat
end

module Chunk = struct
  [@@@SCaml]
  type t = (nat * nat, Cell.t) Map.t

  let from_coord x y =
    let (cx,dx) = Option.get @@ ediv_rem_int x (Int 16) in
    let (cy,dy) = Option.get @@ ediv_rem_int y (Int 16) in
    (cx,cy), (dx,dy)

  let set dx_dy cell (chunk : t) = Map.add dx_dy cell chunk

  let find dx_dy (chunk : t) =
    match Map.find dx_dy chunk with
    | None -> Cell.Stone
    | Some c -> c

  let empty : t = Map.empty
end

module World = struct
  [@@@SCaml]
  type t = (int * int, Chunk.t) BigMap.t

  let find_chunk cx_cy (w : t) =
    match BigMap.find cx_cy w with
    | None -> Chunk.empty
    | Some c -> c

  let raw_get x y w =
    let cx_cy, dx_dy = Chunk.from_coord x y in
    let chunk = find_chunk cx_cy w in
    let cell = Chunk.find dx_dy chunk in
    cx_cy, dx_dy, chunk, cell

  let get x y (w : t) =
    let cx_cy, dx_dy = Chunk.from_coord x y in
    let chunk = find_chunk cx_cy w in
    Chunk.find dx_dy chunk

  let set_chunk cx_cy chunk (w : t) = BigMap.add cx_cy chunk w

  let set x y cell (w : t) =
    let cx_cy, dx_dy = Chunk.from_coord x y in
    let chunk = find_chunk cx_cy w in
    let chunk' = Chunk.set dx_dy cell chunk in
    set_chunk cx_cy chunk' w
end

(* It is not a serious random number generator at all *)
module Random = struct
  [@@@SCaml]
  type t = nat

  let modder = Nat 2147483647 (* 1 lsl 31 - 1 *)

  let roll t = (t *^ Nat 48_271) %^ modder

  (* karma itself does not affect the RNG *)
  let roll t karma upto =
    let t' = roll t in
    t', (t' +^ karma) %^ upto
end

type bag = (nat, Cell.t) Map.t

module Player = struct
  [@@@SCaml]

  type t =
    { x : int
    ; y : int
    ; bag : (nat, Cell.t) Map.t
    ; rng : nat
    ; karma : nat
    }

  let zero = { x = Int 0; y = Int 0; bag= Map.empty; rng= Nat 1; karma= Nat 0 }
end

module Move = struct
  [@@@SCaml]

  type direction = N | E | W | S

  type t =
    | Move of direction
      (* move to the direction not blocked by a Stone *)

    | Dig of direction * Cell.t
      (* dig the block at the direction and find a cell *)

    | Swap of nat * Cell.t * Cell.t
      (* swap a cell at the bag #i with the cell at the ground *)

  (* Every move has a karma, which affects roll *)
  let karma k =
    let karma_of_direction = function
      | N -> Nat 1
      | E -> Nat 2
      | W -> Nat 3
      | S -> Nat 4
    in
    match k with
    | Move d -> karma_of_direction d
    | Dig (d, _) -> karma_of_direction d +^ Nat 10
    | Swap (n, _, _) -> n +^ Nat 20

  let apply_direction x y = function
    | N -> x, y - Int 1
    | S -> x, y + Int 1
    | W -> x - Int 1, y
    | E -> x + Int 1, y
end

let verify_move (world, px, py, bag, rng, karma) move =
  let res =
    match move with
    | Move.Move d ->
        let px', py' = Move.apply_direction px py d in
        let _a, _b, _c, cell = World.raw_get px' py' world in
        begin match cell with
        | Stone -> None
        | _ -> Some (rng, world, px', py', bag)
        end
    | Dig (d, found) ->
        let px', py' = Move.apply_direction px py d in
        let cx_cy, dx_dy, chunk, cell = World.raw_get px' py' world in
        begin match cell with
          | Stone ->
              (* not dependent on (px,py) for now *)
              let rng', res = Random.roll rng karma (Nat 10000) in
              let cell' =
                if res >= Nat 1000 then Cell.Floor (* 90% *)
                else if res >= Nat 400 then Item (Nat 0) (* 6%  *)
                else if res >= Nat 200 then Item (Nat 1) (* 2% *)
                else if res >= Nat 100 then Item (Nat 2) (* 1% *)
                else if res >= Nat 50 then Item (Nat 3) (* 0.5% *)
                else if res >= Nat 25 then Item (Nat 4) (* 0.25% *)
                else if res >= Nat 12 then Item (Nat 5) (* 0.13% *)
                else Item (Nat 6) (* 0.12% *)
              in
              if found <> cell' then None
              else
                let chunk' = Chunk.set dx_dy cell' chunk in
                let world' = World.set_chunk cx_cy chunk' world in
                Some (rng', world', px, py, bag)
          | _ -> None
        end
    | Swap (inv, cell_in_inv, cell_on_ground) ->
        let cell_in_inv' =
          match Map.find inv bag with
          | None -> Cell.Floor
          | Some c -> c
        in
        if cell_in_inv <> cell_in_inv' then None
        else
          let cx_cy, dx_dy, chunk, cell_on_ground' = World.raw_get px py world in
          if cell_on_ground <> cell_on_ground' then None
          else
            let bag' = Map.add inv cell_on_ground bag in
            let chunk' = Chunk.set dx_dy cell_in_inv chunk in
            let world' = World.set_chunk cx_cy chunk' world in
            Some (rng, world', px, py, bag')
  in
  match res with
  | None -> None
  | Some (rng', world', px', py', bag') ->
      let karma' = karma +^ Move.karma move in
      Some (world', px', py', bag', rng', karma')

let rec verify_moves st = function
  | [] -> Some st
  | m::ms ->
      match verify_move st m with
      | None -> None
      | Some st -> verify_moves st ms

type storage =
  { world : (int * int, Chunk.t) BigMap.t
  ; players : (address, Player.t) BigMap.t
  }

let[@entry] submit moves (storage : storage) =
  let address = Global.get_source () in
  let x, y, bag, rng, karma =
    match BigMap.find address storage.players with
    | None ->
        let { Player.x; y; bag; rng; karma } = Player.zero in
        x, y, bag, rng, karma
    | Some p -> p.x, p.y, p.bag, p.rng, p.karma
  in
  [],
  match verify_moves (storage.world, x, y, bag, rng, karma) moves with
  | None -> assert false (* No explicit failure is recorded on chain *)
  | Some (world, x, y, bag, rng, karma) ->
      { world; players= BigMap.add address { Player.x; y; bag; rng; karma } storage.players }

let[@entry] init () (storage : storage) =
  [],
  let world =
    World.set (Int 0) (Int 0) Cell.Floor
    @@ World.set (Int 0) (Int 1) Cell.Stone
    @@ World.set (Int 0) (Int 2) (Cell.Item (Nat 0))
    @@ World.set (Int 0) (Int 3) (Cell.Item (Nat 1)) storage.world
  in
  { world
  ; players = storage.players }

(* We can build a new version of the game keeping the map and the players,
   by providing views of them to the new one.  To do this properly,
   we must have a way to freeze the old version of the game to prevent
   its data from changing.
*)
let[@view] chunk cx_cy { world ; _ } = World.find_chunk cx_cy world
let[@view] player address { players ; _ } = BigMap.find address players
