[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main x y =
  ( [],
    (assert (BigMap.get_with_default (Int 2) BigMap.empty (Nat 3) = Nat 3) ;
     assert (
       BigMap.get_with_default
         (Int 2)
         (BigMap.update (Int 2) (Some (Nat 4)) BigMap.empty)
         (Nat 3)
       = Nat 4)) )
