open SCaml

let[@entry] main () _ =
  let t = Ticket.create () (Nat 10) in
  match Ticket.split t (Nat 1, Nat 9) with
  | None -> assert false
  | Some (t1, t9) -> (
      match Ticket.split t9 (Nat 100, Nat 100) with
      | Some _ -> assert false
      | None -> [], ())
