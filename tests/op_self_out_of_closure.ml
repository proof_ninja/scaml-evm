(* REJECT *)
(*
   STORAGE=(None : (unit -> unit contract) option)
*)
open SCaml
let[@entry] main () _ =
  let c : unit contract = Contract.self in
  [], Some (fun () -> c)
