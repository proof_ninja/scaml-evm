(*
INPUT=Int 10
*)

open SCaml

let[@entry] main ten () =
  let rec g acc x = if x = Int 0 then acc else g (acc ^ "s") (x - Int 1) in
  let v = g "h" ten in
  assert (v = "hssssssssss") ;

  [], ()
