open SCaml

type t = {x : int; y : int}

let[@entry] main b t =
  ( [],
    if b then {t with y = Option.get @@ view Contract.self_address "x" ()}
    else {t with x = Option.get @@ view Contract.self_address "y" ()} )

let[@view] view_x () {x} = x

let[@view] view_y () {y} = y
