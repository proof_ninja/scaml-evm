#!/bin/bash
set -e

rm -rf _build
mkdir _build
cp ../../scaml.opam _build/

# --squash --no-cache 
docker build $* -t dailambda/scaml-build-env:1.3.1 . 
docker run -it --rm dailambda/scaml-build-env:1.3.1 bash
 
