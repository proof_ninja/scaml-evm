(* INPUT=false
*)
open SCaml

let[@entry] main p () =
  (* the else part should be moved to after the if *)
  if p then failwith "fail" else [], ()
