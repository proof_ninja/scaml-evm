(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019-2021  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Tools

let debug = Tools.Debug.register ["scaml"; "pmatch"]

module M = Michelson
open M.Type
module C = M.Constant

open IML

open Transpattern

let noloc = Location.none

let create_ident s = Ident.create_local s

module Type = Michelson.Type

(* rows <-> columns *)
let transpose : 'p list list -> 'p list list =
 fun rows ->
  if rows = [] then []
  else
    let ncolumns = List.length & List.hd rows in
    List.init ncolumns (fun i ->
        List.map
          (fun row ->
            assert (List.length row = ncolumns) ;
            List.nth row i)
          rows)

type id_ty_loc = Ident.t * Type.t * Location.t

type case = {
  pats : Pattern.t list;
  guard : int option;
  action : int;
  bindings : (Ident.t * Location.t * id_ty_loc) list;
}

type matrix = case list

type tree =
  | Fail
  | Leaf of (Ident.t * Location.t * id_ty_loc) list * int
  | Switch of id_ty_loc * (Cnstr.t * id_ty_loc list * tree) list * tree option (* default *)
  | Guard of
      (Ident.t * Location.t * id_ty_loc) list
      (* binder *)
      * int (* guard *)
      * int (* case *)
      * tree
(* otherwise *)

let rec pp_tree ppf =
  let f fmt = Format.fprintf ppf fmt in
  function
  | Fail -> f "Fail"
  | Leaf (binders, n) ->
      f
        "Leaf %a %d"
        (Format.list ",@," (fun ppf (v, _, (v', _, _)) ->
             Format.fprintf
               ppf
               "%s=%s"
               (Ident.unique_name v)
               (Ident.unique_name v')))
        binders
        n
  | Switch (v, xs, None) ->
      f
        "@[<2>Switch %s@ [ @[%a@] ]@]"
        (Ident.unique_name & (fun (x, _, _) -> x) v)
        (Format.list ";@ " (fun ppf ->
             let f fmt = Format.fprintf ppf fmt in
             let pvs _ppf vs =
               f "%s" & String.concat ","
               & List.map (fun (x, _, _) -> Ident.unique_name x) vs
             in
             fun (c, vs, t) ->
               f "%s %a (%a)" (Cnstr.to_string c) pvs vs pp_tree t))
        xs
  | Switch (v, xs, Some d) ->
      f
        "@[<2>Switch %s@ [ @[%a@] default %a]@]"
        (Ident.unique_name & (fun (id, _, _) -> id) v)
        (Format.list ";@ " (fun ppf ->
             let f fmt = Format.fprintf ppf fmt in
             let pvs _ppf vs =
               f "%s" & String.concat ","
               & List.map (fun (x, _, _) -> Ident.unique_name x) vs
             in
             fun (c, vs, t) ->
               f "%s %a (%a)" (Cnstr.to_string c) pvs vs pp_tree t))
        xs
        pp_tree
        d
  | Guard (binders, guard, case, otherwise) ->
      f
        "@[<2>Guard (%a) guard%d case%d [%a]@]"
        (Format.list ",@," (fun ppf (v, _, (v', _, _)) ->
             Format.fprintf
               ppf
               "%s=%s"
               (Ident.unique_name v)
               (Ident.unique_name v')))
        binders
        guard
        case
        pp_tree
        otherwise

(* specialize on Left and Right *)
let rec specialize o c (matrix : matrix) : matrix =
  List.fold_right
    (fun ({pats; bindings} as case) st ->
      match pats with
      | [] -> assert false
      | pat :: pats ->
          let rec f pat =
            let loc = pat.loc in
            let gloc = Location.ghost loc in
            match c, pat.desc with
            | _, Pattern.Alias (pat, i, loc) ->
                let cases = f pat in
                List.map
                  (fun case ->
                    {case with bindings = (i, loc, o) :: case.bindings})
                  cases
            | c, Pattern.Or (p1, p2) ->
                specialize o c [{case with pats = p1 :: pats}]
                @ specialize o c [{case with pats = p2 :: pats}]
            | c, Pattern.Constr (c', ps) ->
                if c = c' then [{case with pats = ps @ pats}] else []
            (* For wild and var, we need to build another wild with arg type.
               XXX Currently we must code for each.  Ugh.
            *)
            | _, (Pattern.Wild | Pattern.Var _) -> (
                let bindings =
                  match pat.desc with
                  | Pattern.Wild -> bindings
                  | Pattern.Var v -> (v, loc, o) :: bindings
                  | _ -> assert false
                in
                match c with
                | Cnstr.Pair ->
                    let ty1, ty2 =
                      match pat.typ.desc with
                      | TyPair (_, ty1, _, ty2) -> ty1, ty2
                      | _ -> assert false
                    in
                    [
                      {
                        case with
                        pats =
                          mk ~loc:gloc ty1 Pattern.Wild
                          :: mk ~loc:gloc ty2 Pattern.Wild
                          :: pats;
                        bindings;
                      };
                    ]
                | Left ->
                    let typl =
                      match pat.typ.desc with
                      | TyOr (_, typl, _, _typr) -> typl
                      | _ -> assert false
                    in
                    [
                      {
                        case with
                        pats = mk ~loc:gloc typl Pattern.Wild :: pats;
                        bindings;
                      };
                    ]
                | Right ->
                    let typr =
                      match pat.typ.desc with
                      | TyOr (_, _typl, _, typr) -> typr
                      | _ -> assert false
                    in
                    [
                      {
                        case with
                        pats = mk ~loc:gloc typr Pattern.Wild :: pats;
                        bindings;
                      };
                    ]
                | Some ->
                    let typ =
                      match pat.typ.desc with
                      | TyOption (_, typ) -> typ
                      | _ -> assert false
                    in
                    [
                      {
                        case with
                        pats = mk ~loc:gloc typ Pattern.Wild :: pats;
                        bindings;
                      };
                    ]
                | Cons ->
                    let typ =
                      match pat.typ.desc with
                      | TyList typ -> typ
                      | _ -> assert false
                    in
                    [
                      {
                        case with
                        pats =
                          mk ~loc:gloc typ Pattern.Wild
                          :: mk ~loc:gloc pat.typ Pattern.Wild
                          :: pats;
                        bindings;
                      };
                    ]
                | None | Nil | Unit | Bool _ | Constant _ ->
                    [{case with pats; bindings}])
          in
          let cases = f pat in
          cases @ st)
    matrix
    []

let pp_matrix ppf matrix =
  let open Format in
  fprintf ppf "matrix:@." ;
  List.iter
    (function
      | {pats; guard = None; action = i} ->
          fprintf ppf "| %a -> %d@." (list ", " Pattern.pp) pats i
      | {pats; guard = Some g; action = i} ->
          fprintf ppf "| %a when %d -> %d@." (list ", " Pattern.pp) pats g i)
    matrix

let pp_osmatrix ppf (os, matrix) =
  let open Format in
  fprintf
    ppf
    "match %a with@ "
    (list ", " (fun ppf (id, _, _) -> fprintf ppf "%s" & Ident.unique_name id))
    os ;
  List.iter
    (function
      | {pats; guard = None; action = i} ->
          fprintf ppf "| %a -> %d@ " (list ", " Pattern.pp) pats i
      | {pats; guard = Some g; action = i} ->
          fprintf ppf "| %a when %d -> %d@ " (list ", " Pattern.pp) pats g i)
    matrix

let specialize o c matrix =
  if !debug then Format.eprintf "specializing... %a@." pp_matrix matrix ;
  let matrix = specialize o c matrix in
  if !debug then Format.eprintf "specialized... %a@." pp_matrix matrix ;
  matrix

let rec default o (matrix : matrix) : matrix =
  List.fold_right
    (fun ({pats} as case) st ->
      match pats with
      | [] -> assert false
      | pat :: pats ->
          let rec f pat =
            match pat.desc with
            | Pattern.Constr (_, _) -> st
            | Pattern.Wild -> {case with pats} :: st
            | Pattern.Var v ->
                {case with pats; bindings = (v, pat.loc, o) :: case.bindings}
                :: st
            | Pattern.Alias (pat, _id, _loc) -> f pat
            | Pattern.Or (p1, p2) ->
                default o [{case with pats = p1 :: pats}]
                @ default o [{case with pats = p2 :: pats}]
                @ st
          in
          f pat)
    matrix
    []

let swap i os (matrix : matrix) : _ * matrix =
  let rec f rev_st i xs =
    match i, xs with
    | 0, x :: xs -> (x :: List.rev rev_st) @ xs
    | _, [] -> assert false
    | i, x :: xs -> f (x :: rev_st) (i - 1) xs
  in
  ( f [] i os,
    List.map (fun ({pats} as case) -> {case with pats = f [] i pats}) matrix )

let rec cc os matrix =
  if !debug then
    Format.eprintf "@[<v2>Pmatch.cc:@ @[%a@]@]@." pp_osmatrix (os, matrix) ;
  match matrix with
  | [] -> Fail
  | {pats = ps; guard = g; action = a; bindings} :: _ ->
      if
        List.for_all
          (fun p ->
            let rec f p =
              match p.desc with
              | Pattern.Alias (p, _, _) -> f p
              | Pattern.Wild | Pattern.Var _ -> true
              | Pattern.Constr _ -> false
              | Pattern.Or _ -> false
            in
            f p)
          ps
      then (
        (* The first case consists only of var, _, or (x as v) *)
        let bindings =
          List.fold_right2
            (fun v p st ->
              let rec f p =
                match p.desc with
                | Pattern.Wild -> st
                | Pattern.Var v' -> (v', p.loc, v) :: st
                | Pattern.Alias (p, v', loc) -> (v', loc, v) :: f p
                | Pattern.Constr _ | Pattern.Or _ ->
                    Format.eprintf
                      "@[<v2>Pmatch.cc:@ @[%a@]@]@."
                      pp_osmatrix
                      (os, matrix) ;
                    assert false
              in
              f p)
            os
            ps
            bindings
        in
        match g with
        | None -> Leaf (bindings, a)
        | Some g ->
            if !debug then prerr_endline "guard" ;
            Guard (bindings, g, a, cc os & List.tl matrix))
      else
        (* find column i where at least one pattern which is not a wildcard *)
        let columns = transpose & List.map (fun case -> case.pats) matrix in
        let icolumns = List.mapi (fun i c -> i, c) columns in
        let i, column =
          match
            List.find_all
              (fun (_i, c) ->
                List.exists
                  (fun p ->
                    let rec f p =
                      match p.desc with
                      | Pattern.Alias (p, _, _) -> f p
                      | Pattern.Wild | Pattern.Var _ -> false
                      | Pattern.Constr _ -> true
                      | Pattern.Or (p1, p2) -> f p1 || f p2
                    in
                    f p)
                  c)
              icolumns
          with
          | [] -> assert false
          | (i, c) :: _ -> i, c
          (* blindly select the first *)
        in
        (* algo 3 (a) *)
        let algo os column =
          let constructors =
            List.sort_uniq compare
            & List.fold_left
                (fun st p ->
                  let rec f p =
                    match p.desc with
                    | Pattern.Alias (p, _, _) -> f p
                    | Pattern.Constr (c, _) -> [c]
                    | Pattern.Wild | Pattern.Var _ -> []
                    | Pattern.Or (p1, p2) -> f p1 @ f p2
                  in
                  f p @ st)
                []
                column
          in
          (* Mending constructors for sum, option, list and bools.
             Since non-exhaustive matches are already rejected, they are always listed fully.
          *)
          let constructors =
            (* Things must be sorted for the correctness of [is_signature] *)
            match constructors with
            | [] -> assert false
            | [Cnstr.Left] | [Right] -> [Cnstr.Left; Right]
            | [Some] | [None] -> [Some; None]
            | [Cons] | [Nil] -> [Cons; Nil]
            | [Bool _] -> [Bool false; Bool true]
            | _ -> constructors
          in
          (* if it is a signature, no default case is required *)
          let is_signature =
            (* Things must be sorted! *)
            match constructors with
            | [Left; Right]
            | [Some; None]
            | [Cons; Nil]
            | [Pair]
            | [Unit]
            | [Bool false; Bool true] ->
                true
            | _ ->
                (* Others including integers.
                   XXX If it is an integer for nullary constructors,
                   no default case is required *)
                false
          in

          (* XXX weak. this depends on the above code *)
          let ivty = List.hd os in
          Switch
            ( ivty,
              (let _, vty, ivty_loc
                   (* XXX not sure whether it should be used or not *) =
                 ivty
               in
               List.map
                 (fun c ->
                   let os = List.tl os in
                   let vs =
                     match c with
                     | Cnstr.Left ->
                         let ty =
                           match vty.desc with
                           | TyOr (_, ty, _, _) -> ty
                           | _ -> assert false
                         in
                         [
                           ( Ident.create_local & Varname.create "l" ty,
                             ty,
                             ivty_loc );
                         ]
                     | Right ->
                         let ty =
                           match vty.desc with
                           | TyOr (_, _, _, ty) -> ty
                           | _ -> assert false
                         in
                         [
                           ( Ident.create_local & Varname.create "r" ty,
                             ty,
                             ivty_loc );
                         ]
                     | Pair ->
                         let ty1, ty2 =
                           match vty.desc with
                           | TyPair (_, ty1, _, ty2) -> ty1, ty2
                           | _ -> assert false
                         in
                         [
                           ( Ident.create_local & Varname.create "l" ty1,
                             ty1,
                             ivty_loc );
                           ( Ident.create_local & Varname.create "r" ty2,
                             ty2,
                             ivty_loc );
                         ]
                     | Cons ->
                         let ty =
                           match vty.desc with
                           | TyList ty -> ty
                           | _ -> assert false
                         in
                         [
                           ( Ident.create_local & Varname.create "hd" ty,
                             ty,
                             ivty_loc );
                           ( Ident.create_local & Varname.create "tl" vty,
                             vty,
                             ivty_loc );
                         ]
                     | Some ->
                         let ty =
                           match vty.desc with
                           | TyOption (_, ty) -> ty
                           | _ -> assert false
                         in
                         [
                           ( Ident.create_local & Varname.create "x" ty,
                             ty,
                             ivty_loc );
                         ]
                     | Nil | None | Bool _ | Constant _ (* int/nat/tz *) | Unit
                       ->
                         []
                   in

                   ( c,
                     vs,
                     (if !debug then
                        Format.eprintf
                          "specialize on %s@." (Cnstr.to_string c) ;
                      cc (vs @ os) (specialize ivty c matrix)) ))
                 constructors),
              if is_signature then None
              else
                Some
                  (if !debug then Format.eprintf "default@." ;
                   cc (List.tl os) (default ivty matrix)) )
        in
        if i = 0 then algo os column
        else
          (* put the non wild card column at the leftest *)
          let o', matrix' = swap i os matrix in
          cc o' matrix'
(* xxx inefficient *)

let build aty acts guards t =
  let warn_unused_default = function
    | None -> ()
    | Some _ ->
        Format.eprintf
          "Warning: meaningless default case in a pattern match found.  This \
           is likely a compiler bug.@."
  in
  let rec f = function
    | Fail ->
        (* Nullary constructor is converted to integer.
           We need the default case for them. *)
        mkassertfalse ~loc:noloc aty
    | Leaf (binders, i) ->
        List.fold_right
          (fun (v, loc, (v', ty, _loc')) st ->
            mklet ~loc (mk ~loc ty v) (mkvar ~loc (v', ty)) st)
          binders
          (List.nth acts i)
    | Guard (binders, guard, case, otherwise) ->
        let guarde = List.nth guards guard in
        List.fold_right
          (fun (v, loc, (v', ty, _loc')) st ->
            mklet ~loc (mk ~loc ty v) (mkvar ~loc (v', ty)) st)
          binders
        & mke ~loc:guarde.loc aty
        & IfThenElse (guarde, List.nth acts case, Some (f otherwise))
    | Switch (_, [], _) -> assert false
    | Switch (v, [(Pair, [(v1, ty1, _loc1); (v2, ty2, _loc2)], t)], d) ->
        warn_unused_default d ;
        let t = f t in
        (* let (v1,v2) = v in <t> *)
        mkunpair
          ~loc:noloc
          (mk ~loc:noloc ty1 v1)
          (mk ~loc:noloc ty2 v2)
          (let id, ty, loc = v in
           mkvar ~loc (id, ty))
          t
    | Switch (_, [(Unit, [], t)], d) ->
        warn_unused_default d ;
        f t
    | Switch
        ( v,
          ( [(Left, [(vl, tyl, _locl)], tl); (Right, [(vr, tyr, _locr)], tr)]
          | [(Right, [(vr, tyr, _locr)], tr); (Left, [(vl, tyl, _locl)], tl)] ),
          d ) ->
        warn_unused_default d ;
        let tl = f tl in
        let tr = f tr in
        mke ~loc:noloc aty
        & Switch_or
            ( (let id, ty, loc = v in
               mkvar ~loc (id, ty)),
              mk ~loc:noloc tyl vl,
              tl,
              mk ~loc:noloc tyr vr,
              tr )
    | Switch
        ( v,
          ( [(Bool true, [], tt); (Bool false, [], tf)]
          | [(Bool false, [], tf); (Bool true, [], tt)] ),
          d ) ->
        warn_unused_default d ;
        let tt = f tt in
        let tf = f tf in
        mke ~loc:noloc aty
        & IfThenElse
            ( (let id, ty, loc = v in
               mkvar ~loc (id, ty)),
              tt,
              Some tf )
    | Switch
        ( v,
          ( [(Some, [(vs, tys, _locs)], ts); (None, [], tn)]
          | [(None, [], tn); (Some, [(vs, tys, _locs)], ts)] ),
          d ) ->
        warn_unused_default d ;
        let ts = f ts in
        let tn = f tn in
        mke ~loc:noloc aty
        & Switch_none
            ( (let id, ty, loc = v in
               mkvar ~loc (id, ty)),
              tn,
              mk ~loc:noloc tys vs,
              ts )
    | Switch
        ( v,
          ( [(Cons, [(v1, ty1, _loc1); (v2, ty2, _loc2)], tc); (Nil, [], tn)]
          | [(Nil, [], tn); (Cons, [(v1, ty1, _loc1); (v2, ty2, _loc2)], tc)] ),
          d ) ->
        warn_unused_default d ;
        let tc = f tc in
        let tn = f tn in
        let id, ty, loc = v in
        mke ~loc:noloc aty
        & Switch_cons
            ( mkvar ~loc (id, ty),
              mk ~loc:noloc ty1 v1,
              mk ~loc:noloc ty2 v2,
              tc,
              tn )
    | Switch (_v, _cases, None) -> assert false
    | Switch (v, cases, Some d) ->
        (* all cases must be about constants with infinite members *)
        List.iter
          (function
            | ( Cnstr.Constant
                  (C.Int _ | String _ | Bytes _ | Set _ | Map _ | Timestamp _),
                _,
                _ ) ->
                ()
            | ( ( Cnstr.Unit | Left | Right | Some | None | Cons | Nil | Pair
                | Bool _ ),
                _,
                _ ) ->
                assert false (* must be handled in the former cases *)
            | ( Cnstr.Constant
                  ( C.Unit | Bool _ | Option _ | List _ | Pair _ | Left _
                  | Right _ ),
                _,
                _ ) ->
                assert false (* invalid *)
            | ( Cnstr.Constant (C.PairN _), _, _) -> assert false (* We should not have it *)
            | Cnstr.Constant (C.Code _), _, _ -> assert false)
          cases ;
        List.fold_right
          (fun case telse ->
            match case with
            | Cnstr.Constant c, [], t ->
                (* XXX comparable check *)
                let t = f t in
                mke ~loc:noloc aty
                &
                let id, ty, loc = v in
                IfThenElse
                  ( mkeq
                      ~loc:noloc
                      (mkvar ~loc (id, ty))
                      (mke ~loc:noloc ty & Const c),
                    t,
                    Some telse )
            | _ -> assert false)
          cases
        & f d
  in
  f t

let compile ~loc e (cases : (Pattern.t * t option * t) list) =
  let gloc = Location.ghost loc in

  (* actions as functions *)
  let acts =
    List.mapi
      (fun i (pat, _g, action) ->
        let gloc = Location.ghost action.loc in
        let vars = Idents.elements & Pattern.vars pat in

        let unpackables =
          let fvs =
            List.fold_left
              (fun fvs idty -> Idents.remove idty fvs)
              (freevars action)
              (if vars = [] then [] else List.tl (List.rev vars))
            (* the last one cannot be free inside the body *)
          in
          Idents.filter (fun {typ} -> not & Michelson.Type.is_packable typ) fvs
        in
        let _must_expand = not & Idents.is_empty unpackables in
        (* It's inefficient for the storage, but we do not want to get troubled
           by unpackables around the LAMBDAs introduced by pmatch.
        *)
        let must_expand = true in
        let case =
          if must_expand then
            create_ident (Printf.sprintf "case_must_expand%d" i)
          else create_ident (Printf.sprintf "case%d" i)
        in

        match vars with
        | [] ->
            (* if [vars = []], we need a [fun () ->].
               Think about the case of [| _ -> assert false].
            *)
            let pvar = mk ~loc:gloc Type.tyUnit & create_ident "unit" in
            let f = mkfun_tmp ~loc:gloc pvar action in
            let e =
              mke
                ~loc:gloc
                action.typ
                (App (mkvar ~loc:gloc (case, f.typ), [mkunit ~loc:gloc ()]))
            in
            if must_expand then case, None, IML.subst [case, f] e
            else case, Some f, e
        | _ ->
            (* match ... with
               | ..x.. when g[x] -> e[x]

               let case xnew = e[xnew] in
               match ... with
               | ..x.. when g[x] -> case x

               We have to rename the pattern variables x in e[x]
               to void name crashes which confuse [count_variables].

               XXX This is very inefficient!
            *)
            let vars' =
              List.map
                (fun {desc = v; typ = ty} ->
                  Ident.create_local & Varname.create (Ident.name v) ty, ty)
                vars
            in
            (* We use alpha_conv, not subst, to keep the original code
               locations *)
            let s = List.map2 (fun {desc = v} (v', _) -> v, v') vars vars' in
            let action = alpha_conv s action in

            (* f = fun v'1 v'2 .. v'n -> action

               XXX Curried.  This makes expansion happens more often
               to avoid unstorable free vars
            *)
            let f =
              List.fold_right
                (fun (v', ty) st -> mkfun_tmp ~loc:gloc (mk ~loc:gloc ty v') st)
                vars'
                action
            in
            (* e = f v1 v2 .. vn *)
            let e =
              mke
                ~loc:gloc
                action.typ
                (App
                   ( mkvar ~loc:gloc (case, f.typ),
                     List.map
                       (fun {desc; typ} ->
                         mkvar
                           ~loc:gloc
                           (* we can use the loc of the vars *) (desc, typ))
                       vars ))
            in

            if must_expand then case, None, IML.subst [case, f] e
            else case, Some f, e)
      cases
  in

  let cases, guards =
    let cases, guards, _ =
      List.fold_left
        (fun (cases, guards, i) case ->
          match case with
          | p, None, e -> (p, None, e) :: cases, guards, i
          | p, Some g, e -> (p, Some i, e) :: cases, g :: guards, i + 1)
        ([], [], 0)
        cases
    in
    List.rev cases, List.rev guards
  in

  let typ = (match List.hd cases with _, _, e -> e).typ in
  let v = Ident.create_local & Varname.create "v" e.typ in

  (* let casei = fun ... in let v = e in ... *)
  let make x =
    (* let v = <e> in <x> *)
    let match_ = mklet ~loc:gloc (mk ~loc:gloc e.typ v) e x in
    (* let casei = <f> in .. *)
    List.fold_right
      (fun (v, fopt, _e) st ->
        match fopt with
        | None -> st
        | Some f -> mklet ~loc:f.loc (mk ~loc:f.loc f.typ v) f st)
      acts
      match_
  in

  let matrix : matrix =
    List.mapi
      (fun i (pat, g, _) ->
        {pats = [pat]; guard = g; action = i; bindings = []})
      cases
  in

  (* XXX if match target is a tuple literal, no need to form a real tuple *)
  let res = cc [v, e.typ, e.loc] matrix in
  if !debug then Format.eprintf "pmatch debug: %a@." pp_tree res ;
  let e = build typ (List.map (fun (_, _, e) -> e) acts) guards res in
  if !debug then Format.eprintf "pmatch debug: %a@." pp e ;
  make e
