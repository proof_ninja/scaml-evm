[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main x y =
  ( [],
    (assert (Map.get_with_default (Int 2) Map.empty (Nat 3) = Nat 3) ;
     assert (
       Map.get_with_default
         (Int 2)
         (Map.update (Int 2) (Some (Nat 4)) Map.empty)
         (Nat 3)
       = Nat 4)) )
