(*
STORAGE= Int 0
*)
open SCaml

let[@entry] f p s =
  ( [],
    let f (st, t) = st + t in
    List.fold_left' f (Int 0) [Int 1; Int 2; Int 3] )
