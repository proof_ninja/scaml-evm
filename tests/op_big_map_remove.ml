[@@@SCaml iml_optimization = false]
open SCaml
open BigMap
let[@entry] main x y =
  [], assert (not (mem "a" (remove "a" (add "a" (Int 1) BigMap.empty))))
