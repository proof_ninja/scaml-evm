(*
   STORAGE=Nat 10
*)

open SCaml

let emod_nat_nat n d =
  match ediv_nat_nat n d with
  | None -> assert false
  | Some (_, m) -> m

let modder = Nat 2147483647 (* 1 lsl 31 - 1 *)

module Random = struct
  let roll t = t +^ Nat 1

  (* karma itself does not affect the RNG *)
  let roll t karma = roll t +^ karma
end

let[@entry] entry () seed =
  [],
  Random.roll seed (Nat 10)
