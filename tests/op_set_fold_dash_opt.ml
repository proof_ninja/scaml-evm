open SCaml
let[@entry] main x y =
  ( [],
    let f (n, sum) = n + sum in
    assert (Int 6 = Set.fold' f (Set [Int 1; Int 2; Int 3]) (Int 0)) )
