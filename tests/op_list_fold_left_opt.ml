open SCaml
let[@entry] main () () =
  ( [],
    let f acc x = acc + x in
    assert (List.fold_left f (Int 0) [Int 1; Int 2; Int 3] = Int 6) )
