(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2022  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** Number of bytes required to store the data in Tezos storage.

    Tezos storage fee is proportional to this value.
*)

val type_ : Michelson.Type.t -> int
val constant : Michelson.Constant.t -> int
val opcode : Michelson.Opcode.t -> int
val module_ : Michelson.Module.t -> int
