(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* This module defines encoding of ML records and variants in Michelson,
   which only has binary product and sum: [pair] and [or].
*)

type side = Left | Right

type 'a tree = Leaf of 'a | Branch of 'a tree * 'a tree

val depth : 'a tree -> int

(** For the definition of right hand balanced,
    see TZIP-6: https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-6/tzip-6.md
    Also returns the depth.
*)
val right_hand_balanced : 'a tree -> bool * int

(** Layout the list of elements in a binary tree *)
val place : 'a list -> 'a tree

(** [path i n] finds out how to access the [i]-th element of [n]
    in the layout by [place].
*)
val path : int -> int -> side list

(** Folding over [tree] *)
val fold : leaf:('a -> 'b) -> branch:('b -> 'b -> 'b) -> 'a tree -> 'b
