open SCaml

let[@entry] main () () =
  assert (nat_of_mutez (Tz 0.000001) = Nat 1) ;
  [], ()
