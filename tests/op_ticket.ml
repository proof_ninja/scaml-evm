(*
STORAGE= (None : unit ticket option)
*)

open SCaml

(* XXX need to check

   - ticket value in the initial storage:
     (Pair "KT1BE..." Unit 10)
*)
let[@entry] main () (s : unit ticket option) =
  [], Some (Ticket.create () (Nat 10))
