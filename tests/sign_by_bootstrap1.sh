#!/bin/sh

set -e

echo input: $1
sig=`tezos-client --protocol PtGRANADsDU8 --mode mockup --base-dir _build/tezos-client sign bytes $1 for bootstrap1 | sed -e 's/Signature: //'`
tezos-client --protocol PtGRANADsDU8 --mode mockup --base-dir _build/tezos-client check that bytes $1 were signed by bootstrap1 to produce $sig
echo signature: $sig



