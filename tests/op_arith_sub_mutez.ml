[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main x y =
  ( [],
    ( assert (Tz 32.1 -$? Tz 1.23 = Some (Tz 30.87));
      assert (Tz 32.1 -$? Tz 40.2 = None) ) )
