(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open SCaml_tools

open Tezos_micheline
open Tezos_micheline.Micheline

type t = Micheline_printer.node

type node = t

(* Printer nodes uses location for comment *)
type comment = Micheline_printer.location

open Micheline_printer

let no_comment = {comment = None}

let set_comment n c =
  match c, n with
  | None, _ -> n
  | Some _, Int ({comment = None}, x) -> Int ({comment = c}, x)
  | Some _, String ({comment = None}, x) -> String ({comment = c}, x)
  | Some _, Bytes ({comment = None}, x) -> Bytes ({comment = c}, x)
  | Some _, Prim ({comment = None}, x, y, z) -> Prim ({comment = c}, x, y, z)
  | Some _, Seq ({comment = None}, x) -> Seq ({comment = c}, x)
  | Some s1, Int ({comment = Some s2}, x) ->
      Int ({comment = Some (s1 ^ "\n" ^ s2)}, x)
  | Some s1, String ({comment = Some s2}, x) ->
      String ({comment = Some (s1 ^ "\n" ^ s2)}, x)
  | Some s1, Bytes ({comment = Some s2}, x) ->
      Bytes ({comment = Some (s1 ^ "\n" ^ s2)}, x)
  | Some s1, Prim ({comment = Some s2}, x, y, z) ->
      Prim ({comment = Some (s1 ^ "\n" ^ s2)}, x, y, z)
  | Some s1, Seq ({comment = Some s2}, x) ->
      Seq ({comment = Some (s1 ^ "\n" ^ s2)}, x)

let string s = String (no_comment, s)
let bytes s =
  let s =
    match String.sub s 0 2 with
    | "0x" -> String.(sub s 2 (length s - 2))
    | _ | exception _ -> s
  in
  Bytes (no_comment, Bytes.of_string & Hex.to_string (`Hex s))
let int n = Int (no_comment, n)
let prim s ts annots = Prim (no_comment, s, ts, annots)
let seq ts = Seq (no_comment, ts)

let pp_custom ppf =
  let one_liner = function
    | Int _ | String _ | Bytes _ -> true
    | Prim (_c, _p, [], []) -> true
    | _ -> false
  in
  let comment ppf {comment = copt} =
    match copt with
    | None -> ()
    | Some s ->
        let lines = String.split_on_char '\n' s in
        Format.fprintf
          ppf
          " @[<v>%a@]"
          (Format.list "@," (fun ppf l -> Format.fprintf ppf "# %s" l))
          lines
  in
  let rec pp ppf = function
    | Int (c, z) -> Format.fprintf ppf "%s%a" (Z.to_string z) comment c
    | String (c, s) -> Format.fprintf ppf "%S%a" s comment c
    | Bytes (c, b) ->
        Format.fprintf
          ppf
          "0x%s%a"
          (let (`Hex h) = Hex.of_bytes b in
           h)
          comment
          c
    | Prim (c, p, [], annot) ->
        Format.fprintf
          ppf
          "%s%s%a"
          p
          (if annot = [] then "" else String.concat " " ("" :: annot))
          comment
          c
    | Prim (c, p, args, annot) ->
        if List.for_all one_liner args then
          Format.fprintf
            ppf
            "%s%s @[<h>%a@]%a"
            p
            (if annot = [] then "" else String.concat " " ("" :: annot))
            (Format.list "@ " (fun ppf x ->
                 let parens =
                   match x with
                   | Int _ | String _ | Bytes _ | Prim (_, _, [], []) | Seq _
                     ->
                       false
                   | _ -> true
                 in
                 if parens then Format.fprintf ppf "(%a)" pp x else pp ppf x))
            args
            comment
            c
        else
          Format.fprintf
            ppf
            "%s%s @[<v>%a@]%a"
            p
            (if annot = [] then "" else String.concat " " ("" :: annot))
            (Format.list "@ " (fun ppf x ->
                 let parens =
                   match x with
                   | Int _ | String _ | Bytes _ | Prim (_, _, [], []) | Seq _
                     ->
                       false
                   | _ -> true
                 in
                 if parens then Format.fprintf ppf "(%a)" pp x else pp ppf x))
            args
            comment
            c
    | Seq (c, []) -> Format.fprintf ppf "{ }%a" comment c
    | Seq (c, nodes) ->
        Format.fprintf
          ppf
          "@[<v>{ %a@ }%a@]"
          (Format.list "@,; " pp)
          nodes
          comment
          c
  in
  pp ppf

let wrap_format f ppf x =
  let buf = Buffer.create 10000 in
  let sppf = Format.formatter_of_buffer buf in
  Format.pp_set_margin sppf 199999 ;
  Format.pp_set_max_indent sppf 99999 ;
  Format.pp_set_max_boxes sppf 99999 ;
  f sppf x ;
  Format.fprintf sppf "%!" ;
  let lines = String.split_on_char '\n' (Buffer.contents buf) in
  Format.pp_print_list
    ~pp_sep:Format.pp_force_newline
    Format.pp_print_string
    ppf
    lines

let pp_custom = wrap_format pp_custom

let use_pp_custom = ref false

let pp ppf = (if !use_pp_custom then pp_custom else print_expr_unwrapped) ppf

let save pp fn x =
  let oc = open_out fn in
  let ppf = Format.formatter_of_out_channel oc in
  Format.fprintf ppf "%a@." pp x ;
  close_out oc

module Parse = struct
  type location = Micheline_parser.location

  type t = (location, string) Micheline.node

  type parsed = t

  let to_parsed = Micheline.map_node (fun _ -> Micheline_parser.location_zero) (fun x -> x)

  let of_parsed = Micheline.map_node (fun _ -> { Micheline_printer.comment= None }) (fun x -> x)

  let parse_expression_string ?(check_indentation=false) s =
    let open Micheline_parser in
    let open Result.Infix in
    no_parsing_error (tokenize s) >>= fun tokens ->
    no_parsing_error (parse_expression ~check:check_indentation tokens)

  let parse_toplevel_string ?(check_indentation=false) s =
    let open Micheline_parser in
    let open Result.Infix in
    no_parsing_error (tokenize s) >>= fun tokens ->
    no_parsing_error (parse_toplevel ~check:check_indentation tokens) >>| fun nodes ->
    Seq (location_zero, nodes)

  (* Micheline_parser has no good interface for stream.
     Therefore we load the entire string then parse.
  *)

  let parse_expression_file ?check_indentation fn =
    match File.to_string fn with
    | Error (`Exn e) -> Tezos_error_monad.Error_monad.error_with_exn e
    | Ok s -> parse_expression_string ?check_indentation s

  let parse_toplevel_file ?check_indentation fn =
    match File.to_string fn with
    | Error (`Exn e) -> Tezos_error_monad.Error_monad.error_with_exn e
    | Ok s -> parse_toplevel_string ?check_indentation s
end

type location = Parse.location

let pp_location = Tezos_micheline.Micheline_parser.print_location

type parsed = Parse.parsed

module Encoding = struct
  type t = Michelson_v1_primitives.prim Micheline.canonical
  type expr = t
  type lazy_expr = expr Data_encoding.lazy_t

  let expr_encoding =
    Tezos_micheline.Micheline_encoding.canonical_encoding_v1
      ~variant:"michelson_v1"
      Michelson_v1_primitives.prim_encoding

  let lazy_expr expr = Data_encoding.make_lazy expr_encoding expr

  let lazy_expr_encoding = Data_encoding.lazy_encoding expr_encoding

  let to_lazy_expr (t : (_, string) Tezos_micheline.Micheline.node) : (lazy_expr, _) result =
    let open Result.Syntax in
    let+ e = Michelson_v1_primitives.prims_of_strings & Micheline.strip_locations t in
    lazy_expr e

  let to_expr (t : (_, string) Tezos_micheline.Micheline.node) : (expr, _) result =
    Michelson_v1_primitives.prims_of_strings & Micheline.strip_locations t

  let to_bytes t =
    match to_expr t with
    | Error es ->
        Format.eprintf "XXX %a@." Tzerror.pp es;
        assert false
    | Ok x -> Data_encoding.Binary.to_bytes_exn expr_encoding x

  let to_lazy_bytes t =
    match to_lazy_expr t with
    | Error _ -> assert false
    | Ok x -> Data_encoding.Binary.to_bytes_exn lazy_expr_encoding x
end

module type S = sig
  type t
  val to_micheline : t -> node
  val of_micheline : Parse.parsed -> (t, Parse.location * string) result
  val pp : Format.formatter -> t -> unit
end
