open SCaml

(** Token type is uniquely identified on the chain by a pair composed of the token
    contract address and token ID, a natural number (nat). If the underlying contract
    implementation supports only a single token type (e.g. ERC-20-like contract),
    the token ID MUST be 0n. In the case when multiple token types are supported
    within the same FA2 token contract (e. g. ERC-1155-like contract), the contract
    is fully responsible for assigning and managing token IDs.
*)
type tokenID = nat

module Token_metadata = struct
  (* token metadata *)

  type info = (string, bytes) map

  (* TZIP-012 predefined keys

     "" (empty-string): should correspond to a TZIP-016 URI which points to a JSON
                        representation of the token metadata.

     "name": should be a UTF-8 string giving a “display name” to the token.

     "symbol": should be a UTF-8 string for the short identifier of the token
               (e.g. XTZ, EUR, …).

     "decimals": should be an integer (converted to a UTF-8 string in decimal)
                 which defines the position of the decimal point in token balances for display
                 purposes.
  *)
  let key_tzip_016_uri = ""
  let key_name = "name"
  let key_suymobl = "symbol"
  let key_decimals = "decimals"

  type binding = {token_id : tokenID; token_info : info}
end

module Contract_metadata = struct
  type t = (string, bytes) big_map
end

type accounts = (address * tokenID, nat) big_map

type storage = {
  accounts : accounts;
  operators : (address * tokenID, address set) big_map;
  token_metadata : (tokenID, Token_metadata.binding) big_map;
  (* tokenID is duped, but TZIP-012 specifies this. *)
  metadata : Contract_metadata.t;
}

(* entry point transfer *)

type tx = {to_ : address; token_id : tokenID; amount : nat}

type transfer = {from_ : address; txs : tx list}

exception FA2_INSUFFICIENT_BALANCE

let check_operator from (token_id : tokenID) operators =
  let sender = Global.get_sender () in
  sender = from
  ||
  match BigMap.get (from, token_id) operators with
  | None -> false
  | Some set -> Set.mem sender set

exception FA2_NOT_OPERATOR

(* Behaviour: operator_transfer_policy == Owner_or_operator_transfer *)
let do_tx from {to_; token_id; amount} operators accounts =
  if not @@ check_operator from token_id operators then raise FA2_NOT_OPERATOR ;
  let none_when_0 x = if x = Nat 0 then None else Some x in
  let from_amount = BigMap.get_with_default (from, token_id) accounts (Nat 0) in
  if from_amount < amount then raise FA2_INSUFFICIENT_BALANCE ;
  (* This update is mandatory before searching to_map.  Otherwise the funciton
     can mint [amount] of the token when [from = to_] *)
  let accounts =
    BigMap.update (from, token_id) (none_when_0 from_amount) accounts
  in
  let to_amount =
    BigMap.get_with_default (to_, token_id) accounts (Nat 0) +^ amount
  in
  BigMap.update (to_, token_id) (none_when_0 to_amount) accounts

let do_transfer operators accounts {from_; txs} =
  List.fold_left
    (fun accounts tx -> do_tx from_ tx operators accounts)
    accounts
    txs

let[@entry] transfer transfers storage =
  ( [],
    {
      storage with
      accounts =
        List.fold_left
          (fun accounts transfer ->
            do_transfer storage.operators accounts transfer)
          storage.accounts
          transfers;
    } )

(* entry point balance_of *)

type request = {owner : address; token_id : nat}

type callback_param = {request : request; balance : nat}

type balance_of = {
  requests : request list;
  callback : callback_param list contract;
}

exception FA2_TOKEN_UNDEFINED

let[@entry] balance_of {requests; callback} ({accounts; _} as storage) =
  let param =
    List.map
      (fun ({owner; token_id} as request) ->
        {
          request;
          balance = BigMap.get_with_default (owner, token_id) accounts (Nat 0);
        })
      requests
  in
  [Operation.transfer_tokens param (Tz 0.) callback], storage

(* update operators *)

type allowance = {owner : address; operator : address; token_id : nat}

type operator_update =
  | Add_operator of allowance
  | Remove_operator of allowance

exception FA2_OPERATORS_UNSUPPORTED

(* Behaviour: only the owner can change his/her operators *)
let do_update_operator operators update =
  match update with
  | Add_operator {owner; operator; token_id} ->
      if Global.get_sender () <> owner then raise FA2_OPERATORS_UNSUPPORTED ;
      BigMap.update
        (owner, token_id)
        (Some
           (Set.update operator true
           @@ BigMap.get_with_default (owner, token_id) operators Set.empty))
        operators
  | Remove_operator {owner; operator; token_id} -> (
      if Global.get_sender () <> owner then raise FA2_OPERATORS_UNSUPPORTED ;
      match BigMap.get (owner, token_id) operators with
      | None -> (* nop *) operators
      | Some set ->
          let setopt =
            let set = Set.update operator false set in
            if Set.length set = Nat 0 then None (* We need Set.is_empty *)
            else Some set
          in
          BigMap.update (owner, token_id) setopt operators)

let[@entry] update_operators updates ({operators; _} as storage) =
  ( [],
    {
      storage with
      operators = List.fold_left do_update_operator operators updates;
    } )
