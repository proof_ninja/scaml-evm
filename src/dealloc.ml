(* f Dealloc *)
open Tools
open IML

module Ids = struct
  include Set.Make (Ident)
  let _pp ppf ids =
    Format.fprintf ppf "@[%a@]" (Format.list "@ " Ident.pp) (elements ids)
end

open Ids

let rec f vs (* vars must not be dealloced since they are used later *) t0 :
    Ids.t (* vars required to evaluate [t0] *) * IML.t
    (* [t0] plus explicit deallocations *) =
  let f' vs t0 =
    let mk desc = {t0 with desc} in
    let deallocs vs t = Ids.fold (fun id t -> mk & Dealloc (t, id)) vs t in
    let deallocs_then vs t =
      Ids.fold (fun id t -> mk & Dealloc_then (id, t)) vs t
    in
    let remove pv vs = remove pv.desc vs in
    let remove_abs pvs vs t =
      List.fold_left
        (fun (vs, t) pv ->
          let t =
            if not & mem pv.desc vs then deallocs_then (singleton pv.desc) t
            else t
          in
          Ids.remove pv.desc vs, t)
        (vs, t)
        pvs
    in
    let merge vs2 t2 vs3 t3 =
      let t2 = deallocs_then (diff vs3 vs2) t2 in
      let t3 = deallocs_then (diff vs2 vs3) t3 in
      union vs2 vs3, t2, t3
    in
    match t0.desc with
    | Dealloc _ -> assert false
    | Dealloc_then _ -> assert false
    | Var id ->
        if mem id vs then vs, t0 else add id vs, deallocs (singleton id) t0
    | Const _ | Nil | IML_None | AssertFalse -> vs, t0
    | IML_Some t ->
        let vs, t = f vs t in
        vs, mk & IML_Some t
    | Left t ->
        let vs, t = f vs t in
        vs, mk & Left t
    | Right t ->
        let vs, t = f vs t in
        vs, mk & Right t
    | Assert t ->
        let vs, t = f vs t in
        vs, mk & Assert t
    | Seq (t1, t2) ->
        let vs, t2 = f vs t2 in
        let vs, t1 = f vs t1 in
        vs, mk & Seq (t1, t2)
    | Cons (t1, t2) ->
        (* evaluate tl first *)
        let vs, t1 = f vs t1 in
        let vs, t2 = f vs t2 in
        vs, mk & Cons (t1, t2)
    | Pair (t1, t2) ->
        (* evaluate right first *)
        let vs, t1 = f vs t1 in
        let vs, t2 = f vs t2 in
        vs, mk & Pair (t1, t2)
    | IfThenElse (t1, t2, Some t3) ->
        let vs2, t2 = f vs t2 in
        let vs3, t3 = f vs t3 in
        let vs, t2, t3 = merge vs2 t2 vs3 t3 in
        let vs, t1 = f vs t1 in
        vs, mk & IfThenElse (t1, t2, Some t3)
    | IfThenElse (t1, t2, None) ->
        let vs2, t2 = f vs t2 in
        let vs3 = vs in
        let vs, t2, t3 = merge vs2 t2 vs3 (mkunit ~loc:t0.loc ()) in
        let vs, t1 = f vs t1 in
        vs, mk & IfThenElse (t1, t2, Some t3)
    | Fun (pv, t) ->
        (* fvars in a closure are implicitly used.
           If their uses are the last ones, they must be dealloc'ed *)
        let vs0 = vs in
        let vs, t = f empty t in
        let vs, t = remove_abs [pv] vs t in
        union vs vs0, deallocs (diff vs vs0) & mk & Fun (pv, t)
    | App (t, ts) ->
        (* function is evaluated first.
           args are evaluated from the last *)
        let vs, rev_ts =
          List.fold_left
            (fun (vs, ts) t ->
              let vs, t = f vs t in
              vs, t :: ts)
            (vs, [])
            ts
        in
        let vs, t = f vs t in
        vs, mk & App (t, List.rev rev_ts)
    | Prim ("List.map", ty, [({desc = Fun (pv, body)} as func); xs]) ->
        let vs0 = vs in
        let fvars =
          Ids.of_list
          @@ List.map (fun x -> x.desc)
          @@ Idents.to_list @@ freevars body
        in
        (* free vars of body must not be deallocated inside *)
        let vs, body = f (Ids.remove pv.desc fvars) body in
        let body =
          if Ids.mem pv.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pv.desc) body
        in
        let vs = Ids.remove pv.desc vs in
        let ds = diff vs vs0 in
        let vs = union vs vs0 in
        let vs, xs = f vs xs in
        let t =
          mk & Prim ("List.map", ty, [{func with desc = Fun (pv, body)}; xs])
        in
        vs, deallocs ds t
    | Prim ("List.fold_left'", ty, [({desc = Fun (pv, body)} as func); init; xs])
      ->
        let vs0 = vs in
        let fvars =
          Ids.of_list
          @@ List.map (fun x -> x.desc)
          @@ Idents.to_list @@ freevars body
        in
        (* free vars of body must not be deallocated inside *)
        let vs, body = f (Ids.remove pv.desc fvars) body in
        let body =
          if Ids.mem pv.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pv.desc) body
        in
        let vs = Ids.remove pv.desc vs in
        let ds = diff vs vs0 in
        let vs = union vs vs0 in
        let vs, init = f vs init in
        let vs, xs = f vs xs in
        let t =
          mk
          & Prim
              ( "List.fold_left'",
                ty,
                [{func with desc = Fun (pv, body)}; init; xs] )
        in
        vs, deallocs ds t
    | Prim
        ( "List.fold_left",
          ty,
          [
            ({desc = Fun (pv, ({desc = Fun (pv', body)} as ifunc))} as func);
            init;
            xs;
          ] ) ->
        (* List.fold_left' (fun x -> let (pv,pv') = x in body) init xs *)
        let vs0 = vs in
        let fvars =
          Ids.of_list
          @@ List.map (fun x -> x.desc)
          @@ Idents.to_list @@ freevars body
        in
        (* free vars of body must not be deallocated inside *)
        let vs, body =
          f (Ids.remove pv'.desc (Ids.remove pv.desc fvars)) body
        in
        let body =
          if Ids.mem pv.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pv.desc) body
        in
        let body =
          if Ids.mem pv'.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pv'.desc) body
        in
        let vs = Ids.remove pv'.desc (Ids.remove pv.desc vs) in
        let ds = diff vs vs0 in
        let vs = union vs vs0 in
        let vs, init = f vs init in
        let vs, xs = f vs xs in
        let t =
          mk
          & Prim
              ( "List.fold_left",
                ty,
                [
                  {
                    func with
                    desc = Fun (pv, {ifunc with desc = Fun (pv', body)});
                  };
                  init;
                  xs;
                ] )
        in
        vs, deallocs ds t
    | Prim
        ( (("Set.fold'" | "Map.fold'") as n),
          ty,
          [({desc = Fun (pv, body)} as func); init; xs] ) ->
        let vs0 = vs in
        let fvars =
          Ids.of_list
          @@ List.map (fun x -> x.desc)
          @@ Idents.to_list @@ freevars body
        in
        (* free vars of body must not be deallocated inside *)
        let vs, body = f (Ids.remove pv.desc fvars) body in
        let body =
          if Ids.mem pv.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pv.desc) body
        in
        let vs = Ids.remove pv.desc vs in
        let ds = diff vs vs0 in
        let vs = union vs vs0 in
        let vs, init = f vs init in
        let vs, xs = f vs xs in
        let t =
          mk & Prim (n, ty, [{func with desc = Fun (pv, body)}; init; xs])
        in
        vs, deallocs ds t
    | Prim
        ( "Set.fold",
          ty,
          [
            ({desc = Fun (pv, ({desc = Fun (pv', body)} as ifunc))} as func);
            init;
            xs;
          ] ) ->
        let vs0 = vs in
        let fvars =
          Ids.of_list
          @@ List.map (fun x -> x.desc)
          @@ Idents.to_list @@ freevars body
        in
        (* free vars of body must not be deallocated inside *)
        let vs, body =
          f (Ids.remove pv'.desc (Ids.remove pv.desc fvars)) body
        in
        let body =
          if Ids.mem pv.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pv.desc) body
        in
        let body =
          if Ids.mem pv'.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pv'.desc) body
        in
        let vs = Ids.remove pv'.desc (Ids.remove pv.desc vs) in
        let ds = diff vs vs0 in
        let vs = union vs vs0 in
        let vs, init = f vs init in
        let vs, xs = f vs xs in
        let t =
          mk
          & Prim
              ( "Set.fold",
                ty,
                [
                  {
                    func with
                    desc = Fun (pv, {ifunc with desc = Fun (pv', body)});
                  };
                  init;
                  xs;
                ] )
        in
        vs, deallocs ds t
    | Prim
        ( "Map.fold",
          ty,
          [
            ({
               desc =
                 Fun
                   ( pk,
                     ({desc = Fun (pv, ({desc = Fun (pacc, body)} as iifunc))}
                     as ifunc) );
             } as func);
            init;
            xs;
          ] ) ->
        let vs0 = vs in
        let fvars =
          Ids.of_list
          @@ List.map (fun x -> x.desc)
          @@ Idents.to_list @@ freevars body
        in
        (* free vars of body must not be deallocated inside *)
        let vs, body =
          f
            (Ids.remove
               pk.desc
               (Ids.remove pv.desc (Ids.remove pacc.desc fvars)))
            body
        in
        let body =
          if Ids.mem pk.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pk.desc) body
        in
        let body =
          if Ids.mem pv.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pv.desc) body
        in
        let body =
          if Ids.mem pacc.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pacc.desc) body
        in
        let vs =
          Ids.remove pk.desc (Ids.remove pv.desc (Ids.remove pacc.desc vs))
        in
        let ds = diff vs vs0 in
        let vs = union vs vs0 in
        let vs, init = f vs init in
        let vs, xs = f vs xs in
        let t =
          mk
          & Prim
              ( "Map.fold",
                ty,
                [
                  {
                    func with
                    desc =
                      Fun
                        ( pk,
                          {
                            ifunc with
                            desc =
                              Fun (pv, {iifunc with desc = Fun (pacc, body)});
                          } );
                  };
                  init;
                  xs;
                ] )
        in
        vs, deallocs ds t
    | Prim ("Map.map'", ty, [({desc = Fun (pkv, body)} as func); xs]) ->
        let vs0 = vs in
        let fvars =
          Ids.of_list
          @@ List.map (fun x -> x.desc)
          @@ Idents.to_list @@ freevars body
        in
        (* free vars of body must not be deallocated inside *)
        let vs, body = f (Ids.remove pkv.desc fvars) body in
        let body =
          if Ids.mem pkv.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pkv.desc) body
        in
        let vs = Ids.remove pkv.desc vs in
        let ds = diff vs vs0 in
        let vs = union vs vs0 in
        let vs, xs = f vs xs in
        let t =
          mk & Prim ("Map.map'", ty, [{func with desc = Fun (pkv, body)}; xs])
        in
        vs, deallocs ds t
    | Prim
        ( "Map.map",
          ty,
          [({desc = Fun (pk, ({desc = Fun (pv, body)} as ifunc))} as func); xs]
        ) ->
        let vs0 = vs in
        let fvars =
          Ids.of_list
          @@ List.map (fun x -> x.desc)
          @@ Idents.to_list @@ freevars body
        in
        (* free vars of body must not be deallocated inside *)
        let vs, body = f (Ids.remove pk.desc (Ids.remove pv.desc fvars)) body in
        let body =
          if Ids.mem pk.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pk.desc) body
        in
        let body =
          if Ids.mem pv.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pv.desc) body
        in
        let vs = Ids.remove pk.desc (Ids.remove pv.desc vs) in
        let ds = diff vs vs0 in
        let vs = union vs vs0 in
        let vs, xs = f vs xs in
        let t =
          mk
          & Prim
              ( "Map.map",
                ty,
                [
                  {func with desc = Fun (pk, {ifunc with desc = Fun (pv, body)})};
                  xs;
                ] )
        in
        vs, deallocs ds t
    | Prim ("Option.map", ty, [({desc = Fun (pv, body)} as func); xs]) ->
        let vs0 = vs in
        let fvars =
          Ids.of_list
          @@ List.map (fun x -> x.desc)
          @@ Idents.to_list @@ freevars body
        in
        (* free vars of body must not be deallocated inside *)
        let vs, body = f (Ids.remove pv.desc fvars) body in
        let body =
          if Ids.mem pv.desc fvars then body
          else
            (* if the parameter is never used, it must be dealloced here *)
            deallocs (Ids.singleton pv.desc) body
        in
        let vs = Ids.remove pv.desc vs in
        let ds = diff vs vs0 in
        let vs = union vs vs0 in
        let vs, xs = f vs xs in
        let t =
          mk & Prim ("Option.map", ty, [{func with desc = Fun (pv, body)}; xs])
        in
        vs, deallocs ds t
    | Prim (p, ty, ts) ->
        (* args are evaluated from the last *)
        let vs, rev_ts =
          List.fold_left
            (fun (vs, ts) t ->
              let vs, t = f vs t in
              vs, t :: ts)
            (vs, [])
            ts
        in
        vs, mk & Prim (p, ty, List.rev rev_ts)
    | Let (pv, t1, t2) ->
        let vs2, t2 = f vs t2 in
        let t2 =
          let ids =
            Ids.of_list (List.filter (fun pv -> not & mem pv vs2) [pv.desc])
          in
          deallocs_then ids t2
        in
        let vs, t1 = f (remove pv vs2) t1 in
        vs, mk & Let (pv, t1, t2)
    | LetRec (pv, t1, t2) ->
        let vs2, t2 = f vs t2 in
        let t2 =
          let ids =
            Ids.of_list (List.filter (fun pv -> not & mem pv vs2) [pv.desc])
          in
          deallocs_then ids t2
        in
        let vs, t1 = f vs2 t1 in
        (* pv might be used in t1 *)
        remove pv vs, mk & LetRec (pv, t1, t2)
    | Unpair (pv1, pv2, t1, t2) ->
        let vs2, t2 = f vs t2 in
        let t2 =
          deallocs_then
            (Ids.of_list
               (List.filter (fun pv -> not & mem pv vs2) [pv1.desc; pv2.desc]))
            t2
        in
        let vs, t1 = f (remove pv1 & remove pv2 vs2) t1 in
        vs, mk & Unpair (pv1, pv2, t1, t2)
    | Switch_or (t1, pv2, t2, pv3, t3) ->
        let vs2, t2 = f vs t2 in
        let vs2, t2 = remove_abs [pv2] vs2 t2 in
        let vs3, t3 = f vs t3 in
        let vs3, t3 = remove_abs [pv3] vs3 t3 in
        let vs, t2, t3 = merge vs2 t2 vs3 t3 in
        let vs, t1 = f vs t1 in
        vs, mk & Switch_or (t1, pv2, t2, pv3, t3)
    | Switch_cons (t1, pv, pv', t2, t3) ->
        let vs2, t2 = f vs t2 in
        let vs2, t2 = remove_abs [pv; pv'] vs2 t2 in
        let vs3, t3 = f vs t3 in
        let vs, t2, t3 = merge vs2 t2 vs3 t3 in
        let vs, t1 = f vs t1 in
        vs, mk & Switch_cons (t1, pv, pv', t2, t3)
    | Switch_none (t1, t2, pv3, t3) ->
        let vs2, t2 = f vs t2 in
        let vs3, t3 = f vs t3 in
        let vs3, t3 = remove_abs [pv3] vs3 t3 in
        let vs, t2, t3 = merge vs2 t2 vs3 t3 in
        let vs, t1 = f vs t1 in
        vs, mk & Switch_none (t1, t2, pv3, t3)
    | Contract_create (cs, loc, t1, t2, t3) ->
        let vs, t3 = f vs t3 in
        let vs, t2 = f vs t2 in
        let vs, t1 = f vs t1 in
        vs, mk & Contract_create (cs, loc, t1, t2, t3)
    | Set _ | Map _ | BigMap _ ->
        (* args do not have variables (XXX for now) *)
        vs, t0
  in
  let vs, t0 = f' vs t0 in
  let fvs = freevars t0 in
  IML.Idents.iter
    (fun {desc = id} ->
      if not (Ids.mem id vs) then begin
        Format.eprintf
          "Dealloc.f: %a is not counted in@.%a@."
          Ident.pp
          id
          IML.pp
          t0 ;
        assert false
      end)
    fvs ;
  vs, t0

let attach t = snd & f empty t
