(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2020  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Tools
open Parsetree
open Ast_mapper

let _Int ~loc s =
  [%expr SCaml._Int [%e Ast_helper.(Exp.constant (Const.string s))]]

let _Nat ~loc s =
  [%expr SCaml._Nat [%e Ast_helper.(Exp.constant (Const.string s))]]

let _Mutez ~loc s =
  [%expr SCaml._Mutez [%e Ast_helper.(Exp.constant (Const.string s))]]

let _Set ~loc e =
  [%expr SCaml._Set [%e e]]

let _Map ~loc e =
  [%expr SCaml._Map [%e e]]

let _BigMap ~loc e =
  [%expr SCaml._BigMap [%e e]]

let parse_tz f =
  try
    let pos = String.index f '.' in
    let dec = String.sub f 0 pos in

    let sub = String.sub f (pos + 1) (String.length f - pos - 1) in
    let all_dec s =
      for i = 0 to String.length s - 1 do
        match String.unsafe_get s i with
        | '0' .. '9' -> ()
        | _ -> failwith "Tz can only take simple decimal floats"
      done
    in
    all_dec dec ;
    all_dec sub ;
    let sub =
      if String.length sub > 6 then
        failwith "The smallest expressive franction of tz is micro" ;
      sub ^ String.init (6 - String.length sub) (fun _ -> '0')
    in
    let z = Z.of_string (dec ^ sub) in
    if z > Michelson.maximum_mutez then failwith "The value is too large for tz" ;
    Ok (Z.to_string z)
  with
  | Failure s -> Error s
  | _exn -> Error "Tz can only take simple decimal floats"

(* Replace pattern [Int 1] to [__scaml_int_n] and returns
   [(__scaml_int_n, Int 1)] to build when clause in [fix_case] *)
let fix_pattern pat =
  let cntr = ref 0 in
  let vars = ref [] in
  let pattern super =
    let pat self p =
      let loc = p.ppat_loc in
      match p.ppat_desc with
      | Ppat_constant (Pconst_integer (s, Some 'i'))
      | Ppat_construct ({txt= Longident.(Lident "Int" | Ldot (Lident "SCaml", "Int")); loc=_},
                        Some ([], {ppat_desc= Ppat_constant (Pconst_integer (s, None))})) ->
          let txt = incr cntr; Printf.sprintf "__scaml_int_%d" !cntr in
          vars := (txt, _Int ~loc s) :: !vars;
          {p with ppat_desc= Ppat_var ({txt; loc})}
      | Ppat_construct ({txt= Longident.(Lident "Int" | Ldot (Lident "SCaml", "Int")); loc=_},
                        Some ([], _)) ->
          Error.raisef Pattern_match ~loc:p.ppat_loc "Int must take a number literal"

      | Ppat_constant (Pconst_integer (s, Some 'n'))
      | Ppat_construct ({txt= Longident.(Lident "Nat" | Ldot (Lident "SCaml", "Nat")); loc=_},
                        Some ([], {ppat_desc= Ppat_constant (Pconst_integer (s, None))})) ->
          let txt = incr cntr; Printf.sprintf "__scaml_nat_%d" !cntr in
          vars := (txt, _Nat ~loc s) :: !vars;
          {p with ppat_desc= Ppat_var ({txt; loc})}
      | Ppat_construct ({txt= Longident.(Lident "Nat" | Ldot (Lident "SCaml", "Nat")); loc=_},
                        Some ([], _)) ->
          Error.raisef Pattern_match ~loc:p.ppat_loc "Nat must take a number literal"

      | Ppat_constant (Pconst_float (s, Some 't'))
      | Ppat_construct ({txt= Longident.(Lident "Tz" | Ldot (Lident "SCaml", "Tz")); loc=_},
                        Some ([], {ppat_desc= Ppat_constant (Pconst_float (s, None))})) ->
          let txt = incr cntr; Printf.sprintf "__scaml_tz_%d" !cntr in
          begin match parse_tz s with
            | Ok s ->
                vars := (txt, _Mutez ~loc s) :: !vars;
                {p with ppat_desc= Ppat_var ({txt; loc})}
            | Error e ->
                Error.raisef
                  Pattern_match
                  ~loc:p.ppat_loc
                  "%s" e
          end
      | Ppat_construct ({txt= Longident.(Lident "Tz" | Ldot (Lident "SCaml", "Tz")); loc=_},
                        Some ([], _)) ->
          Error.raisef Pattern_match ~loc:p.ppat_loc "Tz must take a number literal"

      | Ppat_construct ({txt= Longident.(Lident "Set" | Ldot (Lident "SCaml", "Set")); loc=_},
                        Some _) ->
          Error.raisef
            Pattern_match
            ~loc:p.ppat_loc
            "Set literal is not allowed in patterns"

      | Ppat_construct ({txt= Longident.(Lident "Map" | Ldot (Lident "SCaml", "Map")); loc=_},
                        Some _) ->
          Error.raisef
            Pattern_match
            ~loc:p.ppat_loc
            "Map literal is not allowed in patterns"

      | Ppat_construct ({txt= Longident.(Lident "BigMap" | Ldot (Lident "SCaml", "BigMap")); loc=_},
                        Some _) ->
          Error.raisef
            Pattern_match
            ~loc:p.ppat_loc
            "BigMap literal is not allowed in patterns"

      | _ -> super.pat self p
    in
    { super with pat }
  in
  let mapper = pattern default_mapper in
  let pat = mapper.pat mapper pat in
  pat, !vars

(* Replace patterns like [Int 1] by [__scaml_int_n] then add a guard
   [__scaml_int_n = Int 1] *)
let fix_case case =
  let pc_lhs, vars = fix_pattern case.pc_lhs in
  let guards =
    List.map (fun (s,e) ->
        let loc = e.pexp_loc in
        [%expr [%e Ast_helper.Exp.ident {txt= Longident.Lident s; loc}] = [%e e]]) vars
    @ match case.pc_guard with None -> [] | Some g -> [g]
  in
  let pc_guard =
    match guards with
    | [] -> None
    | g0::guards ->
        Some (List.fold_left (fun acc g ->
            let loc = g.pexp_loc in
            [%expr Stdlib.(&&) [%e acc] [%e g]]) g0 guards)
  in
  { case with pc_lhs; pc_guard }

let extend super =
  let check_vbs vbs =
    List.iter (fun vb ->
        let _, xs = fix_pattern vb.pvb_pat in
        if xs <> [] then
          Error.raisef
            Pattern_match
            ~loc:vb.pvb_pat.ppat_loc
            "let cannot have Michelson number pattern literal"
      ) vbs
  in
  let expr self e =
    let loc = e.pexp_loc in
    match e.pexp_desc with
    | Pexp_constant (Pconst_integer (s, Some 'i'))
    | Pexp_construct ({txt= Longident.(Lident "Int" | Ldot (Lident "SCaml", "Int")); loc=_},
                      Some ({pexp_desc= Pexp_constant (Pconst_integer (s, None))})) ->
        _Int ~loc s
    | Pexp_construct ({txt= Longident.(Lident "Int" | Ldot (Lident "SCaml", "Int")); loc=_},
                      Some _) ->
        Error.raisef Constant ~loc "Int must take a literal number"

    | Pexp_constant (Pconst_integer (s, Some 'n'))
    | Pexp_construct ({txt= Longident.(Lident "Nat" | Ldot (Lident "SCaml", "Nat")); loc=_},
                      Some ({pexp_desc= Pexp_constant (Pconst_integer (s, None))})) ->
        _Nat ~loc s
    | Pexp_construct ({txt= Longident.(Lident "Nat" | Ldot (Lident "SCaml", "Nat")); loc=_},
                      Some _) ->
        Error.raisef Constant ~loc "Nat must take a literal number"

    | Pexp_constant (Pconst_float (s, Some 't'))
    | Pexp_construct ({txt= Longident.(Lident "Tz" | Ldot (Lident "SCaml", "Tz")); loc=_},
                      Some ({pexp_desc= Pexp_constant (Pconst_float (s, None))})) ->
        begin match parse_tz s with
          | Ok s ->
              _Mutez ~loc s
          | Error e ->
              Error.raisef
                Pattern_match
                ~loc
                "%s" e
        end
    | Pexp_construct ({txt= Longident.(Lident "Tz" | Ldot (Lident "SCaml", "Tz")); loc=_},
                      Some _) ->
        Error.raisef Constant ~loc "Tz must take a literal number"

    | Pexp_construct ({txt= Longident.(Lident "Set" | Ldot (Lident "SCaml", "Set")); loc=_},
                      Some e) ->
        _Set ~loc (self.expr self e)

    | Pexp_construct ({txt= Longident.(Lident "Map" | Ldot (Lident "SCaml", "Map")); loc=_},
                      Some e) ->
        _Map ~loc (self.expr self e)

    | Pexp_construct ({txt= Longident.(Lident "BigMap" | Ldot (Lident "SCaml", "BigMap")); loc=_},
                      Some e) ->
        _BigMap ~loc (self.expr self e)

    | Pexp_match (e', cases) ->
        super.expr self { e with pexp_desc= Pexp_match (e', List.map fix_case cases) }
    | Pexp_function cases ->
        super.expr self { e with pexp_desc= Pexp_function (List.map fix_case cases) }
    | Pexp_fun (_, _, p, _) ->
        let _, xs = fix_pattern p in
        if xs <> [] then
          Error.raisef
            Pattern_match
            ~loc:p.ppat_loc
            "fun cannot have Michelson number pattern literal"
        else super.expr self e
    | Pexp_let (_, vbs, _) ->
        check_vbs vbs;
        super.expr self e
    | _ -> super.expr self e
  in
  let structure_item self i =
    match i.pstr_desc with
    | Pstr_value (_, vbs) ->
        check_vbs vbs;
        super.structure_item self i
    | _ -> super.structure_item self i
  in
  { super with expr; structure_item }

let expr m e =
  let es =
    List.filter_map
      (fun a ->
        if a.attr_name.txt <> "scaml.replace" then None
        else
          match a.attr_payload with
          | PStr [si] -> begin
              match si.pstr_desc with Pstr_eval (e, _) -> Some e | _ -> None
            end
          | _ ->
              Error.raisef
                Attribute
                ~loc:a.attr_loc
                "Attribute [@scaml.replace e] takes one expression.")
      e.pexp_attributes
  in
  match es with
  | [] -> default_mapper.expr m e
  | _ :: _ :: _ ->
      Error.raisef
        Attribute
        ~loc:e.pexp_loc
        "At most one [@scaml.replace e] is allowed for each expression"
  | [e] -> m.expr m e

let mapper = extend {default_mapper with expr}
