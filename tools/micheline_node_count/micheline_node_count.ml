open SCaml_michelson

let tbl = Hashtbl.create 101

let add_by n y =
  let x =
    match Hashtbl.find_opt tbl n with
    | Some x -> x + y
    | None -> y
  in
  Hashtbl.replace tbl n x

let add n = add_by n 1

(*
type ('l, 'p) node =
  | Int of 'l * Z.t
  | String of 'l * string
  | Bytes of 'l * Bytes.t
  | Prim of 'l * 'p * ('l, 'p) node list * annot
  | Seq of 'l * ('l, 'p) node list
*)

let count node =
  let open Tezos_micheline.Micheline in
  let rec f node =
    let n =
      let arity_group n = if n >= 3 then 3 else n in
      match node with
      | Int (_,n) when Z.zero <= n && n < Z.of_int 16 -> "int constant [0..15]", 0, false
      | Int (_,n) when Z.of_int 16 <= n && n < Z.of_int 31 -> "int constant [16..31]", 0, false
      | Int (_,n) when Z.of_int 32 <= n && n < Z.of_int 64 -> "int constant [32..63]", 0, false
      | Int _ -> "int constant except [0..63]", 0, false
      | String _ -> "string constant", 0, false
      | Bytes _ -> "bytes constant", 0, false
      | Prim (_, n, ns, []) -> List.iter f ns; (n, arity_group @@ List.length ns, false)
      | Prim (_, n, ns, _) -> List.iter f ns; (n, arity_group @@ List.length ns, true)
      | Seq (_, ns) -> List.iter f ns; "sequence", 0, false
    in
    add n
  in
  f node

let () =
  let doit fn =
    prerr_endline fn;
    match Micheline.Parse.parse_toplevel_file ~check_indentation:false fn with
    | Error es ->
        Format.eprintf "%a@." Tzerror.pp es;
        assert false
    | Ok m ->
        count m
  in
  Arg.parse [] doit "stats";
  Hashtbl.iter (fun (k,arity,annot) v -> Format.printf "%d, %S, %d, %b@." v k arity annot) tbl
