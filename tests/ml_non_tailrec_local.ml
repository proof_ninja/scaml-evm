(* REJECT *)
(*
   STORAGE= Int 10
*)

open SCaml

let[@entry] main () x =
  let rec g x = if x = Int 0 then Int 0 else g (x - Int 1) + x in
  let v = g x in
  assert (v = Int 55) ;
  [], v
