open SCaml_tools
open SCaml_michelson
open New_size

let f fn =
  Format.printf "%s@." fn;
  let s = from_Ok @@ Spotlib.File.to_string fn in
  let parsed =
    match Micheline.Parse.parse_expression_string s with
    | Ok parsed -> parsed
    | Error etrace ->
        Format.eprintf "%a@." Tzerror.pp etrace;
        assert false
  in
  let m =
    match Michelson.Module.of_micheline parsed with
    | Ok m -> m
    | Error (_loc, s) -> prerr_endline s; assert false
  in
  Format.printf "%s size: %d size2: %d size3: %d size4: %d@."
    fn
    (Size.module_ m)
    (New_size2.module_ m)
    (New_size3.module_ m)
    (New_size4.module_ m)

let () =
  List.iter (fun p ->
      let path = p#path in
      if path <> "./_code" then f path
    ) @@ Unix.Find.files ["_code"]
