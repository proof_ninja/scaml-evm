(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Spotlib.Spot

let dir =
  match
    let open Command in
    exec ["opam"; "config"; "var"; "prefix"]
    |> stdout |> wait |> must_exit_with 0
  with
  | dir :: _ -> String.chop_eols dir ^/ "lib/scaml/scamlib"
  | [] -> failwith "Command 'opam config var prefix' answered nothing"
  | exception Failure s ->
      failwithf "Command 'opam config var prefix' has failed: %s" s
  | exception e ->
      failwithf
        "Command 'opam config var prefix' raised an exception: %s"
        (Printexc.to_string e)

let tbl = ["SCAMLIB", dir]

let () =
  let f outfile =
    let infile = outfile ^ ".in" in
    Spotlib.At.replace_file
      (fun k ->
        match List.assoc_opt k tbl with
        | Some v -> v
        | None -> failwithf "Variable %s is not defined" k)
      infile
      outfile
  in
  Arg.parse [] f "configure files .."
