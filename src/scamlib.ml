(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                Copyright 2019,2020,2021  DaiLambda, Inc.               *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open SCaml_tools

(* If --scaml-noscamlib is specified, None.

   If SCAMLIB is specified, SCAMLIB is chosen.

   Otherwise, `opam config var prefix`/lib/scaml/scamlib is used.
   If `opam config var prefix` does not print a directory nor crashes,
   scamlc prints out a warning and continues with None
*)
let scamlib = ref None

let init () =
  (scamlib :=
     if (Conf.get_conf ()).noscamlib then None
     else
       match Sys.getenv "SCAMLIB" with
       | dir -> Some dir
       | exception Not_found -> Some Directory.default_scamlib_path) ;
  match !scamlib with
  | None ->
      (* Explicitly specified by --scaml-noscamlib.  Users must be sure what he/she is doing. *)
      ()
  | Some dir ->
      (* Check the dir has sCaml.cmi or SCaml.cmi *)
      if not (Sys.file_exists (dir ^/ "sCaml.cmi")) then
        Error.raisef
          Installation
          ~loc:Location.none
          "sCaml.cmi is not found in directory %s"
          dir ;
      Clflags.include_dirs := !Clflags.include_dirs @ [dir]

let get_scamlib () = !scamlib
