(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019-2021  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Tools

module C = Michelson.Constant

let parse_timestamp ~loc:_ (* XXX *) s =
  Result.fmap (fun x -> C.Timestamp x) & Timestamp.parse s

let parse_bytes ~loc s =
  let s, with0x =
    match String.is_prefix' "0x" s with None -> s, false | Some s -> s, true
  in
  try
    ignore & Hex.to_string (`Hex s) ;
    let res = Ok (C.Bytes s) in
    if not with0x then
      Error.warnf ~loc Constant "Binary sequence should be prefixed by '0x'" ;
    res
  with _ -> Error "Bytes must take hex representation of bytes"

let parse_huge_nat s =
  try
    let z = Z.of_string s in
    if Z.lt z Z.zero then raise Exit else Ok z
  with _ -> Error "Illegal string for a natural number"

let parse_fr ~loc:_ s =
  let open Result.Infix in
  let open Bls12_381 in
  parse_huge_nat s >>| fun z ->
  let (`Hex h) = Hex.of_bytes (Fr.to_bytes @@ Fr.of_z z) in
  C.Bytes h

let parse_g1 ~loc:_ x y =
  let open Result.Infix in
  let open Bls12_381 in
  parse_huge_nat x >>= fun x ->
  parse_huge_nat y >>= fun y ->
  match G1.of_z_opt ~x ~y with
  | None -> Error "Not a valid G1 point"
  | Some g1 ->
      let (`Hex h) = Hex.of_bytes (G1.to_bytes g1) in
      Ok (C.Bytes h)

let parse_g2 ~loc:_ (x1, x2) (y1, y2) =
  let open Result.Infix in
  let open Bls12_381 in
  parse_huge_nat x1 >>= fun x1 ->
  parse_huge_nat x2 >>= fun x2 ->
  parse_huge_nat y1 >>= fun y1 ->
  parse_huge_nat y2 >>= fun y2 ->
  match G2.of_z_opt ~x:(x1, x2) ~y:(y1, y2) with
  | None -> Error "Not a valid G2 point"
  | Some g2 ->
      let (`Hex h) = Hex.of_bytes (G2.to_bytes g2) in
      Ok (C.Bytes h)
