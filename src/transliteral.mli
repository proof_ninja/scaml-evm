(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019-2021  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** RFC3339 time format *)
val parse_timestamp :
  loc:Location.t -> string -> (Michelson.Constant.t, string) Result.t

(** "0x0123456789" *)
val parse_bytes :
  loc:Location.t -> string -> (Michelson.Constant.t, string) Result.t

(** "12345678901234567890" *)
val parse_huge_nat : string -> (Z.t, string) Result.t

(** BLS12-381 Fr parser *)
val parse_fr :
  loc:Location.t -> string -> (Michelson.Constant.t, string) Result.t

(** BLS12-381 G1 parser *)
val parse_g1 :
  loc:Location.t -> string -> string -> (Michelson.Constant.t, string) Result.t

(** BLS12-381 G2 parser *)
val parse_g2 :
  loc:Location.t ->
  string * string ->
  string * string ->
  (Michelson.Constant.t, string) Result.t
