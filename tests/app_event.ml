(*
INPUT=Int 42
*)
open SCaml

let[@entry] default i () =
  (* XXX We need self' : string -> 'a Contract.t *)
  let s : (int, int) sum contract = Contract.self in
  let a = Contract.address s in
  let co = Contract.contract' a "event" in
  match co with
  | Some c -> [Operation.transfer_tokens (i : int) (Tz 0.) c], ()
  | None -> assert false

let[@entry] event (_ : int) () = [], ()
