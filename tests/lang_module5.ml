(*
   TZ=lang_module5.Y.tz
*)
open SCaml

module Z = struct
  module X = struct
    let x = Int 1
  end
  let z = X.x
end

module Y = struct
  let[@entry] default () () = [], assert (Z.X.x = Z.z)
end
