#!/bin/bash
set -e

# Disable the disclaimer message of tezos-node
export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y 

# Where am I?
SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)")"

# Where to work?
BUILD_DIR=$SCRIPT_DIR/_build
if [ ! -d $BUILD_DIR ]; then mkdir $BUILD_DIR; fi

dune build ../lib/SCamLib.cmxa
SCAMLIB_DIR=../_build/install/default/lib/scaml/scamlib

dune build ../_build/default/driver/main.exe 

# Compilation command
COMP="dune exec ../driver/main.exe -- --scaml-noscamlib --scaml-dump-iml --scaml-debug scaml.compile.time -I $SCAMLIB_DIR  --scaml-michelson-optimize-search-space-multiplier 1.0 --scaml-michelson-optimize-do-dijkstra true"

COMP_CHECKSUM=`md5sum ../_build/default/driver/main.exe | sed -e 's/ .*//'`

# Optional: tezos-client
TEZOS_CLIENT=`which octez-client || true`
if [ -z "$TEZOS_CLIENT" ]; then
    echo You need octez-client in PATH
    exit 1
fi

if [ -n "$TEZOS_CLIENT" ]; then
    export TEZOS_CLIENT="$TEZOS_CLIENT --protocol Psithaca2MLR --mode mockup --base-dir _build/tezos-client"

# Prepare mockup
    if [ ! -d _build/tezos-client ]; then
	echo Preparing mockup  $TEZOS_CLIENT
	mkdir -p _build/tezos-client
	$TEZOS_CLIENT create mockup --protocol-constants protocol-constants.json
	$TEZOS_CLIENT config init
    fi
fi

function check_checksum () {
    test -f _build/checksum/$1
}

function touch_checksum () {
    if [ ! -d _build/checksum ]; then
	mkdir -p _build/checksum
    fi
    touch _build/checksum/$1
}

function all_exist () {
    local i
    for i in $1
    do
	if [ ! -f $i ]; then
	    return 1
	fi
    done
    return 0
}

# Input <ML>
# Output TZS
function compile () {
    source=$1

    # Must be rejected?
    local must_reject=$(grep REJECT $source || true)

    local workdir=$BUILD_DIR/$(basename $source)
    
    # Compile it under $workdir
    if [ ! -d $workdir ]; then mkdir -p $workdir; fi
    local ml=$workdir/$(basename $source)

    local tz=`echo $ml | sed -e 's/\.ml$/.tz/'` # only for simple cases

    # TZ=.*$
    local tzs=$(grep TZ= $i || true)
    if [ -z "$tzs" ]; then
	TZS=$tz
    else
	tzs=(`echo "$tzs" | sed -e 's/.*TZ=//'`)
	TZS=${tzs[@]/#/$workdir/}
    fi

    checksum=`(echo "$COMP_CHECKSUM"; cat $source) | md5sum | sed -e 's/ .*//'`

    # Compile!
    if [ -z "$must_reject" ]; then

	if ! check_checksum $checksum || ! all_exist $TZS; then
	    echo $COMP -o $tz $ml

	    # Remove old output files
	    local prefix=`echo $ml | sed -e 's/\.ml$//'`
	    rm -f "$prefix"*
	    
	    cp $source $ml

	    (cd $SCRIPT_DIR; $COMP -o $tz $ml)

	    if ! all_exist $TZS; then
		echo "Error: output file(s) not exist: " $TZS
		exit 2
	    fi
	fi

    else
	if ! check_checksum $checksum; then
	    echo $COMP -o $tz $ml : THIS COMPILATION MUST FAIL

	    # Remove old output files
	    local prefix=`echo $ml | sed -e 's/\.ml$//'`
	    rm -f "$prefix"*
	    
	    cp $source $ml

	    if  (cd $SCRIPT_DIR; $COMP -o $tz $ml)
	    then
		echo "Error: COMPILATION UNEXPECTEDLY SUCCEEEDED"; exit 2
	    else
		echo "Ok: Compilation failed as expected"
	    fi
	fi
    fi

    # Compilation test successful
    touch_checksum $checksum

}

# Input: <code>
# Output: CONVERSION
function convert () {
    echo "converting $1 ..."
    tmp=`mktemp`
    echo "open SCaml" > $tmp
    echo "let x = $1" >> $tmp
    cat $tmp
    CONVERSION=$($COMP -I $workdir --scaml-convert-value x -impl $tmp | sed -e 's/^x: //')
    if [ -z "$CONVERSION" ]; then
	echo "conversion failure of $1"
	exit 1
    fi
    echo "converted to $CONVERSION"
}

# Input <ML> <TZS>
# Output: none
function run () {

    local ml=$1
    local tzs=$2

    checksum=`(cat $ml $tzs) | md5sum - | sed -e 's/ .*//'`

    if ! check_checksum $checksum; then

	# Must this test fail ?
	local must_fail=$(grep MUST_FAIL $ml || true)

	echo '============================ convert input and storage'

	# STORAGE=.*$
	local storage=$(grep STORAGE= $ml || true)
	if [ -z "$storage" ]; then
	    storage='Unit'
	else
	    storage=`echo "$storage" | sed -e 's/.*STORAGE=//'`
	    convert "$storage"
	    storage=$CONVERSION
	fi

	# INPUT=.*$
	local input=$(grep INPUT= $ml || true)
	if [ -z "$input" ]; then
	    input='Unit'
	else
	    input=`echo "$input" | sed -e 's/.*INPUT=//'`
	    convert "$input"
	    input=$CONVERSION
	fi

	# ENTRY=.*$
	local entry=$(grep ENTRY= $ml || true)
	if [ -z "$entry" ]; then
	    entry=""
	else
	    entry=`echo $entry | sed -e 's/.*ENTRY=//'`
	    entry=" --entrypoint $entry "
	fi

	if [ -z "$TEZOS_CLIENT" ]; then
	    echo "WARNING: NO tezos-client AVAIABLE!"
	    exit 1
	else
	    for tz in $tzs
	    do
		echo '============================ type check'

                local typecheck_option=$(grep TYPECHECK_OPTION= $ml || true)
		typecheck_option=`echo $typecheck_option | sed -e 's/.*TYPECHECK_OPTION=//'`
		
		echo Executing $TEZOS_CLIENT typecheck script $tz $typecheck_option
		
		if
    		    $TEZOS_CLIENT typecheck script $tz $typecheck_option
		then
    		    echo "type checked"
		else
    		    echo "type check failed."
    		    exit 1
		fi
		
		echo '============================ execute'
		# Really weird but --source is to set SENDER and --payer to set SOURCE
		echo "Executing $TEZOS_CLIENT run script $tz on storage $storage and input $input --source bootstrap1 --payer bootstrap2 $entry"
	    
		if [ -z "$must_fail" ]; then
    		    $TEZOS_CLIENT run script $tz on storage "$storage" and input "$input" --source bootstrap1 --payer bootstrap2 $entry
		else
    		    echo THIS TEST MUST FAIL
    		    if
        		$TEZOS_CLIENT run script $tz on storage "$storage" and input "$input" --source bootstrap1 --payer bootstrap2 $entry
    		    then
    			echo "Error: TEST UNEXPECTEDLY SUCCEEEDED"; exit 2
    		    else
    			echo "Ok: Test failed expectedly"
    		    fi
		fi
	    done
	    
	    # Execution test successful
	    touch_checksum $checksum
	fi
    fi
}

for i in $*
do

  workdir=$BUILD_DIR/$(basename $i)
  if [ ! -d $workdir ]; then
      mkdir -p $workdir
  fi
  
  echo "----- $i"    
  case "$i" in
  *.tz)
      # Do nothing if it is *.tz
      TZS="$i"
      ;;
  *)
      compile "$i"
      ;;
  esac

  # If tz Compilation is successful, and if there is tezos-client in the PATH,
  # let's try to execute it.
  if all_exist $TZS; then
      run "$i" "$TZS"
  fi
done
