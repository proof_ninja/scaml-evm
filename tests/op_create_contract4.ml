open SCaml

module N = struct
  module M = struct
    (* Contract: op_create_contract4.N.M *)

    (* lousy but we have to explicitly define type storage *)
    type storage = unit

    let[@entry] main () () = [], ()
  end
end

(* Contract: op_create_contract4 *)
let[@entry] main () () =
  let op, ad = Contract.create (module N.M) None (Tz 0.0) () in
  [op], ()
