(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Tools
open Untyped

let debug_time = Tools.Debug.register ["scaml"; "compile"; "time"]

module M = Michelson

(* -------------------------------- IML level translation and optimization *)

module Module = struct
  type t = {
    name : string;
    sourcefile : string;
    contracts : Contract.contract list;
    defs : IML.t option;
    (* with placeholder *)
    conf : Conf.opt;
  }

  let pp ppf t =
    let open Format in
    fprintf
      ppf
      "@[<v>name= %S;@ source= %S;@ global_entry= @[%a@];@ defs= @[<v>%a@];@ \
       flags= @[%a@];@]"
      t.name
      t.sourcefile
      (list ";@ " Contract.pp) t.contracts
      (Format.option IML.pp) t.defs
      Conf.pp_opt t.conf
end

(* This takes multiple toplevel [IML.t]'s.
   Inter-toplevel-expression optimization is not performed.
*)
let optimize_loop ~exclude outputprefix ts =
  let optimize = (Conf.get_conf ()).iml_optimization in
  let save outputprefix i n ts =
    match outputprefix with
    | None -> ()
    | Some outputprefix ->
        if (Conf.get_conf ()).dump_iml then
          let fn = Printf.sprintf "%s_%04d_%s.ml" outputprefix i n in
          IML.save fn ts
  in
  let phase i name f ts =
    let ts, sec = with_time & fun () -> List.map f ts in
    if !debug_time then Format.eprintf "%s %f@." name sec ;
    save outputprefix i name ts ;
    ts
  in
  if not optimize then begin
    save outputprefix 0 "input" ts ;
    let res, secs =
      with_time & fun () ->
      let ts = phase 1 "ipmatch" Optimize.inline_pmatch ts in
      (* Varable name may crash in the linked modules.  We need to alpha-conv for safety. *)
      let ts = phase 2 "aconv" (Optimize.alpha_conv ~exclude []) ts in
      (* beta reduction is required to estimate arguments of recursive functions *)
      let ts = phase 3 "beta" (Optimize.beta ~exclude (ref false)) ts in
      let ts = phase 4 "tailrec2loop" Optimize.tailrec2loop ts in
      let rec f i ts =
        let modified = ref false in
        let ts =
          phase ((i * 10) + 0) "beta" (Optimize.beta ~exclude modified) ts
        in
        if i = 100 then ts, i else if !modified then f (i + 1) ts else ts, i
      in
      let ts, i = f 1 ts in
      if !debug_time then  Format.eprintf "Iterated %d times@." i ;
      let ts = phase 9999 "unknorm" (Optimize.unknormalize ~exclude) ts in
      ts
    in
    if !debug_time then Format.eprintf "Optimized in %f secs@." secs ;
    res
  end
  else begin
    save outputprefix 0 "input" ts ;
    let res, secs =
      with_time & fun () ->
      let ts = phase 1 "ipmatch" Optimize.inline_pmatch ts in
      (* Varable name may crash in the linked modules.  We need to alpha-conv for safety. *)
      let ts = phase 2 "aconv" (Optimize.alpha_conv ~exclude []) ts in
      let ts = phase 3 "knorm" Optimize.knormalize ts in
      (* beta reduction is required to estimate arguments of recursive functions *)
      let ts = phase 4 "beta" (Optimize.beta ~exclude (ref false)) ts in
      let ts = phase 5 "tailrec2loop" Optimize.tailrec2loop ts in
      let rec f i ts =
        let modified = ref false in
        let ts =
          phase ((i * 10) + 0) "beta" (Optimize.beta ~exclude modified) ts
        in
        let ts = phase ((i * 10) + 1) "assoc" (Optimize.assoc modified) ts in
        let ts =
          phase ((i * 10) + 2) "inline" (Optimize.inline ~exclude modified) ts
        in
        let ts =
          phase ((i * 10) + 3) "elim" (Optimize.elim ~exclude modified) ts
        in
        let ts =
          phase ((i * 10) + 4) "cfold" (Optimize.cfold ~exclude modified) ts
        in
        if i = 100 then ts, i else if !modified then f (i + 1) ts else ts, i
      in
      let ts, i = f 1 ts in
      if !debug_time then Format.eprintf "Iterated %d times@." i ;
      let ts = phase 9999 "unknorm" (Optimize.unknormalize ~exclude) ts in
      ts
    in
    if !debug_time then Format.eprintf "Optimized in %f secs@." secs ;
    res
  end

let optimize ~exclude output_prefix ts =
  let ts = optimize_loop ~exclude output_prefix ts in
  List.map (Constantize.f ~do_big_map:false ~compile:None) ts

(* We need this side effect since the original OCaml compiler does not
   keep the module complation result in memory but save to files.
   Oppositely, SCaml does not save the module compilation to files
   but keep them in memory.
*)
let rev_translated = ref ([] : Module.t list)
let get_translated_modules () = List.rev !rev_translated

let with_flags_in_code str f =
  Conf.with_scaml_attrs
    (List.concat & List.map snd & Attribute.get_scaml_toplevel_attributes str)
    f

(* Translate a module to IML, then optimize.
   No compilation to Michelson.
*)
let translate_and_optimize sourcefile (* "dir/blah.ml" *) outputprefix
    (* "blah" *) modulename (* "Blah" *) implementation =
  let str = implementation.Typedtree.structure in
  Attribute.reject_SCaml_attribute_in_complex_structure str ;
  with_flags_in_code str & fun () ->
  (* translate to IML *)
  let (ems, defs), secs =
    with_time & fun () ->
    Contract.implementation sourcefile outputprefix modulename str
  in
  if !debug_time then Format.eprintf "Translated %s in %f secs@." sourcefile secs ;

  (* partial linearity check *)
  List.iter
    (fun iml ->
      (* do not check variables out of the def *)
      ignore @@ Linear.f ~permit_not_bound:true [] iml)
    defs.code ;

  (* link and optimize.  keep globals *)
  let defs =
    match defs.code with
    | [] -> None
    | _ ->
        Some
          (match
             optimize
               ~exclude:(List.map (fun x -> x.IML.desc) defs.globals)
               (Some (outputprefix ^ "_comp_"))
               [Contract.link Translate.TopLet.top_let_placeholder defs.code]
           with
          | [x] -> x
          | _ -> assert false)
  in
  let translated =
    {
      Module.name = modulename;
      sourcefile;
      contracts = ems;
      defs;
      conf = Conf.get_opt ();
    }
  in
  rev_translated := translated :: !rev_translated ;
  translated

(* -------------------------------------------- value conversion and revert *)

let constantize t =
  let compile t =
    (* XXX check the linearity each time is inefficient *)
    ignore & Linear.f [] t ;
    let t =
      match optimize ~exclude:[] None [t] with [t] -> t | _ -> assert false
    in
    ignore & Linear.f [] t ;
    (* XXX SELF in parameter and storage *)
    let t = Dealloc.attach t in
    let module Compile = Compile.Make (struct
      let allow_big_map = false
    end) in
    Compile.structure t
  in
  Constantize.f ~do_big_map:true ~compile:(Some compile) t

let convert_all _sourcefile _outputprefix _modulename { Typedtree.structure=str; _ } =
  Attribute.reject_SCaml_attribute_in_complex_structure str ;
  let ts = Contract.convert str in
  let ts =
    List.map
      (fun t ->
        match t with
        | `Type _ -> t
        (* XXX
              | `Value (ido, t) when Conf.(!flags.iml_optimization) ->
                  `Value (ido, Optimize.optimize t)
        *)
        | `Value _ -> t)
      ts
  in
  List.iter
    (function
      | `Type (id, t) ->
          Format.printf "type %s: @[%a@]@." (Ident.name id) M.Type.pp t
      | `Value (n, t) -> (
          let t = constantize t in
          match t.desc with
          | Const c -> begin
              match n with
              | None -> Format.printf "noname: @[%a@]@." M.Constant.pp c
              | Some id ->
                  Format.printf "%s: @[%a@]@." (Ident.name id) M.Constant.pp c
            end
          | _ ->
              (* XXX Find non const place and report its location *)
              Error.raisef Constant ~loc:t.loc "Constant expression expected"
          ))
    ts

let convert_value ident _sourcefile _outputprefix _modulename { Typedtree.structure=str; _ } =
  Attribute.reject_SCaml_attribute_in_complex_structure str ;
  let ts = Contract.convert str in
  let t =
    try
      List.find
        (fun t ->
          match t with
          | `Value (Some id, _) -> Ident.name id = ident
          | _ -> false)
        ts
    with Not_found ->
      Error.raisef Convert_ident ~loc:Location.none "no such value: %s" ident
  in
  match t with
  | `Value (_, t) -> (
      (* XXX
           let t = if Conf.(!flags.iml_optimization) then Optimize.optimize t else t in
      *)
      let t = constantize t in
      match t.desc with
      | Const c -> Format.printf "@[%a@]@." M.Constant.pp c
      | _ ->
          (* XXX Find non const place and report its location *)
          Error.raisef Constant ~loc:t.loc "Constant expression expected")
  | _ -> assert false

let convert_type ident _sourcefile _outputprefix _modulename { Typedtree.structure=str; _ } =
  Attribute.reject_SCaml_attribute_in_complex_structure str ;
  let ts = Contract.convert str in
  let t =
    try
      List.find
        (fun t ->
          match t with `Type (id, _) -> Ident.name id = ident | _ -> false)
        ts
    with Not_found ->
      Error.raisef Convert_ident ~loc:Location.none "no such type: %s" ident
  in
  match t with
  | `Type (_, t) -> Format.printf "@[%a@]@." M.Type.pp t
  | _ -> assert false

let revert m _sourcefile _outputprefix _modulename { Typedtree.structure=str; _ } =
  Attribute.reject_SCaml_attribute_in_complex_structure str ;
  match File.to_string m with
  | Error (`Exn e) -> raise e
  | Ok m -> (
      match Revert.do_revert str m with
      | Error e -> failwith e
      | Ok parsetree -> Format.eprintf "%a@." Pprintast.expression parsetree)

(* ------------------------------------------------------- driver interface *)

let compile sourcefile outputprefix modulename implementation =
  let f =
    match (Conf.get_conf ()).mode with
    | Compile -> fun s o m t ->
      (* Keep the compiled modules in a reference.  They are linked together later *)
      ignore @@ translate_and_optimize s o m t
    | ConvertAll -> convert_all
    | ConvertSingleValue ident -> convert_value ident
    | ConvertSingleType ident -> convert_type ident
    | Revert s -> revert s
  in
  f sourcefile outputprefix modulename implementation

(* ----------------------------------- linking and compilation to Michelson *)

(* Link contracts then compile them to Michelson *)
let link_contract opts outputdir defs contract =
  Conf.with_opt (List.fold_left Conf.merge Conf.none opts) & fun () ->
  let module Compile = Compile.Make (struct
    let allow_big_map = false
  end) in
  let outputprefix = Filename.concat outputdir contract.Contract.name in

  let link_contract () =
    Contract.put_global_abstraction contract.Contract.entry_tree
    & Contract.link contract.Contract.entry_tree.branch_code defs
  in

  let link_view view =
    (* view code : fun param storage -> .... return

       => fun param_storage ->
            let param, storage = param_storage in
            (fun param storage -> ... return)
              param storage
    *)
    let id_param = Ident.create_local "param" in
    let id_storage = Ident.create_local "storage" in
    let ty_param = view.Contract.View.ty_param in
    let ty_storage = contract.ty_storage in
    let fun_ty =
      let open Michelson.Type in
      tyLambda (tyPair (None, ty_param, None, ty_storage), view.ty_return)
    in
    let t =
      Contract.put_global_abstraction'
        ~param:(id_param, ty_param)
        ~storage:(id_storage, ty_storage)
      & Contract.link
          (IML.mke
             ~loc:Location.none (* XXX *)
             view.ty_return
             (IML.App
                ( IML.mkvar ~loc:Location.none (* XXX *) (view.ident, fun_ty),
                  [
                    IML.mkvar ~loc:Location.none (id_param, ty_param);
                    IML.mkvar ~loc:Location.none (id_storage, ty_storage);
                  ] )))
          defs
    in
    t
  in

  let (code, view_codes), secs =
    with_time & fun () -> link_contract (), List.map link_view contract.views
  in
  if !debug_time then Format.eprintf "Linked in %f secs@." secs ;

  let ts = code :: view_codes in

  (* linearity check *)
  ignore & List.map (Linear.f []) ts ;
  (* trans module IML optimization *)
  let ts = optimize ~exclude:[] (Some (outputprefix ^ "_link_")) ts in
  (* linearity check again *)
  ignore & List.map (Linear.f []) ts ;
  List.iter (Contract.check_self contract) ts ;
  if (Conf.get_conf ()).dump_iml then
    IML.save (outputprefix ^ "_link_before_dealloc.ml") ts ;
  let ts = List.map Dealloc.attach ts in
  if (Conf.get_conf ()).dump_iml then IML.save (outputprefix ^ "_link.ml") ts ;
  let codes, secs = with_time & fun () -> List.map Compile.structure ts in
  if !debug_time then Format.eprintf "Compiled in %f secs@." secs ;

  let code, views =
    match codes with
    | [] -> assert false
    | code :: view_codes ->
        ( code,
          List.map2
            (fun view view_code ->
              let name = view.Contract.View.name in
              {
                M.View.name;
                ty_param = view.ty_param;
                ty_return = view.ty_return;
                code = view_code;
              })
            contract.views
            view_codes )
  in

  {
    M.Module.parameter = contract.entry_tree.ty_param;
    storage = contract.entry_tree.ty_storage;
    code;
    views;
  }

(* Michelson level optimization *)
let optimize_michelson opts outputprefix (m : Michelson.Module.t) =
  Conf.with_opt (List.fold_left Conf.merge Conf.none opts) & fun () ->
    let conf = Conf.get_conf () in
    if not conf.michelson_optimization then m
    else begin
      let module Optimize =
        SCaml_michelson.Optimize.Make(struct
          let logger = prerr_endline
          let do_dijkstra = conf.michelson_optimization_do_dijkstra
          let search_space_multiplier = conf.michelson_optimization_search_space_multiplier
          let use_lcs_for_astar_score = false
        end)
      in
      if conf.dump_iml (* XXX bad name *) then
        Michelson.Module.save (outputprefix ^ "_before_optimize.tz") m ;
      let m, secs =
        with_time & fun () -> Optimize.module_ m
      in
      if !debug_time then
        Format.eprintf "Optimized michelson in %f secs@." secs ;
      m
      end

let check_contract_names entry_modules =
  ignore @@ List.fold_left
    (fun names em ->
      let n = em.Contract.name in
      match List.assoc_opt n names with
      | None -> (n, em) :: names
      | Some em' ->
          Error.raisef
            Link
            ~loc:em.loc
            "Another contract of the same name is defined at %a"
            Location.print_loc
            em'.loc)
    []
    entry_modules

let get_entry_modules_and_defs opts modules =
  Conf.with_opt (List.fold_left Conf.merge Conf.none opts) & fun () ->
  let defs = List.filter_map (fun m -> m.Module.defs) modules in
  let entry_modules = List.concat_map (fun m -> m.Module.contracts) modules in
  if entry_modules = [] then
    Error.warnf Entry ~loc:Location.none "No entry point is defined" ;
  check_contract_names entry_modules;
  entry_modules, defs

let link_and_save opts outputdir defs entry_module  =
  Format.eprintf "Compiling %s@." entry_module.Contract.name;
  let m = link_contract opts outputdir defs entry_module in
  let outputprefix = Filename.concat outputdir entry_module.name in
  let m = optimize_michelson opts outputprefix m in
  Format.eprintf "Linked to %s@." (outputprefix ^ ".tz");
  Michelson.Module.save (outputprefix ^ ".tz") m;
  Compile.compiled_modules :=
    (entry_module.global_path, m) :: !Compile.compiled_modules

(* Link modules then compile all the Michelson modules *)
let link outputdir modules =
  (* XXX merge all the options into one? *)
  let opts = List.map (fun x -> x.Module.conf) modules in
  let entry_modules, defs = get_entry_modules_and_defs opts modules in
  List.iter (link_and_save opts outputdir defs) entry_modules

(* Link modules then compile the specified Michelson module *)
let link_specified_module outputdir modules name =
  (* XXX merge all the options into one? *)
  let opts = List.map (fun x -> x.Module.conf) modules in
  let entry_modules, defs = get_entry_modules_and_defs opts modules in
  match
    List.find_opt
      (fun entry_module ->
         entry_module.Contract.name = name)
      entry_modules
  with
  | None ->
      Error.raisef
        Link
        ~loc:Location.none
        "No such contract name found: %s"
        name
  | Some entry_module ->
      link_and_save opts outputdir defs entry_module
