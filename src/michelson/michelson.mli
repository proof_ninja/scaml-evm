(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Spotlib.Spot

type Tezos_error_monad.Error_monad.error +=
    Parser_error of Tezos_micheline__Micheline_parser.location * string

module Type : sig
  type t =
    { desc    : desc
    ; tyannot : string option  (** Michelson type annotation :s *)
    ; linear  : bool  (** linearity for ticket *)
    }

  and desc =
    | TyString
    | TyNat
    | TyInt
    | TyBytes
    | TyBool
    | TyUnit
    | TyList of t
    | TyPair of string option * t * string option * t
    | TyPairN of (string option * t) list (* length > 2 *)
    | TyOption of string option * t
    | TyOr of string option * t * string option * t
    | TySet of t
    | TyMap of t * t
    | TyBigMap of t * t
    | TyMutez
    | TyKeyHash
    | TyTimestamp
    | TyAddress
    | TyChainID
    | TyKey
    | TySignature
    | TyOperation
    | TyContract of t
    | TyLambda of t * t
    (* from 008 *)
    | TyNever
    | TyBLS12_381_Fr
    | TyBLS12_381_G1
    | TyBLS12_381_G2
    | TySapling_state of int
    | TySapling_transaction of int
    | TyTicket of t

  (** Make a non linear type *)
  val mk : desc -> t

  val is_linear : t -> bool

  val tyString : t
  val tyNat : t
  val tyInt : t
  val tyBytes : t
  val tyBool : t
  val tyUnit : t
  val tyList : t -> t
  val tyPair : string option * t * string option * t -> t
  val tyPairN : (string option * t) list -> t
  val tyOption : string option * t -> t
  val tyOr : string option * t * string option * t -> t
  val tySet : t -> t
  val tyMap : t * t -> t
  val tyBigMap : t * t -> t
  val tyMutez : t
  val tyKeyHash : t
  val tyTimestamp : t
  val tyAddress : t
  val tyChainID : t
  val tyKey : t
  val tySignature : t
  val tyOperation : t
  val tyContract : t -> t
  val tyLambda : t * t -> t

  (* from 008 *)
  val tyNever : t
  val tyBLS12_381_Fr : t
  val tyBLS12_381_G1 : t
  val tyBLS12_381_G2 : t
  val tySapling_state : int -> t
  val tySapling_transaction : int -> t
  val tyTicket : t -> t

  val validate : int * int (* protocol version *) -> t -> (unit, t * string) Result.t

  val is_comparable : int * int (* protocol version *) -> t -> bool
  val is_parameterable : t -> bool (* passable *)

  val is_storable : t -> bool
  val is_pushable : t -> bool
  val is_packable : t -> bool
  val is_big_mappable : t -> bool
  val is_dupable : t -> bool

  (** Split all the function argument types from the return type *)
  val function_args : t -> t list * t

  val annotate : (string option -> string option) -> t -> t

  (** Drop all annotations *)
  val drop_annots : t -> t

  include Micheline.S with type t := t
end

(** 9223372036854775807 *)
val maximum_mutez : Z.t

module rec Constant : sig
  type t =
    | Unit
    | Bool of bool
    | Int of Z.t
    | String of string
    | Bytes of string
    | Option of t option
    | List of t list
    | Set of t list
    | Map of (t * t) list
    | Pair of t * t
    | PairN of t list
    | Left of t
    | Right of t
    | Timestamp of Z.t
    | Code of Opcode.t list

  (** Print in Micheline *)
  val pp : Format.formatter -> t -> unit

  val to_micheline : t -> Micheline.t

  (* need the type to parse *)
  val of_micheline : Type.t -> Micheline.Parse.parsed -> (t, Micheline.Parse.location * string) result

  val parse_string : check_indentation:bool -> Type.t -> string -> (t, Tzerror.t) result

  val parse_file : check_indentation:bool -> Type.t -> string -> (t, Tzerror.t) result

  (** Big_map IDs contained in the given constant, with its key value types *)
  val big_map_ids : Type.t -> t -> (Z.t * (Type.t * Type.t)) list
end

and Opcode : sig
  type t = { desc : desc; comments : string list }

  and desc =
    | BLOCK of t list
    | DUP
    | DIP of t list
    | DIPn of int * t list
    | DIG of int
    | DUG of int
    | DROP
    | DROPn of int
    | SWAP
    | PAIR of string list
    | CAR
    | CDR
    | LEFT of Type.t
    | RIGHT of Type.t
    | LAMBDA of Type.t * Type.t * t list
    | APPLY
    | PUSH of Type.t * Constant.t
    | NIL of Type.t
    | CONS
    | NONE of Type.t
    | SOME
    | COMPARE
    | EQ
    | LT
    | LE
    | GT
    | GE
    | NEQ
    | IF of t list * t list
    | ADD
    | SUB
    | MUL
    | EDIV
    | ABS
    | ISNAT
    | NEG
    | LSL
    | LSR
    | AND
    | OR
    | XOR
    | NOT
    | EXEC
    | IF_NONE of t list * t list
    | IF_LEFT of t list * t list
    | IF_CONS of t list * t list
    | FAILWITH
    | UNIT
    | EMPTY_SET of Type.t
    | EMPTY_MAP of Type.t * Type.t
    | EMPTY_BIG_MAP of Type.t * Type.t
    | SIZE
    | MEM
    | UPDATE
    | ITER of t list
    | MAP of t list
    | LOOP of t list (* It is not really useful for SCaml *)
    | LOOP_LEFT of t list
    | CONCAT
    | SELF of string option
    | GET
    | RENAME of string option
    | PACK
    | UNPACK of Type.t
    | SLICE
    | CAST of Type.t
    | CONTRACT of Type.t
    | CONTRACT' of Type.t * string (* entry name without '%' *)
    | TRANSFER_TOKENS
    | SET_DELEGATE
    | CREATE_ACCOUNT
    | CREATE_CONTRACT of Module.t
    | IMPLICIT_ACCOUNT
    | NOW
    | AMOUNT
    | BALANCE
    | CHECK_SIGNATURE
    | BLAKE2B
    | SHA256
    | SHA512
    | HASH_KEY
    | STEPS_TO_QUOTA
    | SOURCE
    | SENDER
    | ADDRESS
    | CHAIN_ID
    | INT
    (* from 008 *)
    | LEVEL
    | SELF_ADDRESS
    | UNPAIR
    | PAIRING_CHECK
    | NEVER
    | KECCAK
    | SHA3
    | TICKET
    | READ_TICKET
    | SPLIT_TICKET
    | JOIN_TICKETS
    | SAPLING_EMPTY_STATE of int
    | SAPLING_VERIFY_UPDATE
    | VOTING_POWER
    | TOTAL_VOTING_POWER
    | GET_AND_UPDATE
    (* operators for right combs *)
    | GETn of int
    | UPDATEn of int
    | PAIRn of int (* PAIRn 2 and PAIR are different but with the same semantics *)
    | UNPAIRn of int (* UNPAIRn 2 and UNPAIR are different but with the same semantics *)
    | DUPn of int
    (* 011 *)
    | VIEW of string * Type.t
    (* 012 *)
    | SUB_MUTEZ

  val _BLOCK : t list -> t
  val _DUP : t
  val _DIP : t list -> t
  val _DIPn : int * t list -> t
  val _DIG : int -> t
  val _DUG : int -> t
  val _DROP : t
  val _DROPn : int -> t
  val _SWAP : t
  val _PAIR : string list -> t
  val _CAR : t
  val _CDR : t
  val _LEFT : Type.t -> t
  val _RIGHT : Type.t -> t
  val _LAMBDA : Type.t * Type.t * t list -> t
  val _APPLY : t
  val _PUSH : Type.t * Constant.t -> t
  val _NIL : Type.t -> t
  val _CONS : t
  val _NONE : Type.t -> t
  val _SOME : t
  val _COMPARE : t
  val _EQ : t
  val _LT : t
  val _LE : t
  val _GT : t
  val _GE : t
  val _NEQ : t
  val _IF : t list * t list -> t
  val _ADD : t
  val _SUB : t
  val _MUL : t
  val _EDIV : t
  val _ABS : t
  val _ISNAT : t
  val _NEG : t
  val _LSL : t
  val _LSR : t
  val _AND : t
  val _OR : t
  val _XOR : t
  val _NOT : t
  val _EXEC : t
  val _IF_NONE : t list * t list -> t
  val _IF_LEFT : t list * t list -> t
  val _IF_CONS : t list * t list -> t
  val _FAILWITH : t
  val _UNIT : t
  val _EMPTY_SET : Type.t -> t
  val _EMPTY_MAP : Type.t * Type.t -> t
  val _EMPTY_BIG_MAP : Type.t * Type.t -> t
  val _SIZE : t
  val _MEM : t
  val _UPDATE : t
  val _ITER : t list -> t
  val _MAP : t list -> t
  val _LOOP : t list -> t
  val _LOOP_LEFT : t list -> t
  val _CONCAT : t
  val _SELF : string option -> t
  val _GET : t
  val _RENAME : string option -> t
  val _PACK : t
  val _UNPACK : Type.t -> t
  val _SLICE : t
  val _CAST : Type.t -> t
  val _CONTRACT : Type.t -> t
  val _CONTRACT' : Type.t * string -> t
  val _TRANSFER_TOKENS : t
  val _SET_DELEGATE : t
  val _CREATE_ACCOUNT : t
  val _CREATE_CONTRACT : Module.t -> t
  val _IMPLICIT_ACCOUNT : t
  val _NOW : t
  val _AMOUNT : t
  val _BALANCE : t
  val _CHECK_SIGNATURE : t
  val _BLAKE2B : t
  val _SHA256 : t
  val _SHA512 : t
  val _HASH_KEY : t
  val _STEPS_TO_QUOTA : t
  val _SOURCE : t
  val _SENDER : t
  val _ADDRESS : t
  val _CHAIN_ID : t
  val _INT : t
  val _LEVEL : t
  val _SELF_ADDRESS : t
  val _UNPAIR : t
  val _PAIRING_CHECK : t
  val _NEVER : t
  val _KECCAK : t
  val _SHA3 : t
  val _TICKET : t
  val _READ_TICKET : t
  val _SPLIT_TICKET : t
  val _JOIN_TICKETS : t
  val _SAPLING_EMPTY_STATE : int -> t
  val _SAPLING_VERIFY_UPDATE : t
  val _VOTING_POWER : t
  val _TOTAL_VOTING_POWER : t
  val _GET_AND_UPDATE : t
  val _GETn : int -> t
  val _UPDATEn : int -> t
  val _PAIRn : int -> t
  val _UNPAIRn : int -> t
  val _DUPn : int -> t
  val _VIEW : string * Type.t -> t

  (* This is a macro *)
  val _ASSERT : t

  (** Override the comments of the opcode *)
  val set_comments : t -> string list -> t

  (** Prepend to the comments of the opcode *)
  val prepend_comments : string list -> t -> t

  (** Append to the comments of the opcode *)
  val append_comments : t -> string list -> t

  (** Append to the comments of the last opcode *)
  val append_comments_at_last : t list -> string list -> t list

  val clean_failwith : t list -> t list * bool (* ends with FAILWITH or not *)
  val end_with_failwith : t list -> bool

  val is_pure_push : desc -> bool

  include Micheline.S with type t := t
end

and View : sig
  type t = {
    name : string;
    ty_param : Type.t;
    ty_return : Type.t;
    code : Opcode.t list;
  }

  include Micheline.S with type t := t
end

and Module : sig
  type t = {
    parameter : Type.t;
    storage : Type.t;
    code : Opcode.t list;
    views : View.t list;
  }

  include Micheline.S with type t := t

  val parse_string : check_indentation:bool -> string -> t Tezos_error_monad.Error_monad.tzresult
  val parse_file : check_indentation:bool -> string -> t Tezos_error_monad.Error_monad.tzresult

  val save : string -> t -> unit
end
