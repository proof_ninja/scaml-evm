(* REJECTED *)
open SCaml

let[@entry name = "A"] nat (_ : nat) () = [], ()
let[@entry name = "B"] bool (_ : bool) () = [], ()

type t = Z of unit | C of string

let[@entry name = "maybe_C"] maybe_C (_ : t) () = [], ()
let[@entry name = "maybe_C"] maybe_X (_ : t) () = [], ()
