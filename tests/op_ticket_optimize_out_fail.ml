(* REJECT *)
(*
STORAGE= ([] : unit ticket list)
*)

open SCaml

let[@entry] main () (_ : unit ticket list) =
  let t = Ticket.create () (Nat 10) in
  (* Though [t'] is optimized out, it is reported as an error,
     since the linearity check now runs before the toplevel declaration
     wise optimization. *)
  let _, t' = Ticket.read t in
  [], [t]
