open SCaml_tools

open SCaml_michelson

let () = Debug.set_by_env ()

let do_dijkstra = ref false
let search_space_multiplier = ref 1.0
let use_lcs_for_astar_score = ref false
let files = ref []

let () =
  Arg.parse
    [ "--do-dijkstra",
      Arg.Bool (fun b -> do_dijkstra := b),
      "<bool> : Turn on/off Dijkstra search to verify A* correctness.  Default is false";

      "--search-space-multiplier",
      Arg.Float (fun f -> search_space_multiplier := f),
      "<float> : Tweak optimization search space limit.  Default is 1.0";

      "--use-lcs-for-astar-score",
      Arg.Bool (fun b -> use_lcs_for_astar_score := b),
      "<bool> : Turn on/off LCS scoring in A* search.  Default is true"; ]
    (fun s -> files := s :: !files)
    "optz <options> file.tz"

module Optimize =
  SCaml_michelson.Optimize.Make(struct
    let logger = prerr_endline
    let do_dijkstra = !do_dijkstra
    let search_space_multiplier = !search_space_multiplier
    let use_lcs_for_astar_score = !use_lcs_for_astar_score
  end)

let () =
  match !files with
  | [] ->
      prerr_endline "Error: need to specify 1 Michelson file";
      exit 1
  | _::_::_ ->
      prerr_endline "Error: only 1 Michelson file at a time";
      exit 1
  | [file] ->
      match Michelson.Module.parse_file ~check_indentation:false file with
      | Error es ->
          Format.eprintf "%a@." Tzerror.pp es;
          exit 1
      | Ok m ->
          let msz = Size.module_ m in
          let m' = Optimize.module_ m in
          let msz' = Size.module_ m' in
          Format.printf "# optimized: bytes: %d -> %d@." msz msz';
          Format.printf "%a@." Michelson.Module.pp m'
