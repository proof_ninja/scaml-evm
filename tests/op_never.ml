(* INPUT= (Left (Int 3) : (int, never) sum)
*)
open SCaml

let[@entry] main (x : (int, never) sum) () =
  match x with Left i -> [], () | Right nv -> never nv
