open SCaml

let[@entry] main () () =
  assert (int_of_mutez (Tz 0.000001) = Int 1) ;
  [], ()
