(*
INPUT=Int 3
*)

open SCaml

let double_loop n =
  let int2char (i : int) =
    if i < Int 0 then "."
    else
      let i = nat_of_int i in
      match String.slice i (Nat 1) "0123456789" with None -> "." | Some c -> c
  in
  let rec aux1 acc i =
    let rec aux2 acc j =
      if j = Int 0 then acc
      else
        let c = int2char (i * j) in
        aux2 (c ^ acc) (j - Int 1)
    in
    if i = Int 0 then acc else aux1 (aux2 "" n ^ "\n" ^ acc) (i - Int 1)
  in
  aux1 "" n

let[@entry] main three _ =
  let v = double_loop three in
  assert (v = "123\n246\n369\n") ;

  [], ()
