(*
TZ=lang_module_multi.M.tz lang_module_multi.N.tz

test.sh does not support different storages and inputs for the contracts
*)
open SCaml

module M = struct
  let[@entry] main () () = [], ()
end

module N = struct
  let[@entry] main2 () () = [], ()
end
