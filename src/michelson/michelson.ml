(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open SCaml_tools

type Tezos_error_monad.Error_monad.error +=
    Parser_error of Tezos_micheline__Micheline_parser.location * string

let () =
  Tezos_error_monad.Error_monad.register_error_kind
    `Permanent
    ~id:"michelson.parser_error"
    ~title:"Michelson parse failure"
    ~description:"Michelson parse failure"
    ~pp:(fun ppf (loc, s) ->
      Format.fprintf
        ppf
        "%a: %s"
        Tezos_micheline__Micheline_parser.print_location loc
        s)
    Data_encoding.(obj2
                     (req "location" Tezos_micheline__Micheline_parser.location_encoding)
                     (req "message" string))
    (function
      | Parser_error (loc, s) -> Some (loc, s)
      | _ -> None)
    (fun (loc, s) -> Parser_error (loc, s))

module Type = struct
  (* Michelson type.  This is shamelessly used as types for IML, too. *)
  type t = {desc : desc; tyannot : string option; linear : bool}

  and desc =
    | TyString
    | TyNat
    | TyInt
    | TyBytes
    | TyBool
    | TyUnit
    | TyList of t
    (* XXX fields and annots must be moved to t *)
    | TyPair of string option * t * string option * t
    | TyPairN of (string option * t) list (* length > 2 *) (* XXX We now need pair compaction somewhere else *)
    | TyOption of string option * t
    | TyOr of string option * t * string option * t
    | TySet of t (* comparable *)
    | TyMap of t (* comparable *) * t
    | TyBigMap of t (* comparable *) * t
    | TyMutez
    | TyKeyHash
    | TyTimestamp
    | TyAddress
    | TyChainID
    | TyKey
    | TySignature
    | TyOperation
    | TyContract of t
    | TyLambda of t * t
    | TyNever (* from 008 *)
    | TyBLS12_381_Fr (* from 008 *)
    | TyBLS12_381_G1 (* from 008 *)
    | TyBLS12_381_G2 (* from 008 *)
    | TySapling_state of int (* from 008 *)
    | TySapling_transaction of int (* from 008 *)
    | TyTicket of t
  (* from 008 *)

  let annotate f t = {t with tyannot = f t.tyannot}

  let mk desc = {desc; tyannot = None; linear = false}
  let mk' ?(linear = false) desc = {desc; tyannot = None; linear}
  let is_linear ty = ty.linear
  let lin1 t desc = mk' ~linear:(is_linear t) desc
  let lin2 t1 t2 desc = mk' ~linear:(is_linear t1 || is_linear t2) desc

  let tyString = mk TyString
  let tyNat = mk TyNat
  let tyInt = mk TyInt
  let tyBytes = mk TyBytes
  let tyBool = mk TyBool
  let tyUnit = mk TyUnit

  let tyList t = lin1 t & TyList t
  let tyPair (f1, t1, f2, t2) = lin2 t1 t2 & TyPair (f1, t1, f2, t2)
  let tyPairN fts =
    assert (List.length fts > 2);
    let linear = List.exists (fun (_,ty) -> is_linear ty) fts in
    mk' ~linear & TyPairN fts
  let tyOption (f, t) = lin1 t & TyOption (f, t)
  let tyOr (f1, t1, f2, t2) = lin2 t1 t2 & TyOr (f1, t1, f2, t2)
  let tySet t = lin1 t & TySet t
  let tyMap (t1, t2) = lin2 t1 t2 & TyMap (t1, t2)
  let tyBigMap (t1, t2) = lin2 t1 t2 & TyBigMap (t1, t2)
  let tyMutez = mk TyMutez
  let tyKeyHash = mk TyKeyHash
  let tyTimestamp = mk TyTimestamp
  let tyAddress = mk TyAddress
  let tyChainID = mk TyChainID
  let tyKey = mk TyKey
  let tySignature = mk TySignature
  let tyOperation = mk TyOperation
  let tyContract t = mk & TyContract t
  let tyLambda (t1, t2) = mk & TyLambda (t1, t2)
  let tyNever = mk & TyNever
  let tyBLS12_381_Fr = mk & TyBLS12_381_Fr
  let tyBLS12_381_G1 = mk & TyBLS12_381_G1
  let tyBLS12_381_G2 = mk & TyBLS12_381_G2
  let tySapling_state n = mk & TySapling_state n
  let tySapling_transaction n = mk & TySapling_transaction n
  let tyTicket t = mk' ~linear:true & TyTicket t

  let rec function_args t =
    match t.desc with
    | TyLambda (t1, t2) ->
        let ts, t = function_args t2 in
        t1 :: ts, t
    | _ -> [], t

  let rec to_micheline t =
    let prim n args =
      let attrs = match t.tyannot with Some s -> [":" ^ s] | None -> [] in
      Micheline.prim n args attrs
    in
    let add_field f t =
      match f, t with
      | None, _ -> t
      | Some f, Tezos_micheline.Micheline.Prim (c, s, ts, annots) ->
          Tezos_micheline.Micheline.Prim (c, s, ts, ("%" ^ f) :: annots)
      | _ -> t
    in
    let ( ! ) x = prim x [] in
    match t.desc with
    | TyString -> !"string"
    | TyNat -> !"nat"
    | TyInt -> !"int"
    | TyBytes -> !"bytes"
    | TyBool -> !"bool"
    | TyUnit -> !"unit"
    | TyList t -> prim "list" [to_micheline t]
    | TyPair (f1, t1, f2, t2) ->
(*
        let rec shrink f t = match f, t with
          | None, { desc= TyPair (f1, t1, f2, t2) } ->
              (* XXX tyannot is lost *)
              let fts = shrink f2 t2 in
              (f1, t1) :: fts
          | _ -> [f, t]
        in
        let fts = (f1,t1) :: shrink f2 t2 in
*)
        prim "pair" (List.map (fun (f,t) -> add_field f & to_micheline t) [(f1,t1);(f2,t2)])
    | TyPairN fts ->
        prim "pair" (List.map (fun (f,t) -> add_field f & to_micheline t) fts)
    | TyOption (f, t) -> prim "option" [add_field f & to_micheline t]
    | TyOr (f1, t1, f2, t2) ->
        prim
          "or"
          [add_field f1 & to_micheline t1; add_field f2 & to_micheline t2]
    | TySet t -> prim "set" [to_micheline t]
    | TyMap (t1, t2) -> prim "map" [to_micheline t1; to_micheline t2]
    | TyBigMap (t1, t2) -> prim "big_map" [to_micheline t1; to_micheline t2]
    | TyMutez -> !"mutez"
    | TyKeyHash -> !"key_hash"
    | TyTimestamp -> !"timestamp"
    | TyAddress -> !"address"
    | TyChainID -> !"chain_id"
    | TyKey -> !"key"
    | TySignature -> !"signature"
    | TyOperation -> !"operation"
    | TyContract t -> prim "contract" [to_micheline t]
    | TyLambda (t1, t2) -> prim "lambda" [to_micheline t1; to_micheline t2]
    | TyNever -> !"never"
    | TyBLS12_381_Fr -> !"bls12_381_fr"
    | TyBLS12_381_G1 -> !"bls12_381_g1"
    | TyBLS12_381_G2 -> !"bls12_381_g2"
    | TySapling_state n ->
        prim "sapling_state" [Int ({comment = None}, Z.of_int n)]
    | TySapling_transaction n ->
        prim "sapling_transaction" [Int ({comment = None}, Z.of_int n)]
    | TyTicket t -> prim "ticket" [to_micheline t]

  let pp ppf x = Micheline.pp ppf & to_micheline x

  let rec of_micheline ml =
    let open Result.Syntax in
    match ml with
    | Tezos_micheline.Micheline.Int (l, _z) -> Error (l, "unexpected int for type")
    | String (l, _) -> Error (l, "unexpected string for type")
    | Bytes (l, _) -> Error (l, "unexpected bytes for type")
    | Seq (l, _) -> Error (l, "unexpected {..} for type")
    | Prim (l, n, nodes, annot) ->
        let parse_annot s =
          match s with
          | "" -> Error (l, "invalid annotation")
          | _ ->
              begin match String.split_at 1 s with
                | ":", s -> Ok (`Type s)
                | "%", s -> Ok (`Field s)
                | _ -> Error (l, "invalid annotation")
              end
        in
        let* annots = Result.mapM parse_annot annot in
        let* tyannot =
          match List.filter_map (function `Type s -> Some s | _ -> None) annots with
          | [] -> Ok None
          | [s] -> Ok (Some s)
          | _ -> Error (l, "at most 1 type annotation is allowed")
        in
        let* field =
          match List.filter_map (function `Field s -> Some s | _ -> None) annots with
          | [] -> Ok None
          | [s] -> Ok (Some s)
          | _ -> Error (l, "at most 1 field anntoation is allowed")
        in
        let field_not_allowed = Error (l, "field annotation is not allowed here") in
        let no_field f x =
          match f with
          | None -> Ok x
          | Some _ -> field_not_allowed
        in
        let no_fields f1 f2 x =
          match f1, f2 with
          | None, None -> Ok x
          | _ -> field_not_allowed
        in
        let+ ty =
          match n, nodes with
          | "string", [] -> Ok tyString
          | "nat", [] -> Ok tyNat
          | "int", [] -> Ok tyInt
          | "bytes", [] -> Ok tyBytes
          | "bool", [] -> Ok tyBool
          | "unit", [] -> Ok tyUnit
          | "list", [t] ->
              let* t, f = of_micheline t in
              no_field f & tyList t
          | "pair", [t1; t2] ->
              let* t1, f1 = of_micheline t1 in
              let+ t2, f2 = of_micheline t2 in
              tyPair (f1, t1, f2, t2)
          | "pair", (_::_::_::_ as ts) ->
              let+ tfs = Result.mapM of_micheline ts in
              tyPairN (List.map (fun (x,y) -> (y,x)) tfs)
          | "option", [t] ->
              let+ t, f = of_micheline t in
              tyOption (f, t)
          | "or", [t1; t2] ->
              let* t1, f1 = of_micheline t1 in
              let+ t2, f2 = of_micheline t2 in
              tyOr (f1, t1, f2, t2)
          | "set", [t] ->
              let* t, f = of_micheline t in
              no_field f & tySet t
          | "map", [t1; t2] ->
              let* t1, f1 = of_micheline t1 in
              let* t2, f2 = of_micheline t2 in
              no_fields f1 f2 & tyMap (t1, t2)
          | "big_map", [t1; t2] ->
              let* t1, f1 = of_micheline t1 in
              let* t2, f2 = of_micheline t2 in
              no_fields f1 f2 & tyBigMap (t1, t2)
          | "mutez", [] -> Ok tyMutez
          | "key_hash", [] -> Ok tyKeyHash
          | "timestamp", [] -> Ok tyTimestamp
          | "address", [] -> Ok tyAddress
          | "chain_id", [] -> Ok tyChainID
          | "key", [] -> Ok tyKey
          | "signature", [] -> Ok tySignature
          | "operation", [] -> Ok tyOperation
          | "never", [] -> Ok tyNever
          | "bls12_381_fr", [] -> Ok tyBLS12_381_Fr
          | "bls12_381_g1", [] -> Ok tyBLS12_381_G1
          | "bls12_381_g2", [] -> Ok tyBLS12_381_G2
          | "contract", [t] ->
              let* t, f = of_micheline t in
              no_field f & tyContract t
          | "lambda", [t1; t2] ->
              let* t1, f1 = of_micheline t1 in
              let* t2, f2 = of_micheline t2 in
              no_fields f1 f2 & tyLambda (t1, t2)
          | "sapling_state", [Int (_, z)] ->
              Ok (tySapling_state & Z.to_int z)
          | "sapling_transaction", [Int (_, z)] ->
              Ok (tySapling_transaction & Z.to_int z)
          | "ticket", [t] ->
              let* t, f = of_micheline t in
              no_field f & tyTicket t
          | _ -> Error (l, "unknown type or invalid arity: " ^ n)
        in
        { ty with tyannot }, field

  let of_micheline ml =
    let open Result.Syntax in
    let* (ty, field) = of_micheline ml in
    match field with
    | None -> Ok ty
    | Some _ ->
        let _l = Tezos_micheline.Micheline.location ml in
        (* Error (l, "field annotation is now allowed here"); *) (* XXX warning *)
        (* parameter (unit %default) ; *)
        Ok ty

  let rec validate protocol_version ty =
    let open Result.Infix in
    let rec f ty =
      match ty.desc with
      | TyBigMap (k, v) ->
          f k >>= fun () ->
          f v >>= fun () ->
          if not (is_comparable protocol_version k) then
            Error (ty, "big_map's key type must be comparable")
          else if not (is_big_mappable v) then
            Error (ty, "not allowed for big_map value")
          else Ok ()
      | TySet e ->
          f e >>= fun () ->
          if not (is_comparable protocol_version e) then
            Error (ty, "set's element type must be comparable")
          else Ok ()
      | TyMap (k, v) ->
          f k >>= fun () ->
          f v >>= fun () ->
          if not (is_comparable protocol_version k) then
            Error (ty, "map's key type must be comparable")
          else Ok ()
      | TyContract p ->
          f p >>= fun () ->
          if not (is_parameterable p) then
            Error (ty, "contract's parameter type must be passable")
          else Ok ()
      | TyList ty | TyOption (_, ty) -> f ty
      | TyPair (_, ty1, _, ty2) | TyOr (_, ty1, _, ty2) | TyLambda (ty1, ty2) ->
          f ty1 >>= fun () -> f ty2
      | TyPairN fts -> Result.mapM_ (fun (_,ty) -> f ty) fts
      | TyString | TyNat | TyInt | TyBytes | TyBool | TyUnit | TyMutez
      | TyKeyHash | TyTimestamp | TyAddress | TyChainID | TyKey | TySignature
      | TyOperation ->
          Ok ()
      | TyNever -> Ok ()
      | TyBLS12_381_Fr | TyBLS12_381_G1 | TyBLS12_381_G2 -> Ok ()
      | TySapling_state _ | TySapling_transaction _ -> Ok ()
      | TyTicket t ->
          if not (is_comparable protocol_version t) then
            Error (ty, "ticket information type must be comparable")
          else Ok ()
    in
    f ty

  and is_comparable protocol_version ty =
    (* See Script_ir_translator.parse_comparable_ty *)
    let rec f ty =
      match ty.desc with
      | (TyChainID | TySignature | TyKey | TyKeyHash | TyUnit)
        when protocol_version >= (8, 0) ->
          true (* since 008 *)
      | TyOption (_, ty) when protocol_version >= (8, 0) -> f ty
      | TyOr (_, ty1, _, ty2) when protocol_version >= (8, 0) -> f ty1 && f ty2
      | TyChainID | TySignature | TyKey -> false
      | TyString | TyNat | TyInt | TyBytes | TyBool | TyMutez | TyKeyHash
      | TyTimestamp | TyAddress | TyNever ->
          true
      | TyPair (_, ty1, _, ty2) -> f ty1 && f ty2 (* since 005_Babylon *)
      | TyPairN fts -> List.for_all (fun (_,ty) -> f ty) fts (* since 005_Babylon *)
      | TyBigMap _ | TyContract _ | TyOption _ | TyLambda _ | TyList _ | TyMap _
      | TyOperation | TyOr _ | TySet _ | TyUnit | TyBLS12_381_Fr
      | TyBLS12_381_G1 | TyBLS12_381_G2 | TySapling_state _
      | TySapling_transaction _ | TyTicket _ ->
          false
    in
    f ty

  and is_parameterable ty =
    let rec f ty =
      match ty.desc with
      | TyOperation -> false
      | TyBigMap _ -> true
      | TyContract _ -> true
      | TyList t | TyOption (_, t) | TySet t -> f t
      | TyLambda (_t1, _t2) -> true
      | TyPair (_, t1, _, t2) | TyOr (_, t1, _, t2) | TyMap (t1, t2) ->
          f t1 && f t2
      | TyPairN fts -> List.for_all (fun (_,ty) -> f ty) fts
      | TyString | TyNat | TyInt | TyBytes | TyBool | TyUnit | TyMutez
      | TyKeyHash | TyTimestamp | TyAddress | TyChainID | TyKey | TySignature
      | TyNever | TyBLS12_381_Fr | TyBLS12_381_G1 | TyBLS12_381_G2
      | TySapling_state _ | TySapling_transaction _ | TyTicket _ ->
          true
    in
    f ty

  and is_storable ty =
    let rec f ty =
      match ty.desc with
      | TyOperation -> false
      | TyContract _ -> false
      | TyList t | TyOption (_, t) | TySet t -> f t
      | TyLambda (_t1, _t2) -> true
      | TyPair (_, t1, _, t2)
      | TyOr (_, t1, _, t2)
      | TyMap (t1, t2)
      | TyBigMap (t1, t2) ->
          f t1 && f t2
      | TyPairN fts -> List.for_all (fun (_,ty) -> f ty) fts
      | TyString | TyNat | TyInt | TyBytes | TyBool | TyUnit | TyMutez
      | TyKeyHash | TyTimestamp | TyAddress | TyChainID | TyKey | TySignature
      | TyNever | TyBLS12_381_Fr | TyBLS12_381_G1 | TyBLS12_381_G2
      | TySapling_state _ | TySapling_transaction _ | TyTicket _ ->
          true
    in
    f ty

  and is_pushable ty =
    let rec f ty =
      match ty.desc with
      | TyBigMap _ | TyContract _ | TyOperation | TySapling_state _ | TyTicket _
        ->
          false
      | TyList t | TyOption (_, t) | TySet t -> f t
      | TyLambda (_t1, _t2) -> true
      | TyPair (_, t1, _, t2) | TyOr (_, t1, _, t2) | TyMap (t1, t2) ->
          f t1 && f t2
      | TyPairN fts -> List.for_all (fun (_,ty) -> f ty) fts
      | TyString | TyNat | TyInt | TyBytes | TyBool | TyUnit | TyMutez
      | TyKeyHash | TyTimestamp | TyAddress | TyChainID | TyKey | TySignature
      | TyNever | TyBLS12_381_Fr | TyBLS12_381_G1 | TyBLS12_381_G2
      | TySapling_transaction _ ->
          true
    in
    f ty

  and is_packable ty =
    let rec f ty =
      match ty.desc with
      | TyBigMap _ | TyOperation | TySapling_state _ | TyTicket _ -> false
      | TyContract _ ->
          (* Long ago, contract was not packable except some special places *)
          true
      | TyLambda (_t1, _t2) -> true
      | TyList t | TyOption (_, t) | TySet t -> f t
      | TyPair (_, t1, _, t2) | TyOr (_, t1, _, t2) | TyMap (t1, t2) ->
          f t1 && f t2
      | TyPairN fts -> List.for_all (fun (_,ty) -> f ty) fts
      | TyString | TyNat | TyInt | TyBytes | TyBool | TyUnit | TyMutez
      | TyKeyHash | TyTimestamp | TyAddress | TyChainID | TyKey | TySignature
      | TyNever | TyBLS12_381_Fr | TyBLS12_381_G1 | TyBLS12_381_G2
      | TySapling_transaction _ ->
          true
    in
    f ty

  and is_big_mappable ty =
    let rec f ty =
      match ty.desc with
      | TyBigMap _ | TyOperation | TySapling_state _ -> false
      | TyContract _ -> true
      | TyLambda (_t1, _t2) -> true
      | TyList t | TyOption (_, t) | TySet t -> f t
      | TyPair (_, t1, _, t2) | TyOr (_, t1, _, t2) | TyMap (t1, t2) ->
          f t1 && f t2
      | TyPairN fts -> List.for_all (fun (_,ty) -> f ty) fts
      | TyString | TyNat | TyInt | TyBytes | TyBool | TyUnit | TyMutez
      | TyKeyHash | TyTimestamp | TyAddress | TyChainID | TyKey | TySignature
      | TyNever | TyBLS12_381_Fr | TyBLS12_381_G1 | TyBLS12_381_G2
      | TySapling_transaction _ | TyTicket _ ->
          true
    in
    f ty

  and is_dupable ty =
    let rec f ty =
      match ty.desc with
      | TyTicket _ -> false
      | TyList t | TyOption (_, t) | TySet t -> f t
      | TyPair (_, t1, _, t2) | TyOr (_, t1, _, t2) | TyMap (t1, t2) ->
          f t1 && f t2
      | TyPairN fts -> List.for_all (fun (_,ty) -> f ty) fts
      | TyContract _ | TyLambda _ | TyOperation | TyBigMap _ | TyString | TyNat
      | TyInt | TyBytes | TyBool | TyUnit | TyMutez | TyKeyHash | TyTimestamp
      | TyAddress | TyChainID | TyKey | TySignature | TyNever | TyBLS12_381_Fr
      | TyBLS12_381_G1 | TyBLS12_381_G2 | TySapling_state _
      | TySapling_transaction _ ->
          true
    in
    f ty

  let drop_annots t =
    let rec f t =
      let mk desc = {t with desc; tyannot = None} in
      match t.desc with
      | TyString | TyNat | TyInt | TyBytes | TyBool | TyUnit | TyMutez
      | TyKeyHash | TyTimestamp | TyAddress | TyChainID | TyKey | TySignature
      | TyOperation | TyNever | TyBLS12_381_Fr | TyBLS12_381_G1 | TyBLS12_381_G2
      | TySapling_state _ | TySapling_transaction _ ->
          mk t.desc
      | TyList t -> mk & TyList (f t)
      | TyPair (_, t1, _, t2) -> mk & TyPair (None, f t1, None, f t2)
      | TyPairN fts ->
          let fts = List.map (fun (_,ty) -> (None, f ty)) fts in
          mk & TyPairN fts
      | TyOption (_, t) -> mk & TyOption (None, f t)
      | TyOr (_, t1, _, t2) -> mk & TyOr (None, f t1, None, f t2)
      | TySet t -> mk & TySet (f t)
      | TyMap (t1, t2) -> mk & TyMap (f t1, f t2)
      | TyBigMap (t1, t2) -> mk & TyBigMap (f t1, f t2)
      | TyContract t -> mk & TyContract (f t)
      | TyLambda (t1, t2) -> mk & TyLambda (f t1, f t2)
      | TyTicket t -> mk & TyTicket (f t)
    in
    f t
end

(* we cannot put this in Constant, since Constant is recursively defined *)
let maximum_mutez = Z.of_string "9223372036854775807"

module rec Constant : sig
  type t =
    | Unit
    | Bool of bool
    | Int of Z.t
    | String of string
    | Bytes of string
    | Option of t option
    | List of t list
    | Set of t list
    | Map of (t * t) list
    | Pair of t * t
    | PairN of t list
    | Left of t
    | Right of t
    | Timestamp of Z.t
    | Code of Opcode.t list

  val pp : Format.formatter -> t -> unit
  val to_micheline : t -> Micheline.t
  val of_micheline : Type.t -> Micheline.parsed -> (t, Micheline.location * string) result
  val parse_string : check_indentation:bool -> Type.t -> string -> t Tezos_error_monad.Error_monad.tzresult
  val parse_file : check_indentation:bool -> Type.t -> string -> t Tezos_error_monad.Error_monad.tzresult
  val big_map_ids : Type.t -> t -> (Z.t * (Type.t * Type.t)) list
end = struct
  type t =
    | Unit
    | Bool of bool
    | Int of Z.t
    | String of string
    | Bytes of string (* "0xdeadbeef" *)
    | Option of t option
    | List of t list
    | Set of t list
    | Map of (t * t) list
    | Pair of t * t
    | PairN of t list
    | Left of t
    | Right of t
    | Timestamp of Z.t
    | Code of Opcode.t list

  let to_micheline =
    let open Micheline in
    let rec f = function
      | Bool true -> prim "True" [] []
      | Bool false -> prim "False" [] []
      | Unit -> prim "Unit" [] []
      | Int n -> int n
      | String s -> string s
      | Bytes s (* in hex *) -> bytes s
      | Option None -> prim "None" [] []
      | Option (Some t) -> prim "Some" [f t] []
      | Pair (t1, t2) ->
(* XXX need pair contraction
          let rec shrink = function
            | Pair (t1, t2) -> t1 :: shrink t2
            | v -> [v]
          in
          let ts = t1 :: shrink t2 in
*)
          prim "Pair" (List.map f [t1; t2]) []
      | PairN ts ->
          (* Tezos doc says:
             Right-comb values can also be written using sequences; Pair x{0} x{1} ... x{n-2} x{n-1} can be written {x{0}; x{1}; ...; x{n-2}; x{n-1}}.

             According to tezos-client normalize data, Pair 1 2 3 is the normalized form than { 1; 2; 3 }.
          *)
          prim "Pair" (List.map f ts) []
      | Left t -> prim "Left" [f t] []
      | Right t -> prim "Right" [f t] []
      | List ts -> seq (List.map f ts)
      | Set ts -> seq (List.map f & List.sort compare ts)
      | Map xs -> seq (List.map (fun (k, v) -> prim "Elt" [f k; f v] []) xs)
      | Timestamp z -> begin
          (* XXX move to Timestamp? *)
          match Ptime.of_float_s @@ Z.to_float z with
          | None -> assert false
          | Some t -> string (Ptime.to_rfc3339 ~space:false ~frac_s:0 t)
        end
      | Code os -> seq (List.map Opcode.to_micheline os)
    in
    f

  module TM = Tezos_micheline.Micheline

  (* The type is only used for distinguish list, set, map, big_map,
     string and timestamp
  *)
  let rec of_micheline ty (ml : Micheline.parsed) =
    let open Result.Syntax in
    match ml, ty with
    | _, {Type.desc= Type.TyTicket ty} ->
        of_micheline Type.(tyPairN [None, tyAddress; None, ty; None, tyNat]) ml
    | TM.Int (_l, z), _ -> Ok (Int z)
    | String (_l, s), {Type.desc= TyTimestamp} ->
        begin match Timestamp.parse s with
          | Ok v -> Ok (Timestamp v)
          | Error e ->
              Format.eprintf "WARNING: %s@." e;
              (* Error (l,e)*)
              Ok (Timestamp Z.zero)
        end
    | String (_l, s), _ -> Ok (String s)
    | Bytes (_l, bs), _ ->
        let `Hex s = Hex.of_bytes bs in
        Ok (Bytes ("0x" ^ s))
    | Prim (l, n, nodes, annot), _ ->
        begin match n, nodes, ty with
          | _ when annot <> [] -> Error (l, "invalid annotation")
          | "True", [], _ -> Ok (Bool true)
          | "False", [], _ -> Ok (Bool false)
          | "Unit", [], _ -> Ok Unit
          | "None", [], _ -> Ok (Option None)
          | "Some", [t], {desc= Type.TyOption (_,ty)}  ->
              let+ t = of_micheline ty t in
              Option (Some t)
          | "Pair", [t1; t2], {desc= Type.TyPair (_,ty1,_,ty2)} ->
              let* t1 = of_micheline ty1 t1 in
              let+ t2 = of_micheline ty2 t2 in
              Pair (t1, t2)
          | "Pair", (_::_::_ as ts), {desc= (Type.TyPair _ | Type.TyPairN _)} ->
              let rec f ts ty =
                let open Type in
                match ts, ty with
                | [t1; t2], {desc= TyPair (_f1,ty1,_f2,ty2)} ->
                    Ok [(t1,ty1); (t2,ty2)]
                | [t1; t2], {desc= TyPairN ((_f1,ty1)::fts)} ->
                    let+ ty2 = match fts with
                      | [] | [_] -> Error (l,"Pair: type does not contain enough tuple elements")
                      | [(f2,ty2); (f3,ty3)] -> Ok (tyPair (f2,ty2,f3,ty3))
                      | fts -> Ok (tyPairN fts)
                    in
                    [(t1,ty1); (t2,ty2)]
                | t1 :: ts, {desc= TyPair (_f1,ty1,_f2,ty2)} ->
                    let+ tts = f ts ty2 in
                    (t1,ty1) :: tts
                | t1 :: ts, {desc= TyPairN ((_f1,ty1)::fts)} ->
                    let* ty2 = match fts with
                      | [] | [_] -> Error (l,"Pair: type does not contain enough tuple elements")
                      | [(f2,ty2); (f3,ty3)] -> Ok (tyPair (f2,ty2,f3,ty3))
                      | fts -> Ok (tyPairN fts)
                    in
                    let+ tts = f ts ty2 in
                    (t1,ty1) :: tts
                | _ -> assert false
              in
              let* tts = f ts ty in
              let+ tts = Result.mapM (fun (t,ty) -> of_micheline ty t) tts in
              begin match tts with
                | [] | [_] -> assert false
                | [t1; t2] -> Pair (t1,t2)
                | _ -> PairN tts
              end
          | "Pair", _, _ ->
              Error (l,
                     Format.asprintf
                       "Pair of non pair type: %a : %a"
                       Micheline.pp (Micheline.Parse.of_parsed ml)
                       Type.pp ty
                    )
          | "Left", [t], {desc= Type.TyOr (_, ty, _, _)} ->
              let+ t = of_micheline ty t in
              Left t
          | "Right", [t], {desc= Type.TyOr (_, _, _, ty)} ->
              let+ t = of_micheline ty t in
              Right t
          | _ -> Error (l, "unknown constructor")
        end
    | Seq (_l, nodes), {desc= TyMap (tk,tv)} ->
        let f = function
          | TM.Prim (_, "Elt", [k; v], []) ->
              let* k = of_micheline tk k in
              let+ v = of_micheline tv v in
              (k,v)
          | t -> Error (TM.location t, "not elt")
        in
        Result.(fmap (fun x -> Map x) & Result.mapM f nodes)
    | Seq (_l, nodes), {desc= TyBigMap (tk,tv)} ->
        let f = function
          | TM.Prim (_, "Elt", [k; v], []) ->
              let* k = of_micheline tk k in
              let+ v = of_micheline tv v in
              (k,v)
          | t -> Error (TM.location t, "not elt")
        in
        (* Map is used for BigMap constants *)
        Result.(fmap (fun x -> Map x) & mapM f nodes)
    | Seq (_l, nodes), {desc= TyList ty} ->
        Result.(fmap (fun x -> List x) & mapM (of_micheline ty) nodes)
    | Seq (_l, nodes), {desc= TySet ty} ->
        Result.(fmap (fun x -> Set x) & mapM (of_micheline ty) nodes)
    | Seq (_l, nodes), {desc= TyLambda _} ->
        Result.(fmap (fun x -> Code x) & mapM Opcode.of_micheline nodes)

    (* Very annoying, but I found some big_maps have non-normalized pairs
       { 1; 2; 3 }
    *)
    | Seq (_l, [t1; t2]), { desc= Type.TyPair (_,ty1,_,ty2) } ->
        (* XXX duped *)
        let* t1 = of_micheline ty1 t1 in
        let+ t2 = of_micheline ty2 t2 in
        Pair (t1, t2)

    | Seq (l, (_::_::_ as ts)), { desc= (Type.TyPair _ | Type.TyPairN _)} ->
        (* XXX duped *)
        let rec f ts ty =
          let open Type in
          match ts, ty with
          | [t1; t2], {desc= TyPair (_f1,ty1,_f2,ty2)} ->
              Ok [(t1,ty1); (t2,ty2)]
          | [t1; t2], {desc= TyPairN ((_f1,ty1)::fts)} ->
              let+ ty2 = match fts with
                | [] | [_] -> Error (l,"Pair: type does not contain enough tuple elements")
                | [(f2,ty2); (f3,ty3)] -> Ok (tyPair (f2,ty2,f3,ty3))
                | fts -> Ok (tyPairN fts)
              in
              [(t1,ty1); (t2,ty2)]
          | t1 :: ts, {desc= TyPair (_f1,ty1,_f2,ty2)} ->
              let+ tts = f ts ty2 in
              (t1,ty1) :: tts
          | t1 :: ts, {desc= TyPairN ((_f1,ty1)::fts)} ->
              let* ty2 = match fts with
                | [] | [_] -> Error (l,"Pair: type does not contain enough tuple elements")
                | [(f2,ty2); (f3,ty3)] -> Ok (tyPair (f2,ty2,f3,ty3))
                | fts -> Ok (tyPairN fts)
              in
              let+ tts = f ts ty2 in
              (t1,ty1) :: tts
          | _ -> assert false
        in
        let* tts = f ts ty in
        let+ tts = Result.mapM (fun (t,ty) -> of_micheline ty t) tts in
        begin match tts with
          | [] | [_] -> assert false
          | [t1; t2] -> Pair (t1,t2)
          | _ -> PairN tts
        end

    | Seq (l, _), ty ->
        Error (l, Format.sprintf "invalid type for seq: %a" Type.pp ty)

  let pp ppf x = Micheline.pp ppf & to_micheline x

  let parse_string ~check_indentation ty s =
    let open Tezos_micheline.Micheline_parser in
    match tokenize s with
    | _, (_::_ as errs) -> Error errs
    | tks, [] ->
        match parse_expression ~check:check_indentation tks with
        | _, (_::_ as errs) -> Error errs
        | node, [] -> (* the outer { } is unblocked *)
            match of_micheline ty node with
            | Ok t -> Ok t
            | Error (l, e) -> Error [Parser_error (l, e)]

  let parse_file ~check_indentation ty fn =
    match File.to_string fn with
    | Error (`Exn e) -> Tezos_error_monad.Error_monad.error_with_exn e
    | Ok s -> parse_string ~check_indentation ty s

  let big_map_ids ty c =
    let tbl = Hashtbl.create 101 in
    let rec f ty c =
      match ty.Type.desc, c with
      | TyBigMap (ty1,ty2), Int n -> Hashtbl.replace tbl n (ty1, ty2)
      | (TyString
        | TyNat
        | TyInt
        | TyBytes
        | TyBool
        | TyUnit
        | TyMutez
        | TyKeyHash
        | TyTimestamp
        | TyAddress
        | TyChainID
        | TyKey
        | TySignature
        | TyOperation
        | TyContract _
        | TyNever
        | TyBLS12_381_Fr
        | TyBLS12_381_G1
        | TyBLS12_381_G2
        | TySapling_state _
        | TySapling_transaction _
        ), _ -> ()
      | TyList ty, List cs ->
          List.iter (f ty) cs
      | TyPair (_, ty1, _, ty2), Pair (c1, c2) ->
          f ty1 c1; f ty2 c2
      | TyPair (_, ty1, _, ty2), PairN (c::cs) ->
          f ty1 c;
          let c2 =
            match cs with
            | [] | [_] -> assert false
            | [c1;c2] -> Pair (c1,c2)
            | cs -> PairN cs
          in
          f ty2 c2
      | TyPairN ((_,ty)::fts), Pair (c1, c2) ->
          f ty c1;
          let ty =
            match fts with
            | [] | [_] -> assert false
            | [(f1,ty1); (f2,ty2)] -> Type.tyPair (f1, ty1, f2, ty2)
            | _ -> Type.tyPairN fts
          in
          f ty c2
      | TyPairN ((_,ty)::fts), PairN (c1::cs) ->
          f ty c1;
          let ty2 =
            match fts with
            | [] | [_] -> assert false
            | [(f1,ty1); (f2,ty2)] -> Type.tyPair (f1, ty1, f2, ty2)
            | _ -> Type.tyPairN fts
          in
          let c2 =
            match cs with
            | [] | [_] -> assert false
            | [c1;c2] -> Pair (c1,c2)
            | cs -> PairN cs
          in
          f ty2 c2
      | TyOption (_, ty), Option (Some c) ->
          f ty c
      | TyOption (_, _), Option None -> ()
      | TyOr (_, ty, _, _), Left c
      | TyOr (_, _, _, ty), Right c -> f ty c
      | TySet ty, Set cs -> List.iter (f ty) cs
      | TyMap (ty1,ty2), Map ccs
      | TyBigMap (ty1,ty2), Map ccs ->
          List.iter (fun (c1,c2) -> f ty1 c1; f ty2 c2) ccs
      | TyLambda _, _ ->
          (* It is a lambda and I think it cannot carry big maps inside *)
          ()
      | TyTicket ty, PairN [_; c; _]
      | TyTicket ty, Pair (_, Pair (c, _)) -> f ty c
      | _, c ->
          Format.eprintf "Type: %a@." Type.pp ty;
          Format.eprintf "Constant: %a@." pp c;
          assert false
    in
    f ty c;
    Hashtbl.fold (fun k v acc -> (k,v)::acc) tbl []
end

and Opcode : sig
  type t = {desc : desc; comments : string list}

  and desc =
    | BLOCK of t list
    | DUP
    | DIP of t list
    | DIPn of int * t list
    | DIG of int
    | DUG of int
    | DROP
    | DROPn of int
    | SWAP
    | PAIR of string list
    | CAR
    | CDR
    | LEFT of Type.t
    | RIGHT of Type.t
    | LAMBDA of Type.t * Type.t * t list
    | APPLY
    | PUSH of Type.t * Constant.t
    | NIL of Type.t
    | CONS
    | NONE of Type.t
    | SOME
    | COMPARE
    | EQ
    | LT
    | LE
    | GT
    | GE
    | NEQ
    | IF of t list * t list
    | ADD
    | SUB
    | MUL
    | EDIV
    | ABS
    | ISNAT
    | NEG
    | LSL
    | LSR
    | AND
    | OR
    | XOR
    | NOT
    | EXEC
    | IF_NONE of t list * t list
    | IF_LEFT of t list * t list
    | IF_CONS of t list * t list
    | FAILWITH
    | UNIT
    | EMPTY_SET of Type.t
    | EMPTY_MAP of Type.t * Type.t
    | EMPTY_BIG_MAP of Type.t * Type.t
    | SIZE
    | MEM
    | UPDATE
    | ITER of t list
    | MAP of t list
    | LOOP of t list (* It is not really useful for SCaml *)
    | LOOP_LEFT of t list
    | CONCAT
    | SELF of string option
    | GET
    | RENAME of string option
    | PACK
    | UNPACK of Type.t
    | SLICE
    | CAST of Type.t
    | CONTRACT of Type.t
    | CONTRACT' of Type.t * string (* entry point name *)
    | TRANSFER_TOKENS
    | SET_DELEGATE
    | CREATE_ACCOUNT (* Obsolete from 008 *)
    | CREATE_CONTRACT of Module.t
    | IMPLICIT_ACCOUNT
    | NOW
    | AMOUNT
    | BALANCE
    | CHECK_SIGNATURE
    | BLAKE2B
    | SHA256
    | SHA512
    | HASH_KEY
    | STEPS_TO_QUOTA (* Obsolete from 008 *)
    | SOURCE
    | SENDER
    | ADDRESS
    | CHAIN_ID
    | INT
    (* 008 *)
    | LEVEL
    | SELF_ADDRESS
    | UNPAIR (* = UNPAIRn 2 *)
    | PAIRING_CHECK
    | NEVER
    | KECCAK
    | SHA3
    | TICKET
    | READ_TICKET
    | SPLIT_TICKET
    | JOIN_TICKETS
    | SAPLING_EMPTY_STATE of int
    | SAPLING_VERIFY_UPDATE
    | VOTING_POWER
    | TOTAL_VOTING_POWER
    | GET_AND_UPDATE
    (* operators for right combs *)
    | GETn of int
    | UPDATEn of int
    | PAIRn of int
    | UNPAIRn of int
    | DUPn of int
    (* 011 *)
    | VIEW of string * Type.t
    (* 012 *)
    | SUB_MUTEZ

  val _BLOCK : t list -> t
  val _DUP : t
  val _DIP : t list -> t
  val _DIPn : int * t list -> t
  val _DIG : int -> t
  val _DUG : int -> t
  val _DROP : t
  val _DROPn : int -> t
  val _SWAP : t
  val _PAIR : string list -> t
  val _ASSERT : t
  val _CAR : t
  val _CDR : t
  val _LEFT : Type.t -> t
  val _RIGHT : Type.t -> t
  val _LAMBDA : Type.t * Type.t * t list -> t
  val _APPLY : t
  val _PUSH : Type.t * Constant.t -> t
  val _NIL : Type.t -> t
  val _CONS : t
  val _NONE : Type.t -> t
  val _SOME : t
  val _COMPARE : t
  val _EQ : t
  val _LT : t
  val _LE : t
  val _GT : t
  val _GE : t
  val _NEQ : t
  val _IF : t list * t list -> t
  val _ADD : t
  val _SUB : t
  val _MUL : t
  val _EDIV : t
  val _ABS : t
  val _ISNAT : t
  val _NEG : t
  val _LSL : t
  val _LSR : t
  val _AND : t
  val _OR : t
  val _XOR : t
  val _NOT : t
  val _EXEC : t
  val _IF_NONE : t list * t list -> t
  val _IF_LEFT : t list * t list -> t
  val _IF_CONS : t list * t list -> t
  val _FAILWITH : t
  val _UNIT : t
  val _EMPTY_SET : Type.t -> t
  val _EMPTY_MAP : Type.t * Type.t -> t
  val _EMPTY_BIG_MAP : Type.t * Type.t -> t
  val _SIZE : t
  val _MEM : t
  val _UPDATE : t
  val _ITER : t list -> t
  val _MAP : t list -> t
  val _LOOP : t list -> t
  val _LOOP_LEFT : t list -> t
  val _CONCAT : t
  val _SELF : string option -> t
  val _GET : t
  val _RENAME : string option -> t
  val _PACK : t
  val _UNPACK : Type.t -> t
  val _SLICE : t
  val _CAST : Type.t -> t
  val _CONTRACT : Type.t -> t
  val _CONTRACT' : Type.t * string -> t
  val _TRANSFER_TOKENS : t
  val _SET_DELEGATE : t
  val _CREATE_ACCOUNT : t
  val _CREATE_CONTRACT : Module.t -> t
  val _IMPLICIT_ACCOUNT : t
  val _NOW : t
  val _AMOUNT : t
  val _BALANCE : t
  val _CHECK_SIGNATURE : t
  val _BLAKE2B : t
  val _SHA256 : t
  val _SHA512 : t
  val _HASH_KEY : t
  val _STEPS_TO_QUOTA : t
  val _SOURCE : t
  val _SENDER : t
  val _ADDRESS : t
  val _CHAIN_ID : t
  val _INT : t
  val _LEVEL : t
  val _SELF_ADDRESS : t
  val _UNPAIR : t
  val _PAIRING_CHECK : t
  val _NEVER : t
  val _KECCAK : t
  val _SHA3 : t
  val _TICKET : t
  val _READ_TICKET : t
  val _SPLIT_TICKET : t
  val _JOIN_TICKETS : t
  val _SAPLING_EMPTY_STATE : int -> t
  val _SAPLING_VERIFY_UPDATE : t
  val _VOTING_POWER : t
  val _TOTAL_VOTING_POWER : t
  val _GET_AND_UPDATE : t
  val _GETn : int -> t
  val _UPDATEn : int -> t
  val _PAIRn : int -> t
  val _UNPAIRn : int -> t
  val _DUPn : int -> t
  val _VIEW : string * Type.t -> t

  val set_comments : t -> string list -> t
  val prepend_comments : string list -> t -> t
  val append_comments : t -> string list -> t
  val append_comments_at_last : t list -> string list -> t list

  include Micheline.S with type t := t

  val clean_failwith : t list -> t list * bool

  (** { op1; op2 } => [ op1; op2 ] *)
  val unblock : t -> t list

  val is_pure_push : desc -> bool
  val end_with_failwith : t list -> bool
end = struct
  type t = {desc : desc; comments : string list}

  and desc =
    | BLOCK of t list
    | DUP
    | DIP of t list
    | DIPn of int * t list
    | DIG of int
    | DUG of int
    | DROP
    | DROPn of int
    | SWAP
    | PAIR of string list
    | CAR
    | CDR
    | LEFT of Type.t
    | RIGHT of Type.t
    | LAMBDA of Type.t * Type.t * t list
    | APPLY
    | PUSH of Type.t * Constant.t
    | NIL of Type.t
    | CONS
    | NONE of Type.t
    | SOME
    | COMPARE
    | EQ
    | LT
    | LE
    | GT
    | GE
    | NEQ
    | IF of t list * t list
    | ADD
    | SUB
    | MUL
    | EDIV
    | ABS
    | ISNAT
    | NEG
    | LSL
    | LSR
    | AND
    | OR
    | XOR
    | NOT
    | EXEC
    | IF_NONE of t list * t list
    | IF_LEFT of t list * t list
    | IF_CONS of t list * t list
    | FAILWITH
    | UNIT
    | EMPTY_SET of Type.t
    | EMPTY_MAP of Type.t * Type.t
    | EMPTY_BIG_MAP of Type.t * Type.t
    | SIZE
    | MEM
    | UPDATE
    | ITER of t list
    | MAP of t list
    | LOOP of t list (* It is not really useful for SCaml *)
    | LOOP_LEFT of t list
    | CONCAT
    | SELF of string option
    | GET
    | RENAME of string option
    | PACK
    | UNPACK of Type.t
    | SLICE
    | CAST of Type.t
    | CONTRACT of Type.t
    | CONTRACT' of Type.t * string
    | TRANSFER_TOKENS
    | SET_DELEGATE
    | CREATE_ACCOUNT (* deprecated *)
    | CREATE_CONTRACT of Module.t
    | IMPLICIT_ACCOUNT
    | NOW
    | AMOUNT
    | BALANCE
    | CHECK_SIGNATURE
    | BLAKE2B
    | SHA256
    | SHA512
    | HASH_KEY
    | STEPS_TO_QUOTA
    | SOURCE
    | SENDER
    | ADDRESS
    | CHAIN_ID
    | INT
    (* 008 *)
    | LEVEL
    | SELF_ADDRESS
    | UNPAIR
    | PAIRING_CHECK
    | NEVER
    | KECCAK
    | SHA3
    | TICKET
    | READ_TICKET
    | SPLIT_TICKET
    | JOIN_TICKETS
    | SAPLING_EMPTY_STATE of int
    | SAPLING_VERIFY_UPDATE
    | VOTING_POWER
    | TOTAL_VOTING_POWER
    | GET_AND_UPDATE
    | GETn of int
    | UPDATEn of int
    | PAIRn of int
    | UNPAIRn of int
    | DUPn of int
    (* 011 *)
    | VIEW of string * Type.t
    (* 012 *)
    | SUB_MUTEZ

  let op desc = {desc; comments = []}

  let _BLOCK ts = op & BLOCK ts
  let _DUP = op DUP
  let _DIP x = op & DIP x
  let _DIPn (x, y) = assert (x >= 0); op & DIPn (x, y)
  let _DIG x = assert (x >= 0); op & DIG x
  let _DUG x = assert (x >= 0); op & DUG x
  let _DROP = op DROP
  let _DROPn x = assert (x >= 0); op & DROPn x
  let _SWAP = op SWAP
  let _PAIR fs = op (PAIR fs)
  let _CAR = op CAR
  let _CDR = op CDR
  let _LEFT x = op & LEFT x
  let _RIGHT x = op & RIGHT x
  let _LAMBDA (x, y, z) = op & LAMBDA (x, y, z)
  let _APPLY = op APPLY
  let _PUSH (x, y) = op & PUSH (x, y)
  let _NIL x = op & NIL x
  let _CONS = op CONS
  let _NONE x = op & NONE x
  let _SOME = op SOME
  let _COMPARE = op COMPARE
  let _EQ = op EQ
  let _LT = op LT
  let _LE = op LE
  let _GT = op GT
  let _GE = op GE
  let _NEQ = op NEQ
  let _IF (x, y) = op & IF (x, y)
  let _ADD = op ADD
  let _SUB = op SUB
  let _MUL = op MUL
  let _EDIV = op EDIV
  let _ABS = op ABS
  let _ISNAT = op ISNAT
  let _NEG = op NEG
  let _LSL = op LSL
  let _LSR = op LSR
  let _AND = op AND
  let _OR = op OR
  let _XOR = op XOR
  let _NOT = op NOT
  let _EXEC = op EXEC
  let _IF_NONE (x, y) = op & IF_NONE (x, y)
  let _IF_LEFT (x, y) = op & IF_LEFT (x, y)
  let _IF_CONS (x, y) = op & IF_CONS (x, y)
  let _FAILWITH = op FAILWITH
  let _UNIT = op UNIT
  let _EMPTY_SET x = op & EMPTY_SET x
  let _EMPTY_MAP (x, y) = op & EMPTY_MAP (x, y)
  let _EMPTY_BIG_MAP (x, y) = op & EMPTY_BIG_MAP (x, y)
  let _SIZE = op SIZE
  let _MEM = op MEM
  let _UPDATE = op UPDATE
  let _ITER x = op & ITER x
  let _MAP x = op & MAP x
  let _LOOP x = op & LOOP x
  let _LOOP_LEFT x = op & LOOP_LEFT x
  let _CONCAT = op CONCAT
  let _SELF n = op (SELF n)
  let _GET = op GET
  let _RENAME x = op & RENAME x
  let _PACK = op PACK
  let _UNPACK x = op & UNPACK x
  let _SLICE = op SLICE
  let _CAST x = op (CAST x)
  let _CONTRACT x = op & CONTRACT x
  let _CONTRACT' (x, y) = op & CONTRACT' (x, y)
  let _TRANSFER_TOKENS = op TRANSFER_TOKENS
  let _SET_DELEGATE = op SET_DELEGATE
  let _CREATE_ACCOUNT = op CREATE_ACCOUNT
  let _CREATE_CONTRACT x = op & CREATE_CONTRACT x
  let _IMPLICIT_ACCOUNT = op IMPLICIT_ACCOUNT
  let _NOW = op NOW
  let _AMOUNT = op AMOUNT
  let _BALANCE = op BALANCE
  let _CHECK_SIGNATURE = op CHECK_SIGNATURE
  let _BLAKE2B = op BLAKE2B
  let _SHA256 = op SHA256
  let _SHA512 = op SHA512
  let _HASH_KEY = op HASH_KEY
  let _STEPS_TO_QUOTA = op STEPS_TO_QUOTA
  let _SOURCE = op SOURCE
  let _SENDER = op SENDER
  let _ADDRESS = op ADDRESS
  let _CHAIN_ID = op CHAIN_ID
  let _INT = op INT
  let _LEVEL = op LEVEL
  let _SELF_ADDRESS = op SELF_ADDRESS
  let _UNPAIR = op UNPAIR
  let _PAIRING_CHECK = op PAIRING_CHECK
  let _NEVER = op NEVER
  let _KECCAK = op KECCAK
  let _SHA3 = op SHA3
  let _TICKET = op TICKET
  let _READ_TICKET = op READ_TICKET
  let _SPLIT_TICKET = op SPLIT_TICKET
  let _JOIN_TICKETS = op JOIN_TICKETS
  let _SAPLING_EMPTY_STATE x = op & SAPLING_EMPTY_STATE x
  let _SAPLING_VERIFY_UPDATE = op SAPLING_VERIFY_UPDATE
  let _VOTING_POWER = op VOTING_POWER
  let _TOTAL_VOTING_POWER = op TOTAL_VOTING_POWER
  let _GET_AND_UPDATE = op GET_AND_UPDATE
  let _GETn x = assert (x >= 0); op & GETn x
  let _UPDATEn x = assert (x >= 0); op & UPDATEn x
  let _PAIRn x = assert (x >= 2); op & PAIRn x
  let _UNPAIRn x = assert (x >= 2); op & UNPAIRn x
  let _SUB_MUTEZ = op SUB_MUTEZ

  let _DUPn x =
    assert (x > 0);
    op & DUPn x

  let _VIEW (n, ty) = op & VIEW (n, ty)

  let _ASSERT = _IF ([], [_UNIT; _FAILWITH])

  let set_comments t ss = {t with comments = ss}
  let prepend_comments ss t = {t with comments = ss @ t.comments}
  let append_comments t ss = {t with comments = t.comments @ ss}

  let append_comments_at_last ts ss =
    match ss with
    | [] -> ts
    | _ ->
        let rec f = function
          | [] -> [append_comments (_BLOCK []) ss]
          | [t] -> [append_comments t ss]
          | t :: ts -> t :: f ts
        in
        f ts

  let to_micheline t =
    let open Micheline in
    let prim x args = Micheline.prim x args [] in
    let prim_with_fields x args fs = Micheline.prim x args (List.map (fun s -> "%"^s) fs) in
    let ( ! ) x = prim x [] in
    let rec fs ts = seq (List.map f ts)
    and f t =
      let x = f' t in
      match t.comments with
      | [] -> x
      | cs -> set_comment x (Some (String.concat "\n" cs))
    and f' t =
      match t.desc with
      | BLOCK ts -> fs ts
      | DUP -> !"DUP"
      | DIP code -> prim "DIP" [fs code]
      | DIPn (n, code) -> prim "DIP" [int & Z.of_int n; fs code]
      | DIG n -> prim "DIG" [int & Z.of_int n]
      | DUG n -> prim "DUG" [int & Z.of_int n]
      | SWAP -> !"SWAP"
      | PAIR [] -> !"PAIR"
      | PAIR fs -> prim_with_fields "PAIR" [] fs
      | PUSH (ty, const) ->
          prim "PUSH" [Type.to_micheline ty; Constant.to_micheline const]
      | CAR -> !"CAR"
      | CDR -> !"CDR"
      | LEFT ty -> prim "LEFT" [Type.to_micheline ty]
      | RIGHT ty -> prim "RIGHT" [Type.to_micheline ty]
      | LAMBDA (ty1, ty2, code) ->
          prim "LAMBDA" [Type.to_micheline ty1; Type.to_micheline ty2; fs code]
      | APPLY -> !"APPLY"
      | CONS -> !"CONS"
      | NIL ty -> prim "NIL" [Type.to_micheline ty]
      | SOME -> !"SOME"
      | NONE ty -> prim "NONE" [Type.to_micheline ty]
      | DROP -> !"DROP"
      | DROPn n -> prim "DROP" [int & Z.of_int n]
      | COMPARE -> !"COMPARE"
      | EQ -> !"EQ"
      | LT -> !"LT"
      | LE -> !"LE"
      | GT -> !"GT"
      | GE -> !"GE"
      | NEQ -> !"NEQ"
      | IF (t, e) -> prim "IF" [fs t; fs e]
      | IF_NONE (t, e) -> prim "IF_NONE" [fs t; fs e]
      | ADD -> !"ADD"
      | SUB -> !"SUB"
      | MUL -> !"MUL"
      | EDIV -> !"EDIV"
      | ABS -> !"ABS"
      | ISNAT -> !"ISNAT"
      | NEG -> !"NEG"
      | LSL -> !"LSL"
      | LSR -> !"LSR"
      | AND -> !"AND"
      | OR -> !"OR"
      | XOR -> !"XOR"
      | NOT -> !"NOT"
      | EXEC -> !"EXEC"
      | FAILWITH -> !"FAILWITH"
      | IF_LEFT (t1, t2) -> prim "IF_LEFT" [fs t1; fs t2]
      | IF_CONS (t1, t2) -> prim "IF_CONS" [fs t1; fs t2]
      | UNIT -> !"UNIT"
      | EMPTY_SET ty -> prim "EMPTY_SET" [Type.to_micheline ty]
      | EMPTY_MAP (ty1, ty2) ->
          prim "EMPTY_MAP" [Type.to_micheline ty1; Type.to_micheline ty2]
      | EMPTY_BIG_MAP (ty1, ty2) ->
          prim "EMPTY_BIG_MAP" [Type.to_micheline ty1; Type.to_micheline ty2]
      | SIZE -> !"SIZE"
      | MEM -> !"MEM"
      | UPDATE -> !"UPDATE"
      | ITER code -> prim "ITER" [fs code]
      | MAP code -> prim "MAP" [fs code]
      | LOOP code -> prim "LOOP" [fs code]
      | LOOP_LEFT code -> prim "LOOP_LEFT" [fs code]
      | CONCAT -> !"CONCAT"
      | SELF None -> !"SELF"
      | SELF (Some n) -> Micheline.prim "SELF" [] ["%" ^ n]
      | GET -> !"GET"
      | RENAME (Some s) -> Micheline.prim "RENAME" [] ["@" ^ s]
      | RENAME None -> Micheline.prim "RENAME" [] []
      | PACK -> !"PACK"
      | UNPACK ty -> prim "UNPACK" [Type.to_micheline ty]
      | SLICE -> !"SLICE"
      | CAST ty -> prim "CAST" [Type.to_micheline ty]
      | CONTRACT ty -> prim "CONTRACT" [Type.to_micheline ty]
      | CONTRACT' (ty, n) ->
          Micheline.prim "CONTRACT" [Type.to_micheline ty] ["%" ^ n]
      | TRANSFER_TOKENS -> !"TRANSFER_TOKENS"
      | SET_DELEGATE -> !"SET_DELEGATE"
      | CREATE_ACCOUNT -> !"CREATE_ACCOUNT"
      | IMPLICIT_ACCOUNT -> !"IMPLICIT_ACCOUNT"
      | NOW -> !"NOW"
      | AMOUNT -> !"AMOUNT"
      | BALANCE -> !"BALANCE"
      | CHECK_SIGNATURE -> !"CHECK_SIGNATURE"
      | BLAKE2B -> !"BLAKE2B"
      | SHA256 -> !"SHA256"
      | SHA512 -> !"SHA512"
      | HASH_KEY -> !"HASH_KEY"
      | STEPS_TO_QUOTA -> !"STEPS_TO_QUOTA"
      | SOURCE -> !"SOURCE"
      | SENDER -> !"SENDER"
      | ADDRESS -> !"ADDRESS"
      | CHAIN_ID -> !"CHAIN_ID"
      | CREATE_CONTRACT m ->
          prim "CREATE_CONTRACT" [ Module.to_micheline m ]
      | LEVEL -> !"LEVEL"
      | SELF_ADDRESS -> !"SELF_ADDRESS"
      | UNPAIR -> !"UNPAIR"
      | INT -> !"INT"
      | PAIRING_CHECK -> !"PAIRING_CHECK"
      | NEVER -> !"NEVER"
      | KECCAK -> !"KECCAK"
      | SHA3 -> !"SHA3"
      | TICKET -> !"TICKET"
      | READ_TICKET -> !"READ_TICKET"
      | SPLIT_TICKET -> !"SPLIT_TICKET"
      | JOIN_TICKETS -> !"JOIN_TICKETS"
      | SAPLING_EMPTY_STATE n -> prim "SAPLING_EMPTY_STATE" [int & Z.of_int n]
      | SAPLING_VERIFY_UPDATE -> !"SAPLING_VERIFY_UPDATE"
      | VOTING_POWER -> !"VOTING_POWER"
      | TOTAL_VOTING_POWER -> !"TOTAL_VOTING_POWER"
      | GET_AND_UPDATE -> !"GET_AND_UPDATE"
      | GETn n -> prim "GET" [int & Z.of_int n]
      | UPDATEn n -> prim "UPDATE" [int & Z.of_int n]
      | PAIRn n -> prim "PAIR" [int & Z.of_int n]
      | UNPAIRn n -> prim "UNPAIR" [int & Z.of_int n]
      | DUPn n -> prim "DUP" [int & Z.of_int n]
      | VIEW (n, ty) -> prim "VIEW" [string n; Type.to_micheline ty]
      | SUB_MUTEZ -> !"SUB_MUTEZ"
    in
    f t

  module TM = Tezos_micheline.Micheline

  let unblock t =
    match t.desc with
    | BLOCK xs -> xs
    | _ -> [t]

  let rec of_micheline node : (t, _) result =
    let open Result.Syntax in
    let of_type = Type.of_micheline in
    let of_const = Constant.of_micheline in
    let+ (desc : desc) =
      match node with
      | TM.Seq (_l, nodes) ->
         let+ ts = Result.mapM of_micheline nodes in
         BLOCK ts
      | Prim (l, n, args, annots) ->
          let* annots = Result.mapM (Annot.parse l) annots in
          let annots = ref annots in
          let consume_fields () =
            let fs, rest = Annot.get_fields !annots in
            annots := rest;
            fs
          in
          let consume_vars () =
            let vs, rest = Annot.get_vars !annots in
            annots := rest;
            vs
          in
          (* cannot access the ref anymore except consume_X *)
          let [@warning "-26"] annots = () in
          let* t = match n, args with
            | "DUP", [] -> Ok DUP
            | "SWAP", [] -> Ok SWAP
            | "PAIR", [] ->
                begin match consume_fields () with
                  | [] -> Ok (PAIR [])
                  | [s] -> Ok (PAIR [s])
                  | [s1;s2] -> Ok (PAIR [s1; s2])
                  | _ -> Error (l, "too many annotations")
                end
            | "CAR", [] -> Ok CAR
            | "CDR", [] -> Ok CDR
            | "APPLY", [] -> Ok APPLY
            | "CONS", [] -> Ok CONS
            | "SOME", [] -> Ok SOME
            | "DROP", [] -> Ok DROP
            | "COMPARE", [] -> Ok COMPARE
            | "EQ", [] -> Ok EQ
            | "LT", [] -> Ok LT
            | "LE", [] -> Ok LE
            | "GT", [] -> Ok GT
            | "GE", [] -> Ok GE
            | "NEQ", [] -> Ok NEQ
            | "ADD", [] -> Ok ADD
            | "SUB", [] -> Ok SUB
            | "SUB_MUTEZ", [] -> Ok SUB_MUTEZ
            | "MUL", [] -> Ok MUL
            | "EDIV", [] -> Ok EDIV
            | "ABS", [] -> Ok ABS
            | "ISNAT", [] -> Ok ISNAT
            | "NEG", [] -> Ok NEG
            | "LSL", [] -> Ok LSL
            | "LSR", [] -> Ok LSR
            | "AND", [] -> Ok AND
            | "OR", [] -> Ok OR
            | "XOR", [] -> Ok XOR
            | "NOT", [] -> Ok NOT
            | "EXEC", [] -> Ok EXEC
            | "FAILWITH", [] -> Ok FAILWITH
            | "SIZE", [] -> Ok SIZE
            | "MEM", [] -> Ok MEM
            | "UPDATE", [] -> Ok UPDATE
            | "CONCAT", [] -> Ok CONCAT
            | "SELF", [] ->
                begin match consume_fields () with
                  | [] -> Ok (SELF None)
                  | [s] -> Ok (SELF (Some s))
                  | _ -> Error (l, "too many annotations")
                end
            | "GET", [] -> Ok GET
            | "PACK", [] -> Ok PACK
            | "SLICE", [] -> Ok SLICE
            | "TRANSFER_TOKENS", [] -> Ok TRANSFER_TOKENS
            | "SET_DELEGATE", [] -> Ok SET_DELEGATE
            | "CREATE_ACCOUNT", [] -> Ok CREATE_ACCOUNT
            | "IMPLICIT_ACCOUNT", [] -> Ok IMPLICIT_ACCOUNT
            | "NOW", [] -> Ok NOW
            | "AMOUNT", [] -> Ok AMOUNT
            | "BALANCE", [] -> Ok BALANCE
            | "CHECK_SIGNATURE", [] -> Ok CHECK_SIGNATURE
            | "BLAKE2B", [] -> Ok BLAKE2B
            | "SHA256", [] -> Ok SHA256
            | "SHA512", [] -> Ok SHA512
            | "HASH_KEY", [] -> Ok HASH_KEY
            | "STEPS_TO_QUOTA", [] -> Ok STEPS_TO_QUOTA
            | "SOURCE", [] -> Ok SOURCE
            | "SENDER", [] -> Ok SENDER
            | "ADDRESS", [] -> Ok ADDRESS
            | "CHAIN_ID", [] -> Ok CHAIN_ID
            | "LEVEL", [] -> Ok LEVEL
            | "SELF_ADDRESS", [] -> Ok SELF_ADDRESS
            | "UNPAIR", [] -> Ok UNPAIR
            | "INT", [] -> Ok INT
            | "PAIRING_CHECK", [] -> Ok PAIRING_CHECK
            | "NEVER", [] -> Ok NEVER
            | "KECCAK", [] -> Ok KECCAK
            | "SHA3", [] -> Ok SHA3
            | "TICKET", [] -> Ok TICKET
            | "READ_TICKET", [] -> Ok READ_TICKET
            | "SPLIT_TICKET", [] -> Ok SPLIT_TICKET
            | "JOIN_TICKETS", [] -> Ok JOIN_TICKETS
            | "SAPLING_VERIFY_UPDATE", [] -> Ok SAPLING_VERIFY_UPDATE
            | "VOTING_POWER", [] -> Ok VOTING_POWER
            | "TOTAL_VOTING_POWER", [] -> Ok TOTAL_VOTING_POWER
            | "GET_AND_UPDATE", [] -> Ok GET_AND_UPDATE
            | "UNIT", [] -> Ok UNIT

            | "DIP", [t] ->
                let+ t = of_micheline t in
                DIP (unblock t)
            | "DIP", [Int (_, z); t] ->
                let+ t = of_micheline t in
                DIPn (Z.to_int z, unblock t)
            | "DIG", [Int (_, z)] ->
                Ok (DIG (Z.to_int z))
            | "DUG", [Int (_, z)] ->
                Ok (DUG (Z.to_int z))
            | "PUSH", [ty; const] ->
                let* ty = of_type ty in
                let+ const = of_const ty const in
                PUSH (ty, const)
            | "CAST", [ty] ->
                let+ ty = of_type ty in
                CAST ty
            | "LEFT", [ty] ->
                let+ ty = of_type ty in
                LEFT ty
            | "RIGHT", [ty] ->
                let+ ty = of_type ty in
                RIGHT ty
            | "NIL", [ty] ->
                let+ ty = of_type ty in
                NIL ty
            | "NONE", [ty] ->
                let+ ty = of_type ty in
                NONE ty
            | "LAMBDA", [ty1; ty2; code] ->
                let* ty1 = of_type ty1 in
                let* ty2 = of_type ty2 in
                let+ code = of_micheline code in
                LAMBDA (ty1, ty2, unblock code)
            | "DROP", [Int (_, z)] ->
                Ok (DROPn (Z.to_int z))
            | "EMPTY_SET", [ty] ->
                let+ ty = of_type ty in
                EMPTY_SET ty
            | "EMPTY_MAP", [ty1; ty2] ->
                let* ty1 = of_type ty1 in
                let+ ty2 = of_type ty2 in
                EMPTY_MAP (ty1, ty2)
            | "EMPTY_BIG_MAP", [ty1; ty2] ->
                let* ty1 = of_type ty1 in
                let+ ty2 = of_type ty2 in
                EMPTY_BIG_MAP (ty1, ty2)
            | "UNPACK", [ty] ->
                let+ ty = of_type ty in
                UNPACK ty
            | "CONTRACT", [ty] ->
                let* ty = of_type ty in
                begin match consume_fields () with
                  | [] -> Ok (CONTRACT ty)
                  | [n] -> Ok (CONTRACT' (ty, n))
                  | _ -> Error (l, "too many annotations")
                end
            | "GET", [Int (_, z)] ->
                Ok (GETn (Z.to_int z))
            | "UPDATE", [Int (_, z)] ->
                Ok (UPDATEn (Z.to_int z))
            | "PAIR", [Int (_, z)] ->
                Ok (PAIRn (Z.to_int z))
            | "UNPAIR", [Int (_, z)] ->
                Ok (UNPAIRn (Z.to_int z))
            | "DUP", [Int (_, z)] ->
                Ok (DUPn (Z.to_int z))
            | "IF", [t1; t2] ->
                let* t1 = of_micheline t1 in
                let+ t2 = of_micheline t2 in
                IF (unblock t1, unblock t2)
            | "IF_NONE", [t1; t2] ->
                let* t1 = of_micheline t1 in
                let+ t2 = of_micheline t2 in
                IF_NONE (unblock t1, unblock t2)
            | "IF_LEFT", [t1; t2] ->
                let* t1 = of_micheline t1 in
                let+ t2 = of_micheline t2 in
                IF_LEFT (unblock t1, unblock t2)
            | "IF_CONS", [t1; t2] ->
                let* t1 = of_micheline t1 in
                let+ t2 = of_micheline t2 in
                IF_CONS (unblock t1, unblock t2)
            | "ITER", [t] ->
                let+ t = of_micheline t in
                ITER (unblock t)
            | "MAP", [t] ->
                let+ t = of_micheline t in
                MAP (unblock t)
            | "LOOP", [t] ->
                let+ t = of_micheline t in
                LOOP (unblock t)
            | "LOOP_LEFT", [t] ->
                let+ t = of_micheline t in
                LOOP_LEFT (unblock t)
            | "RENAME", [] ->
                begin match consume_vars () with
                  | [] -> Ok (RENAME None)
                  | [s] -> Ok (RENAME (Some s))
                  | _ -> Error (l, "RENAME can have at most 1 variable annotation")
                end
            | "CREATE_CONTRACT", [s] ->
                let+ m = Module.of_micheline s in
                CREATE_CONTRACT m
            | "SAPLING_EMPTY_STATE", [Int (_, z)] ->
                Ok (SAPLING_EMPTY_STATE (Z.to_int z))
            | "VIEW", [String (_, s); ty] ->
                let+ ty = of_type ty in
                VIEW (s, ty)
            | _ -> Error (l, Format.asprintf "unknown opcode or invalid arity: %s %a"
                            n Micheline.pp (Micheline.Parse.of_parsed node))
          in
          begin match consume_fields () with
            | [] -> Ok t
            | _ ->
                Format.eprintf "Warning: unused field annotation: %a@."
                         Micheline.pp (Micheline.Parse.of_parsed node);
                Ok t
          end
      | _ -> Error (TM.location node, "unknown opcode or invalid arity")
    in
    { desc; comments= [] }

  let pp ppf t = Micheline.pp ppf & to_micheline t

  (* { .. ; FAILWITH ; op1 ; op2 .. } => { .. ; FAILWITH } *)
  let clean_failwith =
    let rec clean_failwith = function
      | [] -> [], false
      | x :: xs ->
          let x, end_with_failwith = aux x in
          if end_with_failwith then [x], true
          else
            let xs, b = clean_failwith xs in
            x :: xs, b
    and clean_failwith' xs = fst @@ clean_failwith xs
    and aux ({desc} as t) =
      let desc, b = aux_desc desc in
      {t with desc}, b
    and aux_desc = function
      | BLOCK ts ->
          let ts, b = clean_failwith ts in
          BLOCK ts, b
      | FAILWITH -> FAILWITH, true
      | DIP ts ->
          let ts, b = clean_failwith ts in
          DIP ts, b
      | DIPn (n, ts) ->
          let ts, b = clean_failwith ts in
          DIPn (n, ts), b
      | ITER ts -> ITER (clean_failwith' ts), false
      | MAP ts -> MAP (clean_failwith' ts), false
      | LAMBDA (ty1, ty2, ts) -> LAMBDA (ty1, ty2, clean_failwith' ts), false
      | IF (t1, t2) ->
          let t1, b1 = clean_failwith t1 in
          let t2, b2 = clean_failwith t2 in
          IF (t1, t2), b1 && b2
      | IF_NONE (t1, t2) ->
          let t1, b1 = clean_failwith t1 in
          let t2, b2 = clean_failwith t2 in
          IF_NONE (t1, t2), b1 && b2
      | IF_LEFT (t1, t2) ->
          let t1, b1 = clean_failwith t1 in
          let t2, b2 = clean_failwith t2 in
          IF_LEFT (t1, t2), b1 && b2
      | IF_CONS (t1, t2) ->
          let t1, b1 = clean_failwith t1 in
          let t2, b2 = clean_failwith t2 in
          IF_CONS (t1, t2), b1 && b2
      | LOOP ts -> LOOP (clean_failwith' ts), false
      | LOOP_LEFT ts -> LOOP_LEFT (clean_failwith' ts), false
      | CREATE_CONTRACT m -> CREATE_CONTRACT m, false
      | PUSH (ty, c) -> PUSH (ty, constant c), false
      | ( DUP | DIG _ | DUG _ | DROP | DROPn _ | SWAP | PAIR _ | CAR | CDR
        | LEFT _ | RIGHT _ | NIL _ | CONS | NONE _ | SOME | COMPARE | EQ | LT
        | LE | GT | GE | NEQ | ADD | SUB | MUL | EDIV | ABS | ISNAT | NEG | LSL
        | LSR | AND | OR | XOR | NOT | EXEC | UNIT | EMPTY_SET _ | EMPTY_MAP _
        | EMPTY_BIG_MAP _ | SIZE | MEM | UPDATE | CONCAT | SELF _ | GET | RENAME _
        | PACK | UNPACK _ | SLICE | CAST _ | CONTRACT _ | CONTRACT' _
        | TRANSFER_TOKENS | SET_DELEGATE | CREATE_ACCOUNT | IMPLICIT_ACCOUNT
        | NOW | AMOUNT | BALANCE | CHECK_SIGNATURE | BLAKE2B | SHA256 | SHA512
        | HASH_KEY | STEPS_TO_QUOTA | SOURCE | SENDER | ADDRESS | APPLY
        | CHAIN_ID | UNPAIR | SELF_ADDRESS | LEVEL | INT | PAIRING_CHECK | NEVER
        | KECCAK | SHA3 | TICKET | READ_TICKET | SPLIT_TICKET | JOIN_TICKETS
        | SAPLING_EMPTY_STATE _ | SAPLING_VERIFY_UPDATE | VOTING_POWER
        | TOTAL_VOTING_POWER | GET_AND_UPDATE | GETn _ | UPDATEn _ | PAIRn _
        | UNPAIRn _ | DUPn _ | VIEW _ | SUB_MUTEZ ) as t ->
          t, false
    and constant =
      let open Constant in
      function
      | Code ops -> Code (clean_failwith' ops)
      | Option (Some t) -> Option (Some (constant t))
      | List ts -> List (List.map constant ts)
      | Set ts -> Set (List.map constant ts)
      | Map kvs -> Map (List.map (fun (k, v) -> constant k, constant v) kvs)
      | Pair (t1, t2) -> Pair (constant t1, constant t2)
      | PairN ts -> PairN (List.map constant ts)
      | Left t -> Left (constant t)
      | Right t -> Right (constant t)
      | (Unit | Bool _ | Int _ | String _ | Bytes _ | Timestamp _ | Option None)
        as c ->
          c
    in
    clean_failwith

  let rec end_with_failwith = function
    | [] -> false
    | [{desc = FAILWITH}] -> true
    | _ :: os -> end_with_failwith os

  (* Ops which consumes nothing then pushes 1 item to the stack *)
  let is_pure_push = function
    | UNIT | LAMBDA _ | PUSH _ | NIL _ | NONE _ | EMPTY_SET _ | EMPTY_MAP _
    | EMPTY_BIG_MAP _ | SELF _ | NOW | AMOUNT | BALANCE | STEPS_TO_QUOTA | SOURCE
    | SENDER | CHAIN_ID | LEVEL | SELF_ADDRESS | VOTING_POWER | TOTAL_VOTING_POWER ->
        true
    | BLOCK _ -> false (* We can investigate within the block... *)
    | DUP | SWAP | PAIR _ | CAR | CDR | APPLY | CONS | SOME | COMPARE | EQ
    | LT | LE | GT | GE | NEQ | ADD | SUB | MUL | EDIV | ABS | ISNAT | NEG | LSL | LSR
    | AND | OR | XOR | NOT | EXEC | FAILWITH | SIZE | MEM | UPDATE | CONCAT | GET | PACK
    | SLICE | CAST _ | TRANSFER_TOKENS | SET_DELEGATE | CREATE_ACCOUNT | IMPLICIT_ACCOUNT
    | CHECK_SIGNATURE | BLAKE2B | SHA256 | SHA512 | HASH_KEY | ADDRESS | INT | UNPAIR
    | PAIRING_CHECK | NEVER | KECCAK | SHA3 | TICKET | READ_TICKET | SPLIT_TICKET | JOIN_TICKETS
    | SAPLING_VERIFY_UPDATE | GET_AND_UPDATE | DIP _ | DIPn _ | DIG _ | DUG _ | DROP | DROPn _ | LEFT _
    | RIGHT _ | IF (_, _) | IF_NONE (_, _) | IF_LEFT (_, _) | IF_CONS (_, _) | ITER _ | MAP _
    | LOOP _ | LOOP_LEFT _ | RENAME _ | UNPACK _ | CONTRACT _ | CONTRACT' _
    | CREATE_CONTRACT _ | SAPLING_EMPTY_STATE _ | GETn _ | UPDATEn _ | PAIRn _ | UNPAIRn _
    | DUPn _ | VIEW _ | SUB_MUTEZ -> false

end and View : sig
  type t = {
    name : string;
    ty_param : Type.t;
    ty_return : Type.t;
    code : Opcode.t list;
  }

  include Micheline.S with type t := t
end = struct
  type t = {
    name : string;
    ty_param : Type.t;
    ty_return : Type.t;
    code : Opcode.t list;
  }

  let to_micheline { name; ty_param; ty_return; code } =
    let open Micheline in
    prim
      "view"
      [
        Micheline.string name;
        Type.to_micheline ty_param;
        Type.to_micheline ty_return;
        Micheline.seq (List.map Opcode.to_micheline code);
      ]
      []

  let pp ppf x = Micheline.pp ppf & to_micheline x

  module TM = Tezos_micheline.Micheline

  let of_micheline n =
    let open Result.Syntax in
    match n with
    | TM.Prim (_l, "view", [String (_, name); ty1; ty2; code], []) ->
        let* ty1 = Type.of_micheline ty1 in
        let* ty2 = Type.of_micheline ty2 in
        let+ code = Opcode.of_micheline code in
        {name; ty_param=ty1; ty_return=ty2; code= Opcode.unblock code}
    | _ -> Error (TM.location n, "module must be a seq")

end and Module : sig
  type t = {
    parameter : Type.t;
    storage : Type.t;
    code : Opcode.t list;
    views : View.t list;
  }

  include Micheline.S with type t := t

  val parse_string : check_indentation:bool -> string -> t Tezos_error_monad.Error_monad.tzresult
  val parse_file : check_indentation:bool -> string -> t Tezos_error_monad.Error_monad.tzresult

  val save : string -> t -> unit
end = struct
  type t = {
    parameter : Type.t;
    storage : Type.t;
    code : Opcode.t list;
    views : View.t list;
  }

  let to_micheline {parameter; storage; code; views} =
    let open Micheline in
    seq @@
    [
      prim "parameter" [Type.to_micheline parameter] [];
      prim "storage" [Type.to_micheline storage] [];
      prim "code" [Micheline.seq (List.map Opcode.to_micheline code)] [];
    ]
    @ List.map View.to_micheline views

  module TM = Tezos_micheline.Micheline

  let of_micheline n =
    let open Result.Syntax in
    match n with
    | TM.Seq (l, ns) ->
        let f = function
          (* KT1SL6CGhjPUyLypDbFv9bXsNF2sHG7Fy3j9 has  parameter %main unit ;  *)
          | TM.Prim (_l, "parameter", [ty], _) ->
              let+ ty = Type.of_micheline ty in
              `Parameter ty
          | Prim (_l, "storage", [ty], []) ->
              let+ ty = Type.of_micheline ty in
              `Storage ty
          | Prim (_l, "code", [code], []) ->
              let+ code = Opcode.of_micheline code in
              `Code (Opcode.unblock code)
          | Prim (_l, "view", _, _) as m ->
              let+ v = View.of_micheline m in
              `View v
          | n -> Error (TM.location n, "invalid module item")
        in
        let* items = Result.mapM f ns in
        let* parameter =
          match
            List.filter_map (function `Parameter ty -> Some ty | _ -> None) items
          with
          | [] -> Error (l, "parameter not found")
          | [ty] -> Ok ty
          | _ -> Error (l, "more than 1 parameters found")
        in
        let* storage =
          match
            List.filter_map (function `Storage ty -> Some ty | _ -> None) items
          with
          | [] -> Error (l, "storage not found")
          | [ty] -> Ok ty
          | _ -> Error (l, "more than 1 storages found")
        in
        let+ code =
          match
            List.filter_map (function `Code code -> Some code | _ -> None) items
          with
          | [] -> Error (l, "code not found")
          | [ty] -> Ok ty
          | _ -> Error (l, "more than 1 codes found")
        in
        (* XXX no check of dupe of views *)
        let views = List.filter_map (function `View v -> Some v | _ -> None) items in
        { parameter; storage; code; views }
    | _ -> Error (TM.location n, "module must be a seq")

  let parse_string ~check_indentation s =
    let open Result.Infix in
    Micheline.Parse.parse_toplevel_string ~check_indentation s >>= fun m ->
    match of_micheline m with
    | Ok t -> Ok t
    | Error (l, e) -> Error [Parser_error (l, e)]

  let parse_file ~check_indentation fn =
    match File.to_string fn with
    | Error (`Exn e) -> Tezos_error_monad.Error_monad.error_with_exn e
    | Ok s -> parse_string ~check_indentation s

  let pp ppf m =
    match to_micheline m with
    | Seq (_, xs) ->
        Format.fprintf ppf "@[<v>%a@]" (Format.list " ;@ " Micheline.pp) xs
    | _ -> assert false

  let save = Micheline.save pp
end
