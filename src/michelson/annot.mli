(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2022 DaiLambda, Inc.                     *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** Parse Michelson annotation %a, :a, @a *)
val parse :
  'loc ->
  string ->
  ([> `Field of string | `Type of string | `Var of string ], 'loc * string) result

(** Filter the field annotations *)
val get_fields : ([> `Field of string ] as 'a) list -> string list * 'a list

(** Filter the variable annotations *)
val get_vars : ([> `Var of string ] as 'a) list -> string list * 'a list

(** Filter the type annotations *)
val get_types : ([> `Type of string ] as 'a) list -> string list * 'a list
