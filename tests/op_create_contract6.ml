(* REJECT *)
open SCaml

module N = struct
  module M = struct
    type storage = unit
    let[@entry] main () x = [], x + Int 1
  end
end

let[@entry] main () () =
  let op, ad = Contract.create (module N.M) None (Tz 0.0) () in
  [op], ()
