(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019-2021  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** [compile ~loc e cases] compiles a pattern match [match e with <cases>] *)
val compile :
  loc:Location.t ->
  IML.t (* matched expr *) ->
  (Transpattern.Pattern.t * IML.t option (* when *) * IML.t) list (* cases *) ->
  IML.t
