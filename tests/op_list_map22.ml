[@@@SCaml iml_optimization = false]

(*
STORAGE=([]: bool list)   
*)
open SCaml
let[@entry] main () _ =
  ( [],
    let one = Int 1 in
    List.map (fun x -> x = one) [Int 1; Int 2; Int 3] )
