[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main x y =
  ( [],
    match Map.get_and_update (Int 2) (Some (Nat 2)) Map.empty with
    | Some _, _ -> assert false
    | None, m -> (
        match Map.get_and_update (Int 2) None m with
        | Some (Nat 2), m -> begin
            match Map.get_and_update (Int 2) None m with
            | None, _ -> ()
            | Some _, _ -> assert false
          end
        | _ -> assert false) )
