(*
   STORAGE=(None : (timestamp * timestamp) option)
*)
open SCaml
open Timestamp
let[@entry] main x y =
  let now = Global.get_now () in
  [], Some (now, sub now (Int 3153600000 (* 100 * 365 * 24 * 60 * 60 *)))
