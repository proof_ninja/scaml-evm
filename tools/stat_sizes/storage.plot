set terminal png
set output '_data/storage.png'
set key left top
set xlabel "original (bytes)"
set ylabel "optimized (bytes)"
f(x)=a*x
set title "storage optimization"
fit f(x) "_data/storage.data" u 3:9 via a
plot "_data/storage.data" using 3:9 title "size", \
     x title "100%", \
     f(x) title "78.52%"

