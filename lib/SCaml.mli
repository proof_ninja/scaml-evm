(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019-2022  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* The actual implementation of these API functions are given in
   primitives.ml as Michelson code.
*)

(** To emphasize the types where SCaml only accepts literals *)
type 'a literal = 'a [@@deriving typerep]

(** Arbitrary length natural number.

    Syntax: [Nat n].  [n] must be a natural number literal.

    Examples: [Nat 0], [Nat 99999999999999999999], [Nat 0xffee]

    The constructor [ZNat] is for OCaml semantics.
    You cannot use [ZNat] in SCaml. Use [Nat] instead.
*)
type nat = ZNat of Z.t [@@deriving typerep]

(** Arbitrary length integer.

    Syntax: [Int n].  [n] must be an integer literal.

    Examples: [Int 0], [Int (-99999999999999999999)], [Int 0xffee]

    The constructor [ZInt] is for OCaml semantics.
    You cannot use [ZInt] in SCaml. Use [Int] instead.
*)
type int = ZInt of Z.t [@@deriving typerep]

(** Tezzie.  The smallest unit is micro tz, [Tz 0.000001].

    Syntax: [Tz f].  [f] is a positive floating point number literal
    in Tezzies upto [9223372036854.775807].

    Examples: [Tz 1.0]  [Tz 100.]

    The constructor [ZMutez] is for OCaml semantics.  You cannot
    use [ZMutez] in SCaml programs.  Unlike [Tz], [ZMutez z] takes
    [z : Z.t] of micro-tezzies.
*)
type tz = ZMutez of Z.t [@@deriving typerep]

(** Option type *)
type nonrec 'a option = 'a option =
  | None
  | Some of 'a
[@@deriving typerep]

module Option : sig
  type 'a t = 'a option [@@deriving typerep]

  (** Getting the contents of an option value, with a default:

      [value (Some x) def = x]
      [value None def = def]
  *)
  val value : 'a t -> 'a -> 'a

  (** Getting the contents of an option value.  If the option is [None]
      it fails:

      [get (Some x) = x]
      [get None -> error ]
  *)
  val get : 'a t -> 'a

  (** Map over an option *)
  val map : ('a -> 'b) -> 'a t -> 'b t
end

(** Basic sum type corresponds with Michelson's [or] type. *)
type ('a, 'b) sum =
  | Left of 'a
  | Right of 'b
[@@deriving typerep]

module Sum : sig
  type ('a, 'b) t = ('a, 'b) sum =
    | Left of 'a
    | Right of 'b
  [@@deriving typerep]

  (** Get the contents of [Left].  If the sum value is [Right], it fails:

      [get_left (Left x) = x]
      [get_left (Right x) -> error]
  *)
  val get_left : ('a, 'b) sum -> 'a

  (** Get the contents of [Right].  If the sum value is [Left], it fails:

      [get_right (Right x) = x]
      [get_right (Left x) -> error]
  *)
  val get_right : ('a, 'b) sum -> 'b
end

(** Arithmetics *)

val ( + ) : int -> int -> int
val ( +^ ) : nat -> nat -> nat
val ( +$ ) : tz -> tz -> tz

val ( - ) : int -> int -> int
val ( -^ ) : nat -> nat -> int

(** Subtraction of [tz].

    [a -$ b] fails when [a < b].
*)
val ( -$ ) : tz -> tz -> tz

(** [a -$? b] returns [None] when [a < b] *)
val ( -$? ) : tz -> tz -> tz option

val ( ~- ) : int -> int
val ( ~-^ ) : nat -> int

val ( * ) : int -> int -> int
val ( *^ ) : nat -> nat -> nat
val ( *$ ) : tz -> nat -> tz

(** Divisions *)

val ediv_rem_int : int -> int -> (int * nat) option
val ediv_rem_nat : nat -> nat -> (nat * nat) option
val ediv_rem_tz : tz -> tz -> (nat * tz) option
val ediv_rem_tz_nat : tz -> nat -> (tz * tz) option

val ediv_int_int : int -> int -> (int * nat) option
[@@ocaml.deprecated "use ediv_rem_int"]

val ediv_int_nat : int -> nat -> (int * nat) option
[@@ocaml.deprecated "use ediv_rem_int"]

val ediv_nat_int : nat -> int -> (int * nat) option
[@@ocaml.deprecated "use ediv_rem_int"]

val ediv_nat_nat : nat -> nat -> (nat * nat) option
[@@ocaml.deprecated "use ediv_rem_nat"]

val ediv_tz_tz : tz -> tz -> (nat * tz) option
[@@ocaml.deprecated "use ediv_rem_tz"]

val ediv_tz_nat : tz -> nat -> (tz * tz) option
[@@ocaml.deprecated "use ediv_rem_tz_nat"]

(** Fail with [Int 0] if the divisor is [Int 0] *)
val ( / ) : int -> int -> int
[@@ocaml.deprecated "Use Euclidean division (//) instead"]

(** Euclidean division.  Fail with [Int 0] if the divisor is [Int 0] *)
val ( // ) : int -> int -> int

(** Euclidean division.  Fail with [Nat 0] if the divisor is [Nat 0] *)
val ( /^ ) : nat -> nat -> nat

(** Euclidean division.  Fail with [Tz 0.] if the divisor is [Tz 0.] *)
val ( /$ ) : tz -> tz -> nat

(** Euclidean division.  Fail with [Nat 0] if the divisor is [Nat 0] *)
val ( /$^ ) : tz -> nat -> tz

(** ( mod ) is not usable in SCaml code *)
val ( mod ) : int -> int -> nat
[@@ocaml.deprecated "Use Euclidean reminder (%%) instead"]

(** Euclidean reminder.  Different from Stdlib.(mod)
    Fails with [Int 0] when the divisor is [Int 0].
*)
val ( %% ) : int -> int -> nat

(** Euclidean reminder.  Fail with [Nat 0] if the divisor is [Nat 0] *)
val ( %^ ) : nat -> nat -> nat

(** Euclidean reminder.  Fail with [Tz 0.] if the divisor is [Tz 0.] *)
val ( %$ ) : tz -> tz -> tz

(** Euclidean reminder.  Fail with [Nat 0] if the divisor is [Nat 0] *)
val ( %$^ ) : tz -> nat -> tz

val ( lsl ) : nat -> nat -> nat
val ( lsr ) : nat -> nat -> nat
val ( lor ) : nat -> nat -> nat
val ( land ) : nat -> nat -> nat
val ( lxor ) : nat -> nat -> nat

val land_int_nat : int -> nat -> nat (* not a binop *)
[@@ocaml.deprecated "Use (land) instead"]

val lnot_nat : nat -> int (* not a binop *)
val lnot : int -> int (* not a binop *)

(* int => nat *)
val abs : int -> nat
val isnat : int -> nat option

(** Fails with [Int (-1)] if the argument is negative *)
val nat_of_int : int -> nat

(* nat => int *)
val int_of_nat : nat -> int

(* nat => tz *)

(** [mutez_of_nat (Nat 1) = Tz 0.000001].
    It fails if the input is too large for mutez. *)
val mutez_of_nat : nat -> tz

(* int => tz *)

(** [mutez_of_nat (Int 1) = Tz 0.000001]
    [mutez_of_nat (Int (-1))] fails with [Int (-1)]
    It fails if the input is too large for mutez. *)
val mutez_of_int : int -> tz

(* tz => int *)

val int_of_mutez : tz -> int

(* tz => nat *)

val nat_of_mutez : tz -> nat

(** Comparison

    They are fully polymorphic but only work for limited set
    of Michelson types.
*)

val compare : 'a -> 'a -> int
val ( = ) : 'a -> 'a -> bool
val ( <> ) : 'a -> 'a -> bool
val ( < ) : 'a -> 'a -> bool
val ( <= ) : 'a -> 'a -> bool
val ( > ) : 'a -> 'a -> bool
val ( >= ) : 'a -> 'a -> bool

(** Logical operators *)

(** Logical and.  This is NOT a function but a special form.
    [a && b] never evaluates [b] when [a] is evaluated to [false] *)
external ( && ) : bool -> bool -> bool = "%sequand"

(** Logical or.  This is NOT a function but a special form.
    [a || b] never evaluates [b] when [a] is evaluated to [true] *)
external ( || ) : bool -> bool -> bool = "%sequor"

(** Unlike (&&) and (||), [xor] is a function. *)
val xor : bool -> bool -> bool

(** Boolean AND function.  No short-circuiting *)
val band : bool -> bool -> bool

(** Boolean OR function.  No short-circuiting *)
val bor : bool -> bool -> bool

val not : bool -> bool

(** Tuples *)

val fst : 'a * 'b -> 'a
val snd : 'a * 'b -> 'b

(** Errors *)

(** Fail the execution of the smart contract.
    There is no way to catch the failure, since [try .. with ..]
    is not allowed in SCaml.

    You can also use [assert b] to fail the execution conditionally.
    [assert b] fails if and only if [b] is evaluated to [false].
*)
val failwith : 'a -> 'b

(** Another version of [failwith] with a better interface for OCaml simualtion *)
val raise : exn -> 'a

type never

exception Never

val never : never -> 'a

(** Loops *)
module Loop : sig
  (** Keep calling the given function over values of type ['a]
      while the function returns [Left a].

      The loop stops when the function returns [Right b] and returns [b].
  *)
  val left : ('a -> ('a, 'b) sum) -> 'a -> 'b
end

(** Data types *)

(** Lists *)
module List : sig
  type 'a t = 'a list [@@deriving typerep]

  val length : 'a t -> nat

  val map : ('a -> 'b) -> 'a t -> 'b t

  val fold_left : ('acc -> 'a -> 'acc) -> 'acc -> 'a list -> 'acc

  (** A variant of [fold_left] which takes an uncurried function.
      This is useful when the curried function is rejected because of
      the unpackable type of ['acc].
  *)
  val fold_left' : ('acc * 'a -> 'acc) -> 'acc -> 'a list -> 'acc

  val rev : 'a t -> 'a t

  val rev_append : 'a t -> 'a t -> 'a t
end

(** Sets

    Set literal can be written using [Set [ x; .. ; x ]] expression.

    Example:  [Set [Int 1; Int 2; Int 3]]

    The constructor [PSet] is for OCaml semantics.
    You cannot use [PSet] in SCaml. Use [Set] instead.
*)
type 'a set = PSet of 'a PSet.t [@@deriving typerep]

module Set : sig
  type 'a t = 'a set [@@deriving typerep]

  val empty : 'a t

  val is_empty : 'a t -> bool

  val length : 'a t -> nat

  val mem : 'a -> 'a t -> bool

  (** [update x b set] adds [x] when [b = true]
      and removes [x] when [b = false].

      Adding an element already exists in the set
      and removing an element  non existent in the set
      do not change the set.
  *)
  val update : 'a -> bool -> 'a t -> 'a t

  val add : 'a -> 'a t -> 'a t

  val remove : 'a -> 'a t -> 'a t

  val fold : ('elt -> 'acc -> 'acc) -> 'elt t -> 'acc -> 'acc

  (** A variant of [fold] which takes an uncurried function.
      This is useful when the curried function is rejected because of
      the unpackable type of ['acc].
  *)
  val fold' : ('elt * 'acc -> 'acc) -> 'elt t -> 'acc -> 'acc
end

(** Maps

    Map literal can be writen using [Map [ (k, v); .. ; (k, v) ]] expression.

    Example: [Map [(Int 1, "hello"); (Int 2, "bye")]]

    The constructor [PMap] is for OCaml semantics. You cannot it in SCaml.
    Use [Map] instead.
*)
type ('k, 'v) map = PMap of ('k, 'v) PMap.t [@@deriving typerep]

module Map : sig
  type ('k, 'v) t = ('k, 'v) map [@@deriving typerep]

  val empty : ('k, 'v) t

  val is_empty : ('k, 'v) t -> bool

  val length : ('k, 'v) t -> nat

  val map : ('k -> 'v -> 'w) -> ('k, 'v) t -> ('k, 'w) t

  val map' : ('k * 'v -> 'w) -> ('k, 'v) t -> ('k, 'w) t

  val get : 'k -> ('k, 'v) t -> 'v option

  val find : 'k -> ('k, 'v) t -> 'v option

  val get_with_default : 'k -> ('k, 'v) t -> 'v -> 'v

  val find_with_default : 'k -> ('k, 'v) t -> 'v -> 'v

  val mem : 'k -> ('k, 'v) t -> bool

  (** [update k vopt map] adds [k=v] when [vopt = Some v]
      and removes [k] when [vopt = None].

      Adding a binding already exists in the set overrides
      the existing binding.

      Removing a binding non existent in the map does not change
      the map.
  *)
  val update : 'k -> 'v option -> ('k, 'v) t -> ('k, 'v) t

  val add : 'k -> 'v -> ('k, 'v) t -> ('k, 'v) t

  val remove : 'k -> ('k, 'v) t -> ('k, 'v) t

  val get_and_update : 'k -> 'v option -> ('k, 'v) t -> 'v option * ('k, 'v) t

  val fold : ('k -> 'v -> 'acc -> 'acc) -> ('k, 'v) t -> 'acc -> 'acc

  (** A variant of [fold] which takes an uncurried function.
      This is useful when the curried function is rejected because of
      the unpackable type of ['v] and ['acc].
  *)
  val fold' : ('k * 'v * 'acc -> 'acc) -> ('k, 'v) t -> 'acc -> 'acc
end

(** Big maps

    Big map literal can be writen using [BigMap [ (k, v); .. ; (k, v) ]] expression.
    Big map literals are NOT allowed in SCaml programs.  They can be only used
    to specify the initial storage values.

    Example: [BigMap [(Int 1, "hello"); (Int 2, "bye")]]

    The constructor [PBigMap] is for OCaml semantics. You cannot use [PBigMap] in SCaml.
    Use [BigMap] instead.
*)
type ('k, 'v) big_map = PBigMap of ('k, 'v) PMap.t [@@deriving typerep]

module BigMap : sig
  type ('k, 'v) t = ('k, 'v) big_map [@@deriving typerep]

  val empty : ('k, 'v) t

  val get : 'k -> ('k, 'v) t -> 'v option

  val find : 'k -> ('k, 'v) t -> 'v option

  val get_with_default : 'k -> ('k, 'v) t -> 'v -> 'v

  val find_with_default : 'k -> ('k, 'v) t -> 'v -> 'v

  val mem : 'k -> ('k, 'v) t -> bool

  (** [update k vopt map] adds [k=v] when [vopt = Some v]
      and removes [k] when [vopt = None].

      Adding a binding already exists in the set overrides
      the existing binding.

      Removing a binding non existent in the map does not change
      the big map.
  *)
  val update : 'k -> 'v option -> ('k, 'v) t -> ('k, 'v) t

  val add : 'k -> 'v -> ('k, 'v) t -> ('k, 'v) t

  val remove : 'k -> ('k, 'v) t -> ('k, 'v) t

  val get_and_update : 'k -> 'v option -> ('k, 'v) t -> 'v option * ('k, 'v) t
end

(** Strings *)
module String : sig
  val length : string -> nat

  val cat : string -> string -> string

  val concat : string -> string -> string
  [@@ocaml.deprecated "Use String.cat"]

  (** Substring. [slice n1 n2 s] returns a substring of length [n2]
      from the position [n1] (zero based).

      If the specified region by [n1] and [n2] exceeds the string [s],
      it returns [None].
  *)
  val slice : nat -> nat -> string -> string option
end

val ( ^ ) : string -> string -> string

(** Bytes

    Bytes literals are written like [Bytes "0x0123456789abcdef"].
    The string must be even number of hex characters prefixed by "0x".
*)
type bytes = Bytes of string literal [@@deriving typerep]

module Bytes : sig
  type t = bytes [@@deriving typerep]

  val length : t -> nat

  val cat : t -> t -> t

  val concat : t -> t -> t
  [@@ocaml.deprecated "Use Bytes.cat"]

  (** Subbytes. [slice n1 n2 s] returns a subbytes of length [n2]
      from the position [n1] (zero based).

      If the specified region by [n1] and [n2] exceeds the bytes [s],
      it returns [None].
  *)
  val slice : nat -> nat -> t -> t option

  val of_string : string literal -> bytes
end

(** Addresses *)
type address = Address of string literal [@@deriving typerep]

module Address : sig
  type t = address [@@deriving typerep]
end

(** Key hashes *)
type key_hash = Key_hash of string literal [@@deriving typerep]

module Key_hash : sig
  type t = key_hash [@@deriving typerep]
end

(** Contract, entry points, and operation *)

type 'a contract

type operation

type operations = operation list

type ('param, 'storage) entry = 'param -> 'storage -> operations * 'storage

(** Contracts *)
module Contract : sig
  (** Contract whose parameter is ['a] *)
  type 'a t = 'a contract

  (** The contract of the code itself.  The type parameter of [self]
      must agree with the actual contract parameter.

      Unlike Michelson's [SELF] operator, [self] can appear inside a function.
      Even if the function value is sent to another contract, [self] still
      points to the original contract which uses [self] in its code.
  *)
  val self : 'a t

  val self_address : address

  val contract : address -> 'a t option

  (** Address of a contract with an entry point name.  The name must not start with '%'. *)
  val contract' : address -> string literal -> 'a t option

  (** [tz1], [tz2], [tz3] accounts *)
  val implicit_account : key_hash -> unit t

  val address : 'a t -> address

  (** Raw interface for CREATE_CONTRACT.

      Michelson code must be given as a string LITERAL.
      In Tezos you cannot generate contract code programically in a contract.

      The types of the contract and the initial storage are NOT checked by SCaml.
  *)
  val create_from_tz_code :
    string -> key_hash option -> tz -> 'storage -> operation * address

  (** Same as [create_from_tz_code] *)
  val create_raw :
    string -> key_hash option -> tz -> 'storage -> operation * address

  (** CREATE_CONTRACT from a michelson source file.

      Michelson file name must be given as a string literal.
      In Tezos you cannot generate contract code programically in a contract.

      The types of the contract and the initial storage are NOT checked
      by SCaml.
  *)
  val create_from_tz_file :
    string -> key_hash option -> tz -> 'storage -> operation * address

  module type MODULE = sig
    type storage
  end

  type 'storage module_ = (module MODULE with type storage = 'storage)

  (** CREATE_CONTRACT from a SCaml module *)
  val create :
    'storage module_ -> key_hash option -> tz -> 'storage -> operation * address
end

(** Operations *)
module Operation : sig
  type t = operation

  val transfer_tokens : 'a -> tz -> 'a contract -> t

  val set_delegate : key_hash option -> t
end

(** Timestamps

    Timestamp literals are [Timestamp s] where [s] is a valid
    RFC3339 string. ex. [Timestamp "2019-09-11T08:30:23Z"].
*)
type timestamp = Timestamp of string literal [@@deriving typerep]

module Timestamp : sig
  type t = timestamp [@@deriving typerep]

  val add : t -> int -> t

  val sub : t -> int -> t

  val diff : t -> t -> int
end

(** Chain ids *)
type chain_id = Chain_id of string literal [@@deriving typerep]

module Chain_id : sig
  type t = chain_id [@@deriving typerep]
end

type bls12_381_g1 =
  | G1Bytes of string literal (* byte sequence of Michelson encoding *)
  | G1Point of string literal * string literal
(* in nats *)

type bls12_381_g2 =
  | G2Bytes of string literal (* byte sequence of Michelson encoding *)
  | G2Point of (string * string) literal * (string * string) literal
(* in nats *)

type bls12_381_fr =
  | FrBytes of string literal (* byte sequence of Michelson encoding *)
  | Fr of string literal
(* natural number in a string *)

module BLS12_381 : sig
  type g1 = bls12_381_g1
  type g2 = bls12_381_g2
  type fr = bls12_381_fr

  module G1 : sig
    type t = g1
    val ( + ) : t -> t -> t
    val ( * ) : t -> fr -> t
    val ( ~- ) : t -> t
  end

  module G2 : sig
    type t = g2
    val ( + ) : t -> t -> t
    val ( * ) : t -> fr -> t
    val ( ~- ) : t -> t
  end

  module Fr : sig
    type t = fr
    val ( + ) : t -> t -> t
    val ( * ) : t -> t -> t
    val to_int : t -> int
    val mul_int : int -> t -> t
    val mul_nat : int -> t -> t
    val ( ~- ) : t -> t
  end

  val pairing_check : (g1 * g2) list -> bool
end

(** Global values

    They are consts but have functional types in order to provide
    semantics in the native OCaml compilation in future.
*)
module Global : sig
  val get_now : unit -> timestamp
  val get_amount : unit -> tz
  val get_balance : unit -> tz
  val get_source : unit -> address
  val get_sender : unit -> address
  val get_chain_id : unit -> chain_id
  val get_level : unit -> nat
  val get_voting_power : unit -> nat
  val get_total_voting_power : unit -> nat
end

module Env : sig
  type t

  val get : unit -> t

  val now : t -> timestamp
  val amount : t -> tz
  val balance : t -> tz
  val source : t -> address
  val sender : t -> address
  val chain_id : t -> chain_id
  val level : t -> nat
  val voting_power : t -> nat
  val total_voting_power : t -> nat
end

(** Keys *)
type key = Key of string literal [@@deriving typerep]

module Key : sig
  type t = key [@@deriving typerep]
end

(** Signatures *)
type signature = Signature of string literal [@@deriving typerep]

module Signature : sig
  type t = signature [@@deriving typerep]
end

(** Cryptographic algorithms *)
module Crypto : sig

  val check_signature : key -> signature -> bytes -> bool

  val blake2b : bytes -> bytes

  val sha256 : bytes -> bytes

  val sha512 : bytes -> bytes

  val keccak : bytes -> bytes

  val sha3 : bytes -> bytes

  val hash_key : key -> key_hash

  module Internal : sig
    val test : unit -> unit
  end
end

(** Serialization *)
module Obj : sig
  val pack : 'a -> bytes
  val unpack : bytes -> 'a option

  module TypeSafe : sig
    val pack : 'a Typerep_lib.Std.Typerep.t -> 'a -> bytes
    val unpack : 'a Typerep_lib.Std.Typerep.t -> bytes -> 'a option
  end

  module Internal : sig
    module type TypeSafePack = sig
      open Typerep_lib.Std
      val pack' : 'a Typerep.t -> 'a -> string
      val unpack' : 'a Typerep.t -> string -> 'a option
    end

    val type_safe_pack : (module TypeSafePack) option ref
  end
end

(** [ticket] is linear: a variable of a type contains [ticket]
    cannot be used more than once.
*)
type 'a ticket

module Ticket : sig
  type 'a t = 'a ticket
  val create : 'a -> nat -> 'a t
  val read : 'a t -> (address * 'a * nat) * 'a t
  val split : 'a t -> nat * nat -> ('a t * 'a t) option
  val join : 'a t -> 'a t -> 'a t option
end

type 'ms sapling_state
type 'ms sapling_transaction

module Sapling : sig
  type 'ms state = 'ms sapling_state
  type 'ms transaction = 'ms sapling_transaction
  val empty_state : 'ms -> 'ms state
  val verify_update : 'ms transaction -> 'ms state -> (bytes * (int * 'ms state)) option
end

val view : address -> string literal -> 'param -> 'a option

val _Nat : string -> nat
val _Int : string -> int
val _Mutez : string -> tz
val _Set : 'a list -> 'a set
val _Map : ('k * 'v) list -> ('k, 'v) map
val _BigMap : ('k * 'v) list -> ('k, 'v) big_map

(* Not for SCaml *)
val z_of_int : int -> Z.t
val z_of_nat : nat -> Z.t
val z_of_mutez : tz -> Z.t
