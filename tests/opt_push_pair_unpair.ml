[@@@SCaml iml_optimization = false]
open SCaml

let[@entry] main () _ = match Nat 1, Int 1 with x, y -> [], assert (abs y = x)
