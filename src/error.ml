(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2021  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open SCaml_tools

type code =
  | Unsupported
  | Type_expr
  | Stdlib
  | Constant
  | Cfold
  | Entry
  | Entry_typing
  | Link
  | Freevar
  | Self
  | Contract
  | Pattern_match
  | Primitive
  | Flags
  | Attribute
  | Convert_ident
  | Installation
  | Internal

let int_of_code = function
  | Unsupported -> 000
  | Type_expr -> 010
  | Stdlib -> 020
  | Constant -> 200
  | Cfold -> 250
  | Entry -> 300
  | Entry_typing -> 310
  | Link -> 350
  | Freevar -> 400
  | Self -> 500
  | Contract -> 600
  | Pattern_match -> 700
  | Primitive -> 800
  | Flags -> 900
  | Attribute -> 910
  | Convert_ident -> 920
  | Installation -> 998
  | Internal -> 999

let scaml_error_name n = Printf.sprintf "[ESCaml%03d]" (int_of_code n)

let raisef code ~loc fmt =
  let ename = scaml_error_name code in
  Location.raise_errorf ~loc ("%s " ^^ fmt) ename

let unsupported ~loc fmt = raisef Unsupported ~loc (fmt ^^ " is not supported")

let raisef_internal ~loc fmt =
  Format.kasprintf
    (fun s ->
      raisef
        Internal
        ~loc
        "SCaml internal error: %s\n%s"
        s
        Printexc.(raw_backtrace_to_string (get_callstack 20)))
    fmt

exception Wrapped_OCaml_error of Location.t * string * exn

let () =
  let rec f = function
    | Wrapped_OCaml_error (_, _, (Wrapped_OCaml_error _ as exn)) -> f exn
    | Wrapped_OCaml_error (loc, msg, exn) ->
        Some
          (match Location.error_of_exn exn with
          | Some (`Ok ocaml) ->
              {
                ocaml with
                main = {loc; txt = (fun ppf -> Format.fprintf ppf "%s" msg)};
                sub = ocaml.Location.main :: ocaml.Location.sub;
              }
          | _ ->
              {
                Location.kind = Report_error;
                main =
                  {
                    loc;
                    txt =
                      (fun ppf ->
                        Format.fprintf
                          ppf
                          "unknown exception: %s"
                          (Printexc.to_string exn));
                  };
                sub = [];
              })
    | _ -> None
  in
  Location.register_error_of_exn f

let wrap_ocaml_exn exn n ~loc fmt =
  let open Format in
  let buf = Buffer.create 64 in
  let ppf = formatter_of_buffer buf in
  Misc.Color.set_color_tag_handling ppf ;
  let ename = scaml_error_name n in
  kfprintf
    (fun _ ->
      pp_print_flush ppf () ;
      let msg = Buffer.contents buf in
      raise (Wrapped_OCaml_error (loc, msg, exn)))
    ppf
    ("%s " ^^ fmt)
    ename

let warnf n ~loc fmt =
  let ename = scaml_error_name n in
  let f s =
    let open Location in
    let report =
      {
        kind = Report_warning ename;
        main = {loc; txt = (fun ppf -> Format.pp_print_string ppf s)};
        sub = [];
      }
    in
    print_report !formatter_for_warnings report
  in
  Format.kasprintf f fmt
