(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2022  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open Michelson

(** Iterator *)

val type_ : (Type.t -> unit) ->Type.t -> unit
val constant : (Type.t -> unit) * (Constant.t -> unit) * (Opcode.t -> unit) -> Constant.t -> unit
val opcode : (Type.t -> unit) * (Constant.t -> unit) * (Opcode.t -> unit) -> Opcode.t -> unit
val module_ : (Type.t -> unit) * (Constant.t -> unit) * (Opcode.t -> unit) -> Module.t -> unit
