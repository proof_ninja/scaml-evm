(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                   Copyright 2019,2020  DaiLambda, Inc.                 *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** Micheline parser and printer tools *)

open Spotlib.Spot
open Tezos_micheline
open Tezos_micheline.Micheline

(** We use AST with comments without locations *)
type t = Micheline_printer.node
type node = t
type comment = Micheline_printer.location

val no_comment : comment

(** Override the comment of the node *)
val set_comment : t -> string option -> t

val string : string -> t

val bytes : string -> t

val int : Z.t -> t

val prim : string -> t list -> annot -> t

val seq : t list -> t

(** Change the printing *)
val use_pp_custom : bool ref

val pp : Format.t -> t -> unit

(** Save a value to a file *)
val save : (Format.formatter -> 'a -> unit) -> string -> 'a -> unit

module Parse : sig
  (** Parsed result *)
  type location = Micheline_parser.location

  type t = (location, string) Micheline.node

  type parsed = t

  (** All the locations are dummies *)
  val to_parsed : node -> t

  val of_parsed :t -> node

  val parse_expression_string :
    ?check_indentation: bool ->
    string ->
    (parsed, Tezos_error_monad.Error_monad.tztrace) result

  (** sequence without { }, i.e.  parameter ...; storage ..; code { } *)
  val parse_toplevel_string :
    ?check_indentation: bool ->
    string ->
    (parsed, Tezos_error_monad.Error_monad.tztrace) result

  val parse_expression_file :
    ?check_indentation: bool ->
    string ->
    (parsed, Tezos_error_monad.Error_monad.tztrace) result

  (** sequence without { }, i.e.  parameter ...; storage ..; code { } *)
  val parse_toplevel_file :
    ?check_indentation: bool ->
    string ->
    (parsed, Tezos_error_monad.Error_monad.tztrace) result
end

type location = Parse.location

val pp_location : Format.formatter -> location -> unit

type parsed = Parse.parsed

module type S = sig
  type t
  val to_micheline : t -> node
  val of_micheline : Parse.parsed -> (t, Parse.location * string) result
  val pp : Format.formatter -> t -> unit
end

module Encoding : sig
  type t = Michelson_v1_primitives.prim Micheline.canonical
  type expr = t
  type lazy_expr = expr Data_encoding.lazy_t

  val lazy_expr_encoding : Michelson_v1_primitives.prim Micheline.canonical Data_encoding.lazy_t Data_encoding.encoding

  val to_lazy_expr : node -> (lazy_expr, Tezos_error_monad.Error_monad.tztrace) result

  val to_lazy_bytes : node -> bytes

  val to_expr : node -> (expr, Tezos_error_monad.Error_monad.tztrace) result

  val to_bytes : node -> bytes
end
