(*
INPUT=Int 10
*)

open SCaml

let rec fact acc x = if x = Int 0 then acc else fact (x * acc) (x - Int 1)

let[@entry] main ten () =
  let v = fact (Int 1) ten in
  assert (v = Int 3628800) ;

  [], ()
