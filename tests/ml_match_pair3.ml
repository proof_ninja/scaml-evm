(*
STORAGE= Int 0
*)

open SCaml

let[@entry] main () _ =
  let (x, y, z), w = (Int 1, Int 2, Int 3), Int 4 in
  [], x + y + z + w
