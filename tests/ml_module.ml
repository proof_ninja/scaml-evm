open SCaml

module M = struct
  let f x = x + Int 1
end

let[@entry] main () () = [], assert (M.f (Int 0) = Int 1)
