#!/bin/bash

export COMMIT=13.0
export OCAML_VERSION=4.12.1
export TEZOS_TAG=v13.0
export SCAML_DOCKER_TAG=13.0.0
./build.sh
