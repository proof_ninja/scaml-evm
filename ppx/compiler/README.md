# scaml.ppx

NOTE: This is still an experimental feature.

Make SCaml work in OCaml eco system: with Dune and Merlin.

## Limitation of the standalone compiler

SCaml's standalone compiler `scamlc' is a simple way to compile smart contracts but has some drawbacks:

Hard to use within Dune build system
:    Dune is specialized for OCaml and the compilers must be `ocamlc` or `ocamlopt`.  It does not know about `scamlc` therefore it is hard to write build rules for it.

SCaml specific errors were not found in IDE
:    Merlin IDE shows OCaml errors on the fly, but it could not show SCaml specific errors, such as "this type is not supported in SCaml".  You had to run `scamlc` to find them out.

Each smart contract required one independent source file
:    Often one dApp consists of more than one smart contracts, and it may be favorable to define them in one source file.  The standalone `scamlc`, however, requires at least one source file per smart contract.

Smart contract code and non contract code could not coexist
:    It was impossible to have non contract code together with contract code.  Contract codes had to be in independent modules.  Tests and tool functions in OCaml must be written in other modules.

## scaml.ppx makes SCaml seamless in the OCaml eco-system

`scaml.ppx`, a PPX for SCaml, to solve the drawbacks above:

Construct smart contracts within Dune
:    Multiple smart contracts defined over multiple toplevel modules are now compiled by Dune without having complex rules to build.

     Smart contract codes in OCaml source files are type-checked then precompiled by SCaml compiler library.  The precompiled smart contract codes are stored in the OCaml object files.

     The precompiled smart contract codes are gathered into an OCaml executable by Dune.  The executable links and emits the smart contracts.
     
IDE is aware of SCaml specific errors
:    `scaml.ppx` checks SCaml specific errors and report them to Merlin.  Most of SCaml specific errors now can be detected within the IDE, except those which are found by SCaml linker.

Smart contract code and non contract code can coexist
:    Modules without `[@@@SCaml]` attributes are ignored by SCaml compilation by `scaml.ppx`.  You can now have smart contract codes and non contract codes within one module.

## How to use `scaml.ppx`

Example: `tests/`

### Code

Smart contract code must be defined within a structure with an attribute `[@@@SCaml]`:

```ocaml
open SCaml

module M = struct
  [@@@SCaml]  (* Structure for Smart contracts *)
  let one = Int 42
end
```

Structures without `[@@@SCaml]` are non contract code, and ignored by `scaml.ppx`:

```ocaml
module N = struct
  let () = prerr_endline "This is not for smart contract"	
end
```

The entry points for a smart contract must be defined in one structure with `[@@@SCaml contract="name"]`.  The smart contract is compiled as `name.tz`:

```ocaml
module Contract = struct
  (* Will produce my_smart_contract.tz *)
  [@@@SCaml contract="my_smart_contract"]
  let [@entry] default () _ = [], M.one
end
```

Here is the entire example:

```ocaml
open SCaml

module M = struct
  [@@@SCaml]  (* Structure for Smart contracts *)
  let one = Int 42
end

module N = struct
  let () = prerr_endline "This is not for smart contract"	
end

module Contract = struct
  [@@@SCaml contract="my_smart_contract"]
  let [@entry] default () _ = [], M.one
end
```

`scaml.ppx` can handle multiple toplevel modules defining multiple smart contracts.  Smart contracts may share code.

### Dune

#### Build an executable

* Use libraries `typerep`, `scaml.scamlib` and `scaml.compiler`
* Preprocessor: `(staged_pps ppx_typerep_conv scaml.ppx)`.  `staged_pps` is mandatory.

`typerep` and `ppx_typerep_conv` are not mandatory but they are useful to convert SCaml and Michelson values:

```
(executable
  (name my_smart_contract)
  (libraries typerep scaml.scamlib scaml.compiler)
  (preprocess (staged_pps ppx_typerep_conv scaml.ppx)))
```

#### Rule to emit Michelson

The smart contract is emitted by executing the executable:

```
(rule
 (target my_smart_contract.tz)
 (deps ./my_smart_contract.exe)
 (action (run ./my_smart_contract.exe)))
```

### Merlin

Once `dune build` tries to build `my_smart_contract.exe`, `.merlin` file for the directory should be built by `dune` automatically.
