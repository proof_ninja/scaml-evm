# SCaml standalone compiler `scamlc`

This covers the standalone compiler `scamlc`.  The PPX compiler `scaml.ppx` is out of the scope of this document.

## Compilation of contract code

In the simplest usage of `scamlc` is `scamlc <source_file>`.

Suppose `foo.ml` has the following contents:

```ocaml
(* foo.ml *)
open SCaml

let [@entry] main () () = [], ()
```

then,

```shell
$ scamlc foo.ml
```

compiles it to `foo.tz`.  The code is first type-checked by OCaml therefore it also produces OCaml compilation files `foo.cm*`.

### Contracts in modules

If a contract is defined in a module `module M = struct .. end`, the name of the module is appended to the output Michelson filename:

```ocaml
(* foo2.ml *)
open SCaml

module M = struct
  let [@entry] main () () = [], ()
end
```

then,

```shell
$ scamlc foo2.ml
```

compiles it to `foo2.M.tz`, instead of `foo2.tz`.

### Multiple contracts

A source file may define multiple contracts separated in module declarations:

```ocaml
(* foo3.ml *)
open SCaml

module M = struct
  let [@entry] main () () = [], ()
end

module N = struct
  let [@entry] main () x = [], x + Int 1
end
```

`scamlc foo3.ml` compiles it to `foo3.M.tz` and `foo3.N.tz`.

### Multiple source files

Contract defintions can be separated into multiple source files.  To compile such multi file contracts, give the source file names in the order of the dependency:  `scamlc <source-file1> <source-file2> .. <source-filen>`.

Separate compilation is not supported by `scamlc`.  You have to list all the source files at once.

## Convert mode

`scamlc --scaml-convert source.ml` compiles the values bound at toplevel to Michelson and print them:

```ocaml
(* x.ml *)
open SCaml
let int = Int 1
let string = "hello"
let pair = (Nat 2, Tz 0.2)
let _ = fun x -> x + Int 1
```

```shell
$ scamlc --scaml-convert x.ml
int: 1
string: "hello"
pair: Pair 2 200000
noname: { PUSH int 1
          /* env: [ (_const_149 : int); (i_144 : int) ] */ ;
          DIG 1
          /* use+free i_144, env: [ (_v_150 : int); (_const_149 : int) ] */ ;
          ADD
          /* call +, env: [ (_result_+_151 : int) ] */ }
Nothing to link...
```

You can include a type definition in the source:

```ocaml
(* x.ml *)
open SCaml
type t = { x : int; y : int }
let _ = { x = Int 1; y = Int 2 }
```

```shell
$ scamlc --scaml-convert x.ml
type t: pair :t (int %x) (int %y)
noname: Pair 1 2
Nothing to link...
```

The source code may refer to types defined in other modules, if they are already compiled and their `*.cmi` files exist.

## Revert mode

`scamlc --scaml-revert <michelson-value-file> <scaml-type-file>`

`<michelson-value-file>` is a file name contains a Michelson value.

`<scaml-type-file>` is SCaml code only with one type definition.

The Michelson values in `<michelson-value-file>` is reverted to SCaml value of type specified in `<scaml-type-file>`.

```
# michelson.tz
Pair 1 2
```

```ocaml
(* type.ml *)
open SCaml
type t = int * int
```

```shell
$ scaml --scaml-revert michelson.tz type.ml
input: Pair 1 2
((Int 1), (Int 2))
```

Another example:

```
# michelson.tz
Pair 1 2
```

```ocaml
(* type.ml *)
open SCaml
type t = { x : int; y : int }
```

```shell
$ scaml --scaml-revert michelson.tz type.ml
input: Pair 1 2
{ x = (Int 1); y = (Int 2) }
Nothing to link...
```








