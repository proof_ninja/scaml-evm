[@@@SCaml iml_optimization = false]
open SCaml
let[@entry] main x y =
  ( [],
    let f = if true then fun (n, sum) -> n + sum else assert false in
    assert (Int 6 = Set.fold' f (Set [Int 1; Int 2; Int 3]) (Int 0)) )
