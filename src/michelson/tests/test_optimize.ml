(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2022  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open SCaml_michelson

module Optimize = Optimize.Make(struct
    let logger = prerr_endline
    let do_dijkstra = true
    let search_space_multiplier = 1.0
    let use_lcs_for_astar_score = true
  end)

let () =
  let open Alcotest in
  run "optimize" [
    "optimize", [
      "test", `Quick, Optimize.test;
    ]
  ]
