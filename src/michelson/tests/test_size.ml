(**************************************************************************)
(*                                                                        *)
(*                                 SCaml                                  *)
(*                                                                        *)
(*                       Jun Furuse, DaiLambda, Inc.                      *)
(*                                                                        *)
(*                     Copyright 2022  DaiLambda, Inc.                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

open SCaml_tools
open SCaml_michelson
open Michelson
open Size

let test_type () =
  let open Type in
  let test t =
    let estimate = type_ t in
    let real = Bytes.length (Micheline.Encoding.to_bytes (to_micheline t)) in
    Format.eprintf "size %a: estimate: %d real: %d@." pp t estimate real;
    if not (estimate = real) then begin
      assert false
    end
  in
  test tyString;
  test { tyString with tyannot = Some "foobar" };
  test (tyList tyString);
  test (tyBigMap (tyString, tyInt));
  test (tySapling_state 0);
  test (tySapling_state 64);
  test (tySapling_state 4096);
  test (tySapling_state 8192);
  test { (tyPair (None, tyInt, None, tyString)) with tyannot = Some "foobar" };
  test { (tyPair (Some "aaa", tyInt, None, tyString)) with tyannot = Some "foobar" };
  test { (tyPair (None, tyInt, Some "bbb", tyString)) with tyannot = Some "foobar" };
  test { (tyPair (Some "aaa", tyInt, Some "bbb", tyString)) with tyannot = Some "foobar" };
  test { (tyPair (Some "aaa", tyInt, Some "bbb", tyString)) with tyannot = Some "foobar" };
  test (tyPair (Some "aaa", tyInt, Some "bbb", tyString));
  test (tyPair (Some "aaa", tyInt, None, tyPair (Some "bbb", tyString, Some "ccc", tyBool)));
  test (tyPairN [(Some "aaa", tyInt); (Some "bbb", tyString); (Some "ccc", tyBool)]);
  ()

let test_constant () =
  let open Constant in
  let byte_size t =
    let m = to_micheline t in
    Format.eprintf "size %a:@." pp t;
    let estimate = constant t in
    let real = Bytes.length (Micheline.Encoding.to_bytes m) in
    Format.eprintf "  estimate: %d real: %d@." estimate real;
    if not (estimate = real) then begin
      assert false
    end;
    estimate
  in
  let test x = ignore & byte_size x in
  test Unit;
  test & Bool true;
  test & Int (Z.of_int 10000);
  test & Int (Z.of_int (-1));
  test & Int (Z.of_int (-125));
  test & Int (Z.of_int (-126));
  test & Int (Z.of_int (-127));
  test & String "hello";
  test & Bytes "0x012345";
  test & Option None;
  test & Option (Some Unit);
  test & List [String "a"; String "ab"; String "abc"];
  test & Map [(String "a", Int (Z.of_int 10)); (String "ba", Int (Z.of_int 100000))];
  test & Pair (Bool true, String "hello");
  test & PairN [Bool true; String "hello"; String "bye"];
  test & Left (Bool true);
  test & Timestamp (Z.of_int 10000000);
  (* test & Code [...]; (* XXX *) *)
  ()

let test_opcode () =
  let open Opcode in
  let byte_size t =
    let m = to_micheline t in
    Format.eprintf "size %a:@." pp t;
    let estimate = opcode t in
    let real = Bytes.length (Micheline.Encoding.to_bytes m) in
    Format.eprintf "  estimate: %d real: %d@." estimate real;
    if not (estimate = real) then begin
      assert false
    end;
    estimate
  in
  let test x = ignore & byte_size x in
  test _SWAP;
  test _DROP;
  test (_DROPn 1);
  test (_DROPn 10);
  test (_DIG 2);
  test (_DIG 10000);
  test (_BLOCK []);
  test (_ITER [_SWAP; _SWAP]);
  test (_IF ([_SWAP], [_SWAP]));
  test (_IF_LEFT ([_SWAP], [_SWAP]));
  test (_IF_LEFT ([_BLOCK [_SWAP; _SWAP]], [_SWAP; _SWAP]));
  test (_IF_LEFT ([_IF_LEFT ([_SWAP], [_SWAP])], [_SWAP]));
  test (_DIP [_SWAP]);
  test (_DIPn (1, [_SWAP]));
  test (_DIPn (2, [_SWAP]));
  test (_LEFT Type.tyString);
  test (_LAMBDA (Type.tyString, Type.tyString, [ _SWAP ]));
  test (_PUSH (Type.tyString, Constant.String "hello"));
  test (_EMPTY_MAP (Type.tyString, Type.tyString));
  test (_PAIR []);
  test (_PAIR ["hello"]);
  test (_PAIR ["hello"; "bye"]);
  test (_SELF None);
  test (_SELF (Some "foobar"));
  test (_RENAME (Some "foobar"));
  test (_CONTRACT' (Type.tyString, "name"));
  test (_VIEW ("name", Type.tyString));
  test (_CREATE_CONTRACT Module.{ parameter = Type.tyString;
                                  storage = Type.tyString;
                                  code = [];
                                  views = [] });
  ()

let test_module () =
  let open Module in
  let byte_size t =
    let m = to_micheline t in
    Format.eprintf "size @[%a@]:@." pp t;
    let estimate = module_ t in
    let real = Bytes.length (Micheline.Encoding.to_bytes m) in
    Format.eprintf "  estimate: %d real: %d@." estimate real;
    if not (estimate = real) then begin
      assert false
    end;
    estimate
  in
  let test x = ignore & byte_size x in
  test { parameter = Type.tyString;
         storage =  Type.tyString;
         code = [];
         views = [] };
  test { parameter = Type.tyString;
         storage =  Type.tyString;
         code = [Opcode._SWAP];
         views = [] };
  test { parameter = Type.tyString;
         storage =  Type.tyString;
         code = [Opcode._SWAP];
         views = [ { name = "foobar";
                     ty_param = Type.tyString;
                     ty_return = Type.tyString;
                     code = [] } ] };
  ()

let () =
  let open Alcotest in
  run "size" [
    "size", [
      "type", `Quick, test_type;
      "constant", `Quick, test_constant;
      "opcode", `Quick, test_opcode;
      "module", `Quick, test_module
    ]
  ]
